const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
   entry: {
     index: './src/window.js',
     'add-rule': './src/add-rule-window.js',
     'export-preview': './src/export-preview-window.js',
     'export-progress': './src/export-progress-window.js',
   },
   output: {
      path: path.join(__dirname, '/dist'),
      filename: '[name].bundle.js'
   },
   devServer: {
      inline: true,
      port: 3000,
      contentBase: path.join(__dirname, 'assets')
   },
   module: {
      rules: [
         {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
               presets: ['@babel/preset-env', '@babel/preset-react']
            }
         },
         {
            test: /\.s[ac]ss$/i,
            use:  [
              MiniCssExtractPlugin.loader,
              {
                loader: 'css-loader',
                options: {
                  url: false
                }
              },
              'sass-loader',
          ]
        },
      ]
   },
   plugins:[
      new CopyWebpackPlugin({patterns: [
         {from: './src/index.html', to: '.'},
         {from: './src/loader-worker.html', to: '.'},
         {from: './src/add-rule.html', to: '.'},
         {from: './src/export-preview.html', to: '.'},
         {from: './src/export-progress.html', to: '.'},
         {from: './src/renderer-worker.html', to: '.'},
         {from: './assets', to: '.'}
      ]}),
      new MiniCssExtractPlugin({
        filename: '[name].css',
        chunkFilename: '[id].css',
      })
   ]
};
