const PDFDocument = require("pdfkit");
const fs = require("fs");

const doc = new PDFDocument({
  info: {
    Title: "Testowy pedeefik",
  }
});

doc.registerFont("LatoTest", "assets/fonts/lato-v16-latin_latin-ext-regular.ttf");
doc.font("LatoTest").fontSize(18).text("Zażółć gęślą jaźń");
doc.end();

doc.pipe(fs.createWriteStream("dist/pdftest.pdf"));
