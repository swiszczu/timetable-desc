/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * NSIS INSTALLER SCRIPT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */



###############################
#                             #
# Timetable DESC setup script #
#                             #
###############################



!include "MUI2.nsh"
!include "FileFunc.nsh"

!define SETUP_NAME "ttdesc-setup"
!define RELEASE_FOLDER "..\deploy\Timetable DESC-win32-ia32"
!define SOFTWARE_NAME "Timetable DESC"
!define REG_KEY "Software\Swiszcz\TTDESC"
!getdllversion "${RELEASE_FOLDER}\TTDESC.exe" Version

SetCompressor /SOLID lzma
SetCompressorDictSize 64
SetDatablockOptimize on
#SetCompressor zlib # for debug purposes

Name "Timetable DESC"
OutFile "..\deploy\${SETUP_NAME}-${Version1}.${Version2}.${Version3}.${Version4}.exe"
ManifestDPIAware true
Unicode true
RequestExecutionLevel admin
ShowInstDetails show



# VERSIONINFO block

VIAddVersionKey "ProductName" "${SOFTWARE_NAME} Installer"
VIAddVersionKey "Comments" "${SOFTWARE_NAME} Installer"
VIAddVersionKey "CompanyName" "Łukasz Świszcz"
VIAddVersionKey "LegalCopyright" "© 2020 Łukasz Świszcz"
VIAddVersionKey "FileDescription" "${SOFTWARE_NAME} Installer"
VIAddVersionKey "ProductVersion" "${Version1}.${Version2}.${Version3}.${Version4}"
VIAddVersionKey "FileVersion" "${Version1}.${Version2}.${Version3}.${Version4}"
VIProductVersion "${Version1}.${Version2}.${Version3}.${Version4}"

InstallDir "$PROGRAMFILES32\${SOFTWARE_NAME}"
InstallDirRegKey HKLM ${REG_KEY} "InstallLocation"

# MUI2 Setup

!define MUI_ICON "install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\classic-install.ico"

!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Header\win.bmp"
!define MUI_UNHEADERIMAGE
!define MUI_UNHEADERIMAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Header\win.bmp"

!define MUI_ABORTWARNING
!define MUI_ABORTWARNING_CANCEL_DEFAULT

!define MUI_UNABORTWARNING
!define MUI_UNABORTWARNING_CANCEL_DEFAULT

!define MUI_FINISHPAGE_RUN "$INSTDIR\TTDESC.exe"
!define MUI_FINISHPAGE_NOREBOOTSUPPORT
!define MUI_FINISHPAGE_NOAUTOCLOSE

!define MUI_FINISHPAGE_SHOWREADME
!define MUI_FINISHPAGE_SHOWREADME_TEXT "$(DESKTOP_SHORTCUT)"
!define MUI_FINISHPAGE_SHOWREADME_FUNCTION "CreateDesktopShortcut"

!define MUI_UNFINISHPAGE_NOAUTOCLOS

!define MUI_STARTMENUPAGE_DEFAULTFOLDER "${SOFTWARE_NAME}"
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKLM"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "SOFTWARE\Swiszcz\TTDESC"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"

# MUI pages

Var SMDir

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "license.rtf"
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_STARTMENU Application $SMDir
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH


# MUI languages

!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "Polish"

# Strings

LangString DESKTOP_SHORTCUT ${LANG_POLISH} "Utwórz skrót na pulpicie"
LangString DESKTOP_SHORTCUT ${LANG_ENGLISH} "Create desktop shortcut"

LangString DIRECTORY_NOT_EMPTY ${LANG_POLISH} "Folder instalacji nie jest pusty. Instalacja spowoduje usunięcie całej jego zawartości. Czy chcesz kontynuować?"
LangString DIRECTORY_NOT_EMPTY ${LANG_ENGLISH} "Installation directory is not empty. Its content is to be entirely removed. Are you sure you want to continue?"

LangString ABORT_WRONG_DIR ${LANG_POLISH} "Nie można zainstalować w tym folderze."
LangString ABORT_WRONG_DIR ${LANG_ENGLISH} "Can't install in this directory."

LangString UNNAME ${LANG_POLISH} "Odinstaluj ${SOFTWARE_NAME}"
LangString UNNAME ${LANG_ENGLISH} "Uninstall ${SOFTWARE_NAME}"

LangString ADD_REMOVE ${LANG_POLISH} "Dodaj informacje do okna 'Dodaj lub usuń programy'"
LangString ADD_REMOVE ${LANG_ENGLISH} "Add information to 'Add or remove programs' window"

LangString STARTMENU ${LANG_POLISH} "Dodaj skróty do Menu Start"
LangString STARTMENU ${LANG_ENGLISH} "Add Start Menu shortcuts"

LangString UN_WRONG_DIR ${LANG_POLISH} "Wygląda na to, że ${SOFTWARE_NAME} nie jest zainstalowany w tym folderze. Czy chcesz kontynuować mimo to (niezalecane)?"
LangString UN_WRONG_DIR ${LANG_ENGLISH} "It looks like ${SOFTWARE_NAME} is not installed in this directory. Are you sure you want to continue (not recommended)?"

LangString UN_ABORTED ${LANG_POLISH} "Deinstalacja została przerwana przez użytkownika."
LangString UN_ABORTED ${LANG_ENGLISH} "Uninstallaton has been aborted by user."


Function .onInit
	!insertmacro MUI_LANGDLL_DISPLAY
	
	# Retrieve last install dir from registry
	ReadRegStr $0 HKLM ${REG_KEY} "InstallLocation"
	StrLen $1 $0
	IntCmp $1 5 done done
		IntOp $2 $1 - 2
		StrCpy $INSTDIR $0 $2 1
	done:
FunctionEnd

Function CreateDesktopShortcut
	CreateShortcut "$DESKTOP\${SOFTWARE_NAME}.lnk" "$INSTDIR\TTDESC.exe"
FunctionEnd

Section
	IfFileExists "$INSTDIR\*.*" directory_exists
		CreateDirectory $INSTDIR

	directory_exists:
		Push $INSTDIR
		Call isEmptyDir
		Pop $0
		StrCmp $0 1 0 directory_not_empty
		  Goto directory_ok
		
		# Check if there is an old version of software there - it might be an update,
		# otherwise warn about files being currently in the directory
		directory_not_empty: 
			IfFileExists "$INSTDIR\TTDESC.exe" directory_ok
			MessageBox MB_YESNO|MB_ICONEXCLAMATION "$(DIRECTORY_NOT_EMPTY)" /SD IDNO IDYES continue IDNO abort
			continue:
				RMDir /r $INSTDIR
				CreateDirectory $INSTDIR
				Goto directory_ok
			abort:
				Abort "$(ABORT_WRONG_DIR)"
	directory_ok:
		# Save installdir to registry
		WriteRegStr HKLM ${REG_KEY} "InstallLocation" "$\"$INSTDIR$\""
		
		# Start copying files
		SetOutPath $INSTDIR
		File "${RELEASE_FOLDER}\chrome_100_percent.pak"
		File "${RELEASE_FOLDER}\chrome_200_percent.pak"
		File "${RELEASE_FOLDER}\d3dcompiler_47.dll"
		File "${RELEASE_FOLDER}\ffmpeg.dll"
		File "${RELEASE_FOLDER}\icudtl.dat"
		File "${RELEASE_FOLDER}\libEGL.dll"
		File "${RELEASE_FOLDER}\libGLESv2.dll"
		File "${RELEASE_FOLDER}\LICENSE"
		File "${RELEASE_FOLDER}\LICENSES.chromium.html"
		File "${RELEASE_FOLDER}\resources.pak"
		File "${RELEASE_FOLDER}\snapshot_blob.bin"
		File "${RELEASE_FOLDER}\v8_context_snapshot.bin"
		File "${RELEASE_FOLDER}\version"
		File "${RELEASE_FOLDER}\vk_swiftshader.dll"
		File "${RELEASE_FOLDER}\vk_swiftshader_icd.json"
		
		SetOutPath "$INSTDIR\locales"
		File /r "${RELEASE_FOLDER}\locales\*"
		
		SetOutPath "$INSTDIR\resources"
		File /r "${RELEASE_FOLDER}\resources\*"
		
		SetOutPath "$INSTDIR\swiftshader"
		File /r "${RELEASE_FOLDER}\swiftshader\*"
		
		# Windows Visual Elements Manifest
		SetOutPath "$INSTDIR"
		File "${RELEASE_FOLDER}\TTDESC.exe"
		File "TTDESC.VisualElementsManifest.xml"
		
		SetOutPath "$INSTDIR\win32"
		File "logo150.png"
		File "logo70.png"
SectionEnd

Section -AddOrRemovePrograms
	DetailPrint "$(ADD_REMOVE)"
	
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TTDESC" \
		"DisplayName" "${SOFTWARE_NAME}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TTDESC" \
		"UninstallString" "$\"$INSTDIR\uninstall.exe$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TTDESC" \
		"InstallLocation" "$\"$INSTDIR$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TTDESC" \
		"DisplayIcon" "$\"$INSTDIR\TTDESC.exe$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TTDESC" \
		"Publisher" "Łukasz Świszcz"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TTDESC" \
		"DisplayVersion" "${Version1}.${Version2}.${Version3}.${Version4}"
	WriteRegDword HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TTDESC" \
		"VersionMajor" "${Version1}"
	WriteRegDword HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TTDESC" \
		"VersionMinor" "${Version2}"
	WriteRegDword HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TTDESC" \
		"NoModify" "1"
	WriteRegDword HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TTDESC" \
		"NoRepair" "1"

	${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
	IntFmt $0 "0x%08X" $0
	
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TTDESC" \
		"EstimatedSize" "$0"
SectionEnd

Section -StartMenu
	DetailPrint "$(STARTMENU)"

	!insertmacro MUI_STARTMENU_WRITE_BEGIN Application ;This macro sets $SMDir and skips to MUI_STARTMENU_WRITE_END if the "Don't create shortcuts" checkbox is checked...
	CreateDirectory "$SMPrograms\$SMDir"
	CreateShortCut "$SMPROGRAMS\$SMDir\${SOFTWARE_NAME}.lnk" "$INSTDIR\TTDESC.exe"
	CreateShortCut "$SMPROGRAMS\$SMDir\$(UNNAME).lnk" "$INSTDIR\uninstall.exe"
	!insertmacro MUI_STARTMENU_WRITE_END
SectionEnd

Section -Finish
	SetOutPath "$INSTDIR"
	WriteUninstaller "uninstall.exe"
	
	${RefreshShellIcons}
SectionEnd

Section un.Uninstall

	#Check if TTDESC is installed in this directory
	IfFileExists $INSTDIR\TTDESC.exe ttdesc_installed
		MessageBox MB_YESNO "$(UN_WRONG_DIR)" IDYES ttdesc_installed
		Abort "$(UN_ABORTED)"

	ttdesc_installed:

	# Remove files
	Delete "$INSTDIR\TTDESC.exe"
	Delete "$INSTDIR\chrome_100_percent.pak"
	Delete "$INSTDIR\chrome_200_percent.pak"
	Delete "$INSTDIR\d3dcompiler_47.dll"
	Delete "$INSTDIR\ffmpeg.dll"
	Delete "$INSTDIR\icudtl.dat"
	Delete "$INSTDIR\libEGL.dll"
	Delete "$INSTDIR\libGLESv2.dll"
	Delete "$INSTDIR\LICENSE"
	Delete "$INSTDIR\LICENSES.chromium.html"
	Delete "$INSTDIR\resources.pak"
	Delete "$INSTDIR\snapshot_blob.bin"
	Delete "$INSTDIR\v8_context_snapshot.bin"
	Delete "$INSTDIR\version"
	Delete "$INSTDIR\vk_swiftshader.dll"
	Delete "$INSTDIR\vk_swiftshader_icd.json"
	Delete "$INSTDIR\TTDESC.visualelementsmanifest.xml"

	RMDir /r "$INSTDIR\locales"
	RMDir /r "$INSTDIR\resources"
	RMDir /r "$INSTDIR\swiftshader"
	RMDir /r "$INSTDIR\win32"

	Delete "$INSTDIR\uninstall.exe"

	# Remove SM shortcuts
	ReadRegStr $0 HKLM ${REG_KEY} "Start Menu Folder"
	StrLen $1 $0
	
	IntCmp $1 0 done done 0 # Directory name should be longer than 0 characters :)
	Delete "$SMPrograms\$0\${SOFTWARE_NAME}.lnk"
	Delete "$SMPrograms\$0\$(UNNAME).lnk"
	RMDir "$SMPrograms\$0"
	
	done:

	RMDir "$INSTDIR"
	
	# Clear Registry
	DeleteRegKey HKLM ${REG_KEY}

	# Remove from Add Or Remove Programs
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\TTDESC"
SectionEnd

# Functions

# https://nsis.sourceforge.io/Check_if_dir_is_empty
Function isEmptyDir
  # Stack ->                    # Stack: <directory>
  Exch $0                       # Stack: $0
  Push $1                       # Stack: $1, $0
  FindFirst $0 $1 "$0\*.*"
  strcmp $1 "." 0 _notempty
    FindNext $0 $1
    strcmp $1 ".." 0 _notempty
      ClearErrors
      FindNext $0 $1
      IfErrors 0 _notempty
        FindClose $0
        Pop $1                  # Stack: $0
        StrCpy $0 1
        Exch $0                 # Stack: 1 (true)
        goto _end
     _notempty:
       FindClose $0
       ClearErrors
       Pop $1                   # Stack: $0
       StrCpy $0 0
       Exch $0                  # Stack: 0 (false)
  _end:
FunctionEnd

