/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * deploy-win32.js - packages app with electron-packager for Windows
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const packager = require("electron-packager");

async function deploy(options) {
  const appPaths = await packager(options);
  console.log(`Electron app bundles created:\n${appPaths.join("\n")}`);
}

let fileSelector = (path) => {
	if(path.length == 0) return false;
	if(path === "/package.json") return false;
	if(path.startsWith("/lib")) {
		return !path.startsWith("/lib/custom-electron-titlebar") && path !== "/lib";
	}
	if(path.startsWith("/locales")) return false;
	if(path.startsWith("/node_modules")) return false;
	if(path.startsWith("/dist")) return false;
	if(path.startsWith("/src")) {
		return !path.startsWith("/src/backend") && path !== "/src";
	}
	return true;
};

deploy({
	arch: "ia32",
	asar: true,
	dir: ".",
  defefSymlinks: true,
	executableName: "TTDESC",
	ignore: fileSelector,
	icon: "./icon.ico",
	name: "Timetable DESC",
	out: "./deploy",
	overwrite: true,
	platform: 'win32',
	win32metadata: {
		"requested-execution-level": 'asInvoker',
		"LegalCopyright": "(C) 2020 Łukasz Świszcz",
	},
});
