# Timetable DESC

> Stylish PDF aSc Timetables render engine based on [Electron](https://www.electronjs.org/)

## Installation

### Binaries

You can download prebuilt binaries for Windows on the [releases page](https://gitlab.com/swiszczu/timetable-desc/-/releases).

### Source code

#### Prerequisites

+ [Git](https://git-scm.com/)
+ [Node.js](https://nodejs.org/en/)
+ [NPM](https://www.npmjs.com/)

#### Steps

If you want to tinker with code:

* Clone this repo using `git clone https://gitlab.com/swiszczu/timetable-desc`
* Run `npm install` to install all dependencies
* Start webpack development server using npm script: `npm run webpack`
* Start electron with nodemon in dev mode by running `npm start`

## Basic usage

* Export your timetable from aSc Timetables using `aSc Timetables XML Database` or `aSc Timetables 2012 XML` configuration
* Load that file using `Browse...` button in TTDESC window
* Configure rendering appearance on `Appearance` tab
* If you want, you can set up group merging and renaming on `Groups` tab:
    * Select grade you want to configure (grade is determined by the first group of numeric characters in class name)
    * Add some rules to subjects on the list below – e.g. rule defined as `name: group1` for two groups `group1a` and `group1b` on subject `Maths` will result in showing those 2 groups as one called `group1` on every Mathematics lesson - it can be useful in some schools where timetables tend to be overcomplicated
* Set PDF meta on `Input/Output` tab and select appropriate export option. If you select batch export, output filename will be ignored and every single file will be automatically named after an object it represents.

## License

+ Copyright (C) 2020 Łukasz Świszcz
+ [GPL v3 License](https://www.gnu.org/licenses/gpl-3.0.txt)
