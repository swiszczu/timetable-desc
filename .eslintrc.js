module.exports = {
    "env": {
        "commonjs": true,
        "browser": true,
        "es6": true,
        "node": true,
        "mocha": true
    },
    "extends": [
      "eslint:recommended",
      "plugin:react/recommended"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parser": "babel-eslint",
    "parserOptions": {
        "ecmaVersion": 2018
    },
    "rules": {
        "no-unused-vars": "warn",
        "semi": "warn",
        "no-redeclare": "off",
    },
    "plugins": [
      "babel",
      "react"
    ],
};
