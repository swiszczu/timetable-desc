/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * exporter.test.js - tests PDFExporter object
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// To test PDF exporter, we need to render Page component
// to get render info object
import React from 'react';
import { Page } from '../src/components/page.jsx';

const fs = require('fs');
const path = require('path');
const should = require('should');

// Mocha test framework declarations:
/* global describe, it, afterEach, beforeEach,  mount */

const PDFExporter = require('../src/backend/pdf-exporter');

describe('PDF Exporter tests', () => {
  /* eslint no-new: off */
  it('create exporter object', () => {
    should(() => { new PDFExporter(); }).not.throw();
  });

  describe('Object methods', () => {
    let exporter = null;

    beforeEach(() => {
      exporter = new PDFExporter();
    });

    afterEach(() => {
      exporter = null;
    });

    it('set document meta', () => {
      const title = 'title';
      const author = 'author';
      const subject = 'subject';

      exporter.setDocumentMeta(title, author, subject);

      exporter.doc.info.Title.should.equal(title);
      exporter.doc.info.Author.should.equal(author);
      exporter.doc.info.Subject.should.equal(subject);
      exporter.doc.info.Producer.should.be.a.String().and.not.empty();
      exporter.doc.info.Creator.should.be.a.String().and.match(/Timetable DESC/);
    });

    it('set output path', () => {
      const path = '/tmp/path.pdf';

      exporter.setOutputPath(path);
      should(exporter.getOutputPath()).be.equal(path);
    });

    it('finish without crash', () => {
      should(new Promise(() => { exporter.finish(); })).not.throw();
    });
  });

  describe('Page component integration', () => {
    const i18n = {
      __: () => '{lang}',
      __n: () => '{lang2}',
      __mf: () => '{lang3}'
    };

    let exporter = null;

    beforeEach(() => {
      exporter = new PDFExporter();
    });

    it('renders page with header', () => {
      return new Promise((resolve) => {
        const handleExportData = (data) => {
          // simulate page-holder.jsx layer

          data.headerInfo = {
            type: 'class',
            name: '1A',
            tutorName: 'Smith John',
            exportDate: '11.06.2020',
            renderDate: '31.08.2020'
          };

          data.lastPage = true;

          // simulate export-preview.js layer
          const exportObj = {
            pageLayout: {
              relativeX: 0,
              relativeY: 0,
              relativeWidth: 2100,
              relativeHeight: 2900
            },
            contentLayout: data,
            isMulti: false
          };

          exporter.acceptPageData(exportObj);
          resolve();
        };

        mount(
          <Page
            i18n={i18n}
            hasHeader
            hasFooter
            fragmentData={JSON.parse(fs.readFileSync(path.join(__dirname, '../test_files/renderdata.json')).toString())}
            fragmentOutput={JSON.parse(fs.readFileSync(path.join(__dirname, '../test_files/renderdata2.json')).toString())}
            firstRow={0}
            lastRow={10}
            probeId={1}
            mono={false}
            isLastPage
            exportMode
            onExportDataReady={handleExportData}
            pageString='API TEST'
          />
        );
      });
    });

    it('renders page without header in mono', () => {
      const fragmentData = JSON.parse(fs.readFileSync(path.join(__dirname, '../test_files/renderdata.json')).toString());

      fragmentData.softConfig.appearanceOptions.mono = true;

      return new Promise((resolve) => {
        const handleExportData = (data) => {
          // simulate page-holder.jsx layer

          data.headerInfo = {
            type: 'class',
            name: '1A',
            tutorName: 'Smith John',
            exportDate: '11.06.2020',
            renderDate: '31.08.2020'
          };

          data.lastPage = true;

          // simulate export-preview.js layer
          const exportObj = {
            pageLayout: {
              relativeX: 0,
              relativeY: 0,
              relativeWidth: 2100,
              relativeHeight: 2900
            },
            contentLayout: data,
            isMulti: false
          };

          exporter.acceptPageData(exportObj);
          resolve();
        };

        mount(
          <Page
            i18n={i18n}
            hasHeader={false}
            hasFooter
            fragmentData={fragmentData}
            fragmentOutput={JSON.parse(fs.readFileSync(path.join(__dirname, '../test_files/renderdata2.json')).toString())}
            firstRow={0}
            lastRow={10}
            probeId={1}
            mono
            isLastPage
            exportMode
            onExportDataReady={handleExportData}
            pageString='API TEST'
          />
        );
      });
    });
  });
});
