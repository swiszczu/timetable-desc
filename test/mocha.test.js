/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * mocha.test.js - checks if unit test framework works correctly
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const should = require('should');

// Mocha test framework declarations:
/* global describe, it */

async function testAssert () {
  return 2 + 2 * 2;
}

describe('Test framework', () => {
  it('Truth assert', () => {
    should(2 + 2).be.exactly(4);
  });
  it('Async truth assert', async () => {
    should(await testAssert()).be.exactly(6);
  });
});
