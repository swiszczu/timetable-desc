/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * setup.js - provides setup for react components testing
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import sinon from 'sinon';
import { mount, render, shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

// Setup JSDOM

import { JSDOM } from 'jsdom';

configure({ adapter: new Adapter() });

global.sinon = sinon;

global.mount = mount;
global.render = render;
global.shallow = shallow;

const { window } = new JSDOM('<!doctype html><html><body><div id="app"></div></body></html>');

function copyProps (src, target) {
  const props = Object.getOwnPropertyNames(src)
    .filter(prop => typeof target[prop] === 'undefined')
    .reduce((result, prop) => ({
      ...result,
      [prop]: Object.getOwnPropertyDescriptor(src, prop)
    }), {});
  Object.defineProperties(target, props);
}

global.window = window;
global.document = window.document;
global.navigator = {
  userAgent: 'node.js'
};

// Mocking WebAPI

window.requestAnimationFrame = (mockCallback) => {
  mockCallback();
};

window.HTMLCanvasElement.prototype.getContext = () => {
  return { drawImage: sinon.stub() };
};

window.HTMLCanvasElement.prototype.toDataURL = () => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg==';

window.HTMLElement.prototype.getBoundingClientRect = () => {
  return {
    x: 0,
    y: -680.86669921875,
    width: 1590,
    height: 160.8333282470703,
    top: -680.86669921875,
    right: 1590,
    bottom: -520.0333709716797,
    left: 0
  };
};

window.Image.prototype.addEventListener = (listener, fun) => {
  if (listener === 'load') {
    fun({ target: null });
  }
};

copyProps(window, global);
