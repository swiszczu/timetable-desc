/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * renderer.test.js - tests TimetableRenderer object
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const should = require('should');
const fs = require('fs');
const path = require('path');

const TimetableParser = require('../src/backend/parser');
const TimetableRenderer = require('../src/backend/renderer');

// Mocha test framework declarations:
/* global describe, it, before, afterEach, beforeEach */

describe('Timetable renderer tests', () => {
  let renderer = null;
  let parser = null;

  before(() => {
    parser = new TimetableParser();

    const contents = fs.readFileSync(
      path.resolve('test_files/cards.xml'));

    return parser.parseTimetable(contents);
  });

  it('create renderer object', () => {
    should(() => { renderer = new TimetableRenderer(); }).not.throw();
  });

  it('setup parameters', () => {
    should(() => { renderer.setParserResult(parser.timetableInfo); }).not.throw();
    should(() => { renderer.setFileInfo(parser.fileInfo); }).not.throw();
  });

  it('class name normalizer returns correct abbreviations', () => {
    renderer.normalizeClassListName(['class 1A']).should.equal('class 1A');
    renderer.normalizeClassListName(['1A', '1B', '1C']).should.equal('1ABC');
    renderer.normalizeClassListName(['class 1A', 'class 1B']).should.equal('class 1AB');
    renderer.normalizeClassListName(['1AG', '1BG', '1CG']).should.equal('1ABCG');
    renderer.normalizeClassListName(['1A', '1A']).should.equal('1A');
    renderer.normalizeClassListName([]).should.equal('');
    renderer.normalizeClassListName(['1A', '2B', '2C']).should.equal('1A2B2C');
    renderer.normalizeClassListName(['1A', '2A']).should.equal('1A2A');
    renderer.normalizeClassListName(['1AG', '2AG']).should.equal('1AG2AG');
    renderer.normalizeClassListName(['2AL', '2B']).should.equal('2ALB');
    renderer.normalizeClassListName(['2AL', '2B', '']).should.equal('2ALB');
  });

  describe('Rendering tests', () => {
    const defaultConfig = fs.readFileSync(
      path.resolve('test_files/default_configuration.json')).toString();
    const renameConfig = fs.readFileSync(
      path.resolve('test_files/renaming_configuration.json')).toString();

    beforeEach((done) => {
      renderer = new TimetableRenderer();
      renderer.setParserResult(parser.timetableInfo);
      renderer.setFileInfo(parser.fileInfo);
      done();
    });

    it('handle incorrect ID for class', async () => {
      const renderConfig = {
        softConfig: JSON.parse(defaultConfig),
        renderData: {
          type: 'class',
          condensed: false,
          multi: false,
          id: 'INVALID_ID',
          ascttVersion: parser.timetableInfo.ascVersion
        }
      };

      renderer.setRenderConfiguration(renderConfig);

      await renderer.process();

      const renderData = renderer.getRenderData();

      should(renderData).be.an.Array().and.have.lengthOf(1);
      should(renderData[0]).have.properties([
        'data',
        'headerInfo',
        'cards'
      ]);

      renderData[0].headerInfo.type.should.equal('class');
      renderData[0].headerInfo.name.should.equal('???');
      renderData[0].headerInfo.tutorName.should.equal('???');

      Object.keys(renderData[0].cards).forEach(cardId => {
        const cardArr = renderData[0].cards[cardId];

        cardArr.forEach(cardArr2 => {
          cardArr2.should.be.an.empty().Array();
        });
      });

      renderData[0].data.forEach((period, i) => {
        period.should.eql({
          startTime: parser.timetableInfo.periodArr[i].startTime,
          endTime: parser.timetableInfo.periodArr[i].endTime,
          number: parser.timetableInfo.periodArr[i].name
        });
      });
    });

    it('handle incorrect ID for teacher', async () => {
      const renderConfig = {
        softConfig: JSON.parse(defaultConfig),
        renderData: {
          type: 'teacher',
          condensed: false,
          multi: false,
          id: 'INVALID_ID',
          ascttVersion: parser.timetableInfo.ascVersion
        }
      };

      renderer.setRenderConfiguration(renderConfig);

      await renderer.process();

      const renderData = renderer.getRenderData();

      should(renderData).be.an.Array().and.have.lengthOf(1);
      should(renderData[0]).have.properties([
        'data',
        'headerInfo',
        'cards'
      ]);

      renderData[0].headerInfo.type.should.equal('teacher');
      renderData[0].headerInfo.name.should.equal('???');

      Object.keys(renderData[0].cards).forEach(cardId => {
        const cardArr = renderData[0].cards[cardId];

        cardArr.forEach(cardArr2 => {
          cardArr2.should.be.an.empty().Array();
        });
      });

      renderData[0].data.forEach((period, i) => {
        period.should.eql({
          startTime: parser.timetableInfo.periodArr[i].startTime,
          endTime: parser.timetableInfo.periodArr[i].endTime,
          number: parser.timetableInfo.periodArr[i].name
        });
      });
    });

    it('handle incorrect ID for classroom', async () => {
      const renderConfig = {
        softConfig: JSON.parse(defaultConfig),
        renderData: {
          type: 'classroom',
          condensed: false,
          multi: false,
          id: 'INVALID_ID',
          ascttVersion: parser.timetableInfo.ascVersion
        }
      };

      renderer.setRenderConfiguration(renderConfig);

      await renderer.process();

      const renderData = renderer.getRenderData();

      should(renderData).be.an.Array().and.have.lengthOf(1);
      should(renderData[0]).have.properties([
        'data',
        'headerInfo',
        'cards'
      ]);

      renderData[0].headerInfo.type.should.equal('classroom');
      renderData[0].headerInfo.name.should.equal('???');

      Object.keys(renderData[0].cards).forEach(cardId => {
        const cardArr = renderData[0].cards[cardId];

        cardArr.forEach(cardArr2 => {
          cardArr2.should.be.an.empty().Array();
        });
      });

      renderData[0].data.forEach((period, i) => {
        period.should.eql({
          startTime: parser.timetableInfo.periodArr[i].startTime,
          endTime: parser.timetableInfo.periodArr[i].endTime,
          number: parser.timetableInfo.periodArr[i].name
        });
      });
    });

    it('render single class', async () => {
      const renderConfig = {
        softConfig: JSON.parse(renameConfig),
        renderData: {
          type: 'class',
          condensed: false,
          multi: false,
          id: Object.keys(parser.timetableInfo.classAssoc)[0],
          ascttVersion: parser.timetableInfo.ascVersion
        }
      };

      const classInfo = parser.timetableInfo.classAssoc[renderConfig.renderData.id];
      const classTutor = parser.timetableInfo.teacherAssoc[classInfo.teacherId];

      renderer.setRenderConfiguration(renderConfig);

      await renderer.process();

      const renderData = renderer.getRenderData();

      should(renderData).be.an.Array().and.have.lengthOf(1);
      should(renderData[0]).have.properties([
        'data',
        'headerInfo',
        'cards'
      ]);

      renderData[0].headerInfo.name.should.equal(classInfo.name);
      renderData[0].headerInfo.tutorName.should.equal(classTutor.name);
      renderData[0].headerInfo.type.should.equal('class');

      renderData[0].data.forEach((period, i) => {
        period.should.eql({
          startTime: parser.timetableInfo.periodArr[i].startTime,
          endTime: parser.timetableInfo.periodArr[i].endTime,
          number: parser.timetableInfo.periodArr[i].name
        });
      });

      Object.keys(renderData[0].cards).forEach(periodId => {
        const periodArr = renderData[0].cards[periodId];
        periodArr.should.be.an.Array();

        periodArr.forEach(dayArr => {
          dayArr.should.be.an.Array();

          dayArr.forEach(card => {
            card.should.be.an.Object().and.have.properties([
              'subjectName',
              'isEmpty',
              'colorRGBs',
              'teacherNames',
              'classroomNames',
              'groupNames'
            ]);
          });
        });
      });
    });

    it('render single teacher', async () => {
      const renderConfig = {
        softConfig: JSON.parse(renameConfig),
        renderData: {
          type: 'teacher',
          condensed: false,
          multi: false,
          id: Object.keys(parser.timetableInfo.teacherAssoc)[0],
          ascttVersion: parser.timetableInfo.ascVersion
        }
      };

      const teacherInfo = parser.timetableInfo.teacherAssoc[renderConfig.renderData.id];

      renderer.setRenderConfiguration(renderConfig);

      await renderer.process();

      const renderData = renderer.getRenderData();

      should(renderData).be.an.Array().and.have.lengthOf(1);
      should(renderData[0]).have.properties([
        'data',
        'headerInfo',
        'cards'
      ]);

      renderData[0].headerInfo.name.should.equal(teacherInfo.name);
      renderData[0].headerInfo.type.should.equal('teacher');

      renderData[0].data.forEach((period, i) => {
        period.should.eql({
          startTime: parser.timetableInfo.periodArr[i].startTime,
          endTime: parser.timetableInfo.periodArr[i].endTime,
          number: parser.timetableInfo.periodArr[i].name
        });
      });

      Object.keys(renderData[0].cards).forEach(periodId => {
        const periodArr = renderData[0].cards[periodId];
        periodArr.should.be.an.Array();

        periodArr.forEach(dayArr => {
          dayArr.should.be.an.Array();

          dayArr.forEach(card => {
            card.should.be.an.Object().and.have.properties([
              'subjectName',
              'isEmpty',
              'colorRGBs',
              'teacherNames',
              'classroomNames',
              'groupNames'
            ]);
          });
        });
      });
    });

    it('render single classroom', async () => {
      const renderConfig = {
        softConfig: JSON.parse(renameConfig),
        renderData: {
          type: 'classroom',
          condensed: false,
          multi: false,
          id: Object.keys(parser.timetableInfo.classroomAssoc)[0],
          ascttVersion: parser.timetableInfo.ascVersion
        }
      };

      const classroomInfo = (parser.timetableInfo
        .classroomAssoc[renderConfig.renderData.id]);

      renderer.setRenderConfiguration(renderConfig);

      await renderer.process();

      const renderData = renderer.getRenderData();

      should(renderData).be.an.Array().and.have.lengthOf(1);
      should(renderData[0]).have.properties([
        'data',
        'headerInfo',
        'cards'
      ]);

      renderData[0].headerInfo.name.should.equal(classroomInfo.name);
      renderData[0].headerInfo.type.should.equal('classroom');

      renderData[0].data.forEach((period, i) => {
        period.should.eql({
          startTime: parser.timetableInfo.periodArr[i].startTime,
          endTime: parser.timetableInfo.periodArr[i].endTime,
          number: parser.timetableInfo.periodArr[i].name
        });
      });

      Object.keys(renderData[0].cards).forEach(periodId => {
        const periodArr = renderData[0].cards[periodId];
        periodArr.should.be.an.Array();

        periodArr.forEach(dayArr => {
          dayArr.should.be.an.Array();

          dayArr.forEach(card => {
            card.should.be.an.Object().and.have.properties([
              'subjectName',
              'isEmpty',
              'colorRGBs',
              'teacherNames',
              'classroomNames',
              'groupNames'
            ]);
          });
        });
      });
    });

    it('render multiple classes', async () => {
      const renderConfig = {
        softConfig: JSON.parse(renameConfig),
        renderData: {
          type: 'class',
          condensed: false,
          multi: false,
          id: null,
          ascttVersion: parser.timetableInfo.ascVersion
        }
      };

      renderer.setRenderConfiguration(renderConfig);

      await renderer.process();

      const renderData = renderer.getRenderData();

      should(renderData).be.an.Array().and.have.lengthOf(3);

      const classKeys = Object.keys(parser.timetableInfo.classAssoc);

      renderData.forEach((rd, i) => {
        const classInfo = parser.timetableInfo.classAssoc[classKeys[i]];
        const classTutor = parser.timetableInfo.teacherAssoc[classInfo.teacherId];

        should(rd).have.properties([
          'data',
          'headerInfo',
          'cards'
        ]);

        rd.headerInfo.name.should.equal(classInfo.name);
        rd.headerInfo.tutorName.should.equal(classTutor.name);
        rd.headerInfo.type.should.equal('class');

        rd.data.forEach((period, i) => {
          period.should.eql({
            startTime: parser.timetableInfo.periodArr[i].startTime,
            endTime: parser.timetableInfo.periodArr[i].endTime,
            number: parser.timetableInfo.periodArr[i].name
          });
        });

        Object.keys(rd.cards).forEach(periodId => {
          const periodArr = rd.cards[periodId];
          periodArr.should.be.an.Array();

          periodArr.forEach(dayArr => {
            dayArr.should.be.an.Array();

            dayArr.forEach(card => {
              card.should.be.an.Object().and.have.properties([
                'subjectName',
                'isEmpty',
                'colorRGBs',
                'teacherNames',
                'classroomNames',
                'groupNames'
              ]);
            });
          });
        });
      });
    });

    it('render multiple teachers', async () => {
      const renderConfig = {
        softConfig: JSON.parse(renameConfig),
        renderData: {
          type: 'teacher',
          condensed: false,
          multi: false,
          id: null,
          ascttVersion: parser.timetableInfo.ascVersion
        }
      };

      renderer.setRenderConfiguration(renderConfig);

      await renderer.process();

      const renderData = renderer.getRenderData();

      should(renderData).be.an.Array().and.have.lengthOf(3);

      const teacherKeys = Object.keys(parser.timetableInfo.teacherAssoc);

      renderData.forEach((rd, i) => {
        const teacherInfo = parser.timetableInfo.teacherAssoc[teacherKeys[i]];

        should(rd).have.properties([
          'data',
          'headerInfo',
          'cards'
        ]);

        rd.headerInfo.name.should.equal(teacherInfo.name);
        rd.headerInfo.type.should.equal('teacher');

        rd.data.forEach((period, i) => {
          period.should.eql({
            startTime: parser.timetableInfo.periodArr[i].startTime,
            endTime: parser.timetableInfo.periodArr[i].endTime,
            number: parser.timetableInfo.periodArr[i].name
          });
        });

        Object.keys(rd.cards).forEach(periodId => {
          const periodArr = rd.cards[periodId];
          periodArr.should.be.an.Array();

          periodArr.forEach(dayArr => {
            dayArr.should.be.an.Array();

            dayArr.forEach(card => {
              card.should.be.an.Object().and.have.properties([
                'subjectName',
                'isEmpty',
                'colorRGBs',
                'teacherNames',
                'classroomNames',
                'groupNames'
              ]);
            });
          });
        });
      });
    });

    it('render multiple classrooms', async () => {
      const renderConfig = {
        softConfig: JSON.parse(renameConfig),
        renderData: {
          type: 'classroom',
          condensed: false,
          multi: false,
          id: null,
          ascttVersion: parser.timetableInfo.ascVersion
        }
      };

      renderer.setRenderConfiguration(renderConfig);

      await renderer.process();

      const renderData = renderer.getRenderData();

      should(renderData).be.an.Array().and.have.lengthOf(3);

      const classroomKeys = Object.keys(parser.timetableInfo.classroomAssoc);

      renderData.forEach((rd, i) => {
        const classroomInfo = parser.timetableInfo.classroomAssoc[classroomKeys[i]];

        should(rd).have.properties([
          'data',
          'headerInfo',
          'cards'
        ]);

        rd.headerInfo.name.should.equal(classroomInfo.name);
        rd.headerInfo.type.should.equal('classroom');

        rd.data.forEach((period, i) => {
          period.should.eql({
            startTime: parser.timetableInfo.periodArr[i].startTime,
            endTime: parser.timetableInfo.periodArr[i].endTime,
            number: parser.timetableInfo.periodArr[i].name
          });
        });

        Object.keys(rd.cards).forEach(periodId => {
          const periodArr = rd.cards[periodId];
          periodArr.should.be.an.Array();

          periodArr.forEach(dayArr => {
            dayArr.should.be.an.Array();

            dayArr.forEach(card => {
              card.should.be.an.Object().and.have.properties([
                'subjectName',
                'isEmpty',
                'colorRGBs',
                'teacherNames',
                'classroomNames',
                'groupNames'
              ]);
            });
          });
        });
      });
    });

    afterEach((done) => {
      renderer = null;
      done();
    });
  });
});
