/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* global describe it shallow sinon */
import React from 'react';
import { ActionButton } from '../../src/components/action-button.jsx';

const should = require('should');

describe('ActionButton', () => {
  it('renders text', () => {
    const testString = 'Hello world!';

    const wrapper = shallow(<ActionButton text={testString} />);
    should(wrapper.find('.ab-text').text()).equal(testString);
  });

  it('sets class name', () => {
    const wrapper = shallow(<ActionButton className='test_class' />);
    should(wrapper.hasClass('test_class')).be.true();
  });

  it('sets image path', () => {
    const wrapper = shallow(<ActionButton imagePath='test.jpg' />);
    should(wrapper.find('img').props().src).equal('test.jpg');
  });

  it('has tabindex', () => {
    const wrapper = shallow(<ActionButton />);
    should(wrapper.props()).have.keys('tabIndex');
  });

  it('responds to click and keyboard', () => {
    const mock = sinon.spy();

    const wrapper = shallow(<ActionButton onClick={mock} />);
    wrapper.simulate('click');
    wrapper.simulate('keypress', { key: 'Enter' });

    mock.calledTwice.should.be.true();

    wrapper.simulate('keypress', { key: 'F' });

    mock.calledTwice.should.be.true();
  });
});
