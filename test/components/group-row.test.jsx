/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* global describe it shallow sinon */
import React from 'react';
import { GroupRow } from '../../src/components/group-row.jsx';

const should = require('should');

describe('GroupRow', () => {
  it('renders group name', () => {
    const wrapper = shallow(<GroupRow groupName='Group1' />);

    should(wrapper.find('p').text()).equal('Group1');
  });

  it('highlights on active', () => {
    const wrapper = shallow(<GroupRow groupName='Group1' active />);

    should(wrapper.hasClass('active')).be.true();
  });

  it('handles click and keyboard', () => {
    const mock = sinon.spy();

    const wrapper = shallow(<GroupRow groupName='Group1' onToggle={mock} />);
    wrapper.simulate('click');

    should(mock.calledOnce).be.true();

    wrapper.simulate('keypress', { key: 'Enter' });

    should(mock.calledTwice).be.true();

    wrapper.simulate('keypress', { key: 'X' });

    should(mock.calledThrice).be.false();
  });
});
