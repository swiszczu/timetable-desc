/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* global describe it shallow sinon beforeEach */
import React from 'react';
import { GradeSelector } from '../../src/components/grade-selector.jsx';

const should = require('should');

describe('GradeSelector', () => {
  let wrapper = null;
  let mock = null;

  const grades = ['String1', 'String2', 'String3'];

  beforeEach(() => {
    wrapper = shallow(<GradeSelector label='Test string' grades={grades} defaultGrade={1} />);
    mock = sinon.spy();
  });

  it('sets correct label text', () => {
    should(wrapper.find('.title').text()).equal('Test string');
  });

  it('preselects correct entry', () => {
    should(wrapper.find('.grade-selector').childAt(0).hasClass('active')).be.false();
    should(wrapper.find('.grade-selector').childAt(1).hasClass('active')).be.true();
    should(wrapper.find('.grade-selector').childAt(2).hasClass('active')).be.false();
  });

  it('changes entry using mouse and keyboard', () => {
    wrapper.setProps({ onChange: mock });

    wrapper.find('.grade-selector').childAt(0).simulate('click');

    should(mock.calledOnce).be.true();
    should(mock.firstCall.args[0]).equal(0);

    wrapper.find('.grade-selector').childAt(2).simulate('keypress', { key: 'Enter' });

    should(mock.calledTwice).be.true();
    should(mock.secondCall.args[0]).equal(2);
  });

  it('does not react to any other key', () => {
    wrapper.setProps({ onChange: mock });
    wrapper.find('.grade-selector').childAt(2).simulate('keypress', { key: 'X' });

    should(mock.notCalled).be.true();
  });
});
