/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* global describe it shallow sinon */
import React from 'react';
import { Checkbox } from '../../src/components/checkbox.jsx';

const should = require('should');

describe('Checkbox', () => {
  it('renders label', () => {
    const testString = 'Checkbox test label';
    const wrapper = shallow(<Checkbox checked={false} text={testString} />);

    should(wrapper.find('.select-text').text()).equal(testString);
  });

  it('changes state on click and sends callback', () => {
    const mock = sinon.spy();

    let wrapper = shallow(<Checkbox checked={false} onChange={mock} />);
    wrapper.find('input').simulate('change', { target: { checked: true } });

    should(mock.firstCall.args[0]).be.true();

    wrapper = shallow(<Checkbox checked onChange={mock} />);
    should(wrapper.find('input').props().checked).be.true();
    wrapper.find('input').simulate('change', { target: { checked: false } });

    should(mock.secondCall.args[0]).be.false();

    mock.calledTwice.should.be.true();

    wrapper = shallow(<Checkbox checked />);

    should(wrapper.find('input').simulate('change',
      { target: { checked: false } })).not.throw();
  });
});
