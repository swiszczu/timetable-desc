/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* global describe it shallow sinon beforeEach */
import React from 'react';
import { Button } from '../../src/components/button.jsx';
import { ColorProperty } from '../../src/components/color-property.jsx';
import { PhotoshopPicker } from '../../lib/react-color';
import Modal from 'react-modal';

const should = require('should');

describe('ColorProperty', () => {
  let wrapper = null;
  let mock = null;

  beforeEach(() => {
    wrapper = shallow(<ColorProperty label='Test' color='#FF0000' />);
    mock = sinon.spy();
  });

  it('renders correct color', () => {
    should(wrapper.find('.color-def').props().style.backgroundColor).equal('#FF0000');
  });

  it('opens modal on click', () => {
    wrapper.find(Button).dive().find('.button-inner').simulate('click');
    wrapper.find(Modal).dive();

    should(wrapper.find(Modal).props().isOpen).be.true();
  });

  it('handles color change', () => {
    wrapper.find(Modal).dive().find(PhotoshopPicker).dive();
    wrapper.find(PhotoshopPicker).props().onChange({ hex: '#00FF00' });

    wrapper.setProps({ onChange: mock });

    wrapper.find(PhotoshopPicker).props().onChange({ hex: '#0000FF' });
    should(mock.calledOnce).be.true();
    should(mock.firstCall.args[0]).equal('#0000FF');
  });

  it('handles cancelling', () => {
    wrapper.setProps({ onChange: mock });

    wrapper.find(Button).dive().find('.button-inner').simulate('click');

    wrapper.find(Modal).dive().find(PhotoshopPicker).dive();
    wrapper.find(PhotoshopPicker).props().onChange({ hex: '#00FF00' });

    should(mock.calledOnce).be.true();

    wrapper.setProps({ color: '#00FF00 ' });
    wrapper.find(PhotoshopPicker).props().onCancel();

    should(mock.calledTwice).be.true();
    should(mock.secondCall.args[0]).equal('#FF0000'); // Original color
  });

  it('handles ok', () => {
    wrapper.find(Button).dive().find('.button-inner').simulate('click');
    wrapper.find(Modal).dive().find(PhotoshopPicker).dive();

    wrapper.find(PhotoshopPicker).props().onAccept();

    should(wrapper.find(Modal).props().isOpen).be.false();
  });

  it('handles close event', () => {
    wrapper.setProps({ onChange: mock });

    wrapper.find(Button).dive().find('.button-inner').simulate('click');

    wrapper.find(Modal).dive().find(PhotoshopPicker).dive();
    wrapper.find(PhotoshopPicker).props().onChange({ hex: '#00FF00' });

    should(mock.calledOnce).be.true();

    wrapper.setProps({ color: '#00FF00 ' });
    wrapper.find(Modal).props().onRequestClose();

    should(mock.calledTwice).be.true();
    should(mock.secondCall.args[0]).equal('#FF0000'); // Original color
  });
});
