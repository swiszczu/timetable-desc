/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* global describe it shallow */
import React from 'react';
import { Label } from '../../src/components/label.jsx';

const should = require('should');

describe('Label', () => {
  it('renders text', () => {
    const testString = 'Hello world! Foo!';

    const wrapper = shallow(<Label text={testString} />);
    should(wrapper.html().includes(testString)).be.true();
  });

  it('renders children', () => {
    const wrapper = shallow(<Label><p>A</p><p>B</p></Label>);

    should(wrapper.find('p')).have.lengthOf(2);
  });
});
