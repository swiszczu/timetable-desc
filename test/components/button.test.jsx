/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* global describe it shallow sinon */
import React from 'react';
import { Button } from '../../src/components/button.jsx';

const should = require('should');

describe('Button', () => {
  it('renders text', () => {
    const testString = 'Button test';
    const wrapper = shallow(<Button text={testString} />);

    should(wrapper.find('.button-inner').text()).equal(testString);
    should(wrapper.find('.button-anim').text()).equal(testString);
  });

  it('renders children', () => {
    const wrapper = shallow(<Button><p>Test</p></Button>);

    should(wrapper.find('.button-inner p').text()).equal('Test');
    should(wrapper.find('.button-anim p').text()).equal('Test');
  });

  it('has tabindex', () => {
    const wrapper = shallow(<Button />);
    should(wrapper.find('*[tabIndex]').props()).have.keys('tabIndex');
  });

  it('responds to click and keyboard', () => {
    const mock = sinon.spy();

    const wrapper = shallow(<Button onClick={mock} />);
    wrapper.find('.button-inner').simulate('click');
    wrapper.find('.button-inner').simulate('keypress', { key: 'Enter' });

    mock.calledTwice.should.be.true();
  });

  it('ignores events when disabled', () => {
    const mock = sinon.spy();

    const wrapper = shallow(<Button disabled onClick={mock} />);
    wrapper.find('.button-inner').simulate('click');
    wrapper.find('.button-inner').simulate('keypress', { key: 'Enter' });

    mock.notCalled.should.be.true();
  });

  it('supports small style', () => {
    const wrapper = shallow(<Button small />);
    should(wrapper.find('.button-small')).have.lengthOf(1);
  });
});
