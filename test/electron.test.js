/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * electron.test.js - checks that main electron window shows up correctly
 * and there are no runtime errors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const should = require('should');
const Application = require('spectron').Application;
const path = require('path');

// Mocha test framework declarations:
/* global describe, it, after, before */

if (process.env.CI !== 'true') {
  describe('Electron UI tests', async () => {
    const app = new Application({
      path: path.resolve(__dirname, '../node_modules/.bin/electron'),
      args: [path.resolve(__dirname, '../src/backend/main.js')],
      chromeDriverArgs: ['remote-debugging-port=9222']
    });

    before((done) => {
      app.start().then(() => done());
    });

    describe('Init', () => {
      it('make window visible', () => {
        should(app.browserWindow.isVisible()).be.ok();
      });

      it('window has title', () => {
        should(app.browserWindow.getTitle()).be.ok();
      });
    });

    describe('Content test', () => {
      it('there are no errors after 1s', async () => {
        await new Promise((resolve) => {
          setTimeout(() => { resolve(); }, 1000);
        });

        const out = await app.client.execute(() => {
          return document.getElementById('reportedErrors').getAttribute('content');
        });

        const errors = JSON.parse(out.value);

        errors.should.be.an.empty().Array();
      });
    });

    after((done) => {
      app.stop().then(() => done());
    });
  });
}
