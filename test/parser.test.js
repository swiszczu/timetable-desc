/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * parser.test.js - tests TimetableParser object
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const should = require('should');
const TimetableParser = require('../src/backend/parser');
const fs = require('fs');
const path = require('path');

// Mocha test framework declarations:
/* global describe, it, afterEach, beforeEach */

describe('ASCTT Parser tests', () => {
  let parser = null;

  it('create parser object', () => {
    should(() => { parser = new TimetableParser(); }).not.throw();
  });

  it('throw on broken binary file', () => {
    const contents = fs.readFileSync(
      path.resolve('test_files/broken.xml'));
    should(parser.parseTimetable(contents)).be.rejected();
  });

  it('throw on broken xml file', () => {
    const contents = fs.readFileSync(
      path.resolve('test_files/broken_xml.xml'));
    should(parser.parseTimetable(contents)).be.rejected();
  });

  it('change modified date', () => {
    const oldDate = parser.fileInfo.modified;
    parser.setFileModified(new Date(1234));
    parser.fileInfo.modified.should.not.equal(oldDate);
  });

  it('filter wrong xml nodes', async () => {
    const contents = fs.readFileSync(
      path.resolve('test_files/broken_nodes.xml'));
    await should(parser.parseTimetable(contents)).be.fulfilled();
  });

  it('period stats are valid', async () => {
    const contents = fs.readFileSync(
      path.resolve('test_files/cards.xml'));
    await should(parser.parseTimetable(contents)).be.fulfilled();

    parser.getPeriodStats().should.be.eql({
      1: 1,
      2: 1,
      3: 1,
      4: 3,
      8: 1,
      9: 2,
      10: 1
    });
  });

  it('cache buffer', async () => {
    const contents = fs.readFileSync(
      path.resolve('test_files/cards.xml'));

    parser.setXmlBuffer(contents);
    parser.getXmlBuffer().should.be.equal(contents);

    await should(parser.parseTimetable(parser.getXmlBuffer())).be.fulfilled();
  });

  describe('File sections', () => {
    beforeEach(() => {
      parser = new TimetableParser();
    });

    it('read asctt version', async () => {
      const contents = fs.readFileSync(
        path.resolve('test_files/subjects.xml'));
      await should(parser.parseTimetable(contents)).be.fulfilled();

      parser.timetableInfo.ascVersion.should.be.exactly('2020.9.1');
    });

    it('read subjects', async () => {
      const contents = fs.readFileSync(
        path.resolve('test_files/subjects.xml'));
      await should(parser.parseTimetable(contents)).be.fulfilled();

      (parser.timetableInfo.subjectAssoc.should.be.an.instanceOf(Object)
        .and.have.size(3));

      Object.keys(parser.timetableInfo.subjectAssoc).forEach((subKey, i) => {
        const subject = parser.timetableInfo.subjectAssoc[subKey];
        subject.name.should.be.exactly('subject ' + (i + 1));
        subject.short.should.be.exactly('s' + (i + 1));
      });
    });

    it('read teachers', async () => {
      const contents = fs.readFileSync(
        path.resolve('test_files/teachers.xml'));
      await should(parser.parseTimetable(contents)).be.fulfilled();

      (parser.timetableInfo.teacherAssoc.should.be.an.instanceOf(Object)
        .and.have.size(3));

      parser.timetableInfo.teacherAssoc.should.have.property('B4D784DD59BDC1B8');
      parser.timetableInfo.teacherAssoc.should.have.property('9636E316CA78CA0E');
      parser.timetableInfo.teacherAssoc.should.have.property('E5E2001E220D88B3');

      (parser.timetableInfo.teacherAssoc.B4D784DD59BDC1B8
        .name.should.equal('Smith John'));
      (parser.timetableInfo.teacherAssoc['9636E316CA78CA0E']
        .name.should.equal('Johnson Andrew'));
      (parser.timetableInfo.teacherAssoc.E5E2001E220D88B3
        .name.should.equal('Freund Anna'));

      (parser.timetableInfo.teacherAssoc.B4D784DD59BDC1B8
        .short.should.equal('JS'));
      (parser.timetableInfo.teacherAssoc['9636E316CA78CA0E']
        .short.should.equal('AJ'));
      (parser.timetableInfo.teacherAssoc.E5E2001E220D88B3
        .short.should.equal('AF'));

      (parser.timetableInfo.teacherAssoc.B4D784DD59BDC1B8
        .color.should.equal('#FF0000'));
      (parser.timetableInfo.teacherAssoc['9636E316CA78CA0E']
        .color.should.equal('#00FF00'));
      (parser.timetableInfo.teacherAssoc.E5E2001E220D88B3
        .color.should.equal('#0000FF'));
    });

    it('read classrooms', async () => {
      const contents = fs.readFileSync(
        path.resolve('test_files/classrooms.xml'));
      await should(parser.parseTimetable(contents)).be.fulfilled();

      (parser.timetableInfo.classroomAssoc.should.be.an.instanceOf(Object)
        .and.have.size(3));

      parser.timetableInfo.classroomAssoc.should.have.property('900BC60C884E075A');
      parser.timetableInfo.classroomAssoc.should.have.property('9B565FBB932B16DF');
      parser.timetableInfo.classroomAssoc.should.have.property('472E97E57BB15DDA');

      (parser.timetableInfo.classroomAssoc['900BC60C884E075A']
        .name.should.equal('10'));
      (parser.timetableInfo.classroomAssoc['9B565FBB932B16DF']
        .name.should.equal('11'));
      (parser.timetableInfo.classroomAssoc['472E97E57BB15DDA']
        .name.should.equal('12'));

      (parser.timetableInfo.classroomAssoc['900BC60C884E075A']
        .short.should.equal('10'));
      (parser.timetableInfo.classroomAssoc['9B565FBB932B16DF']
        .short.should.equal('11'));
      (parser.timetableInfo.classroomAssoc['472E97E57BB15DDA']
        .short.should.equal('12'));
    });

    it('read grades', async () => {
      const contents = fs.readFileSync(
        path.resolve('test_files/grades.xml'));
      await should(parser.parseTimetable(contents)).be.fulfilled();

      (parser.timetableInfo.gradeArr.should.be.an.instanceOf(Array)
        .and.have.lengthOf(20));

      for (let i = 0; i < 20; i++) {
        const gradeObj = parser.timetableInfo.gradeArr[i];
        gradeObj.should.be.ok();

        gradeObj.name.should.be.equal('Poziom ' + (i + 1));
        gradeObj.short.should.be.equal('G ' + (i + 1));
      }
    });

    it('read classes', async () => {
      const contents = fs.readFileSync(
        path.resolve('test_files/classes.xml'));
      await should(parser.parseTimetable(contents)).be.fulfilled();

      (parser.timetableInfo.classAssoc.should.be.an.instanceOf(Object)
        .and.have.size(4));

      parser.timetableInfo.classAssoc.should.have.property('09361AF63204C0C6');
      parser.timetableInfo.classAssoc.should.have.property('764912916F62DF90');
      parser.timetableInfo.classAssoc.should.have.property('7E14009BC815AAFE');

      (parser.timetableInfo.classAssoc['09361AF63204C0C6']
        .name.should.equal('1A'));
      (parser.timetableInfo.classAssoc['764912916F62DF90']
        .name.should.equal('1B'));
      (parser.timetableInfo.classAssoc['7E14009BC815AAFE']
        .name.should.equal('2A'));

      (parser.timetableInfo.classAssoc['09361AF63204C0C6']
        .short.should.equal('1A'));
      (parser.timetableInfo.classAssoc['764912916F62DF90']
        .short.should.equal('1B'));
      (parser.timetableInfo.classAssoc['7E14009BC815AAFE']
        .short.should.equal('2A'));

      parser.timetableInfo.classArrForGrade['1'].should.have.lengthOf(2);
      parser.timetableInfo.classArrForGrade['2'].should.have.lengthOf(1);

      Object.keys(parser.timetableInfo.classAssoc).forEach(classId => {
        const classObj = parser.timetableInfo.classAssoc[classId];

        parser.timetableInfo.teacherAssoc.should.have.property(classObj.teacherId);
      });

      parser.timetableInfo.classAssoc.should.have.property('broken');
      should(parser.timetableInfo.classAssoc.broken.gradeNumber).not.be.a.Number();
    });

    it('read groups', async () => {
      const contents = fs.readFileSync(
        path.resolve('test_files/groups.xml'));
      await should(parser.parseTimetable(contents)).be.fulfilled();

      (parser.timetableInfo.groupAssoc.should.be.an.instanceOf(Object)
        .and.have.size(9));

      Object.keys(parser.timetableInfo.groupAssoc).forEach(groupId => {
        const groupObj = parser.timetableInfo.groupAssoc[groupId];
        groupObj.name.should.be.equalOneOf('Entire class', 'Group1',
          'Group2', 'Group3');
        parser.timetableInfo.classAssoc.should.have.property(groupObj.classId);
      });
    });

    it('read lessons', async () => {
      const contents = fs.readFileSync(
        path.resolve('test_files/lessons.xml'));
      await should(parser.parseTimetable(contents)).be.fulfilled();

      (parser.timetableInfo.lessonAssoc.should.be.an.instanceOf(Object)
        .and.have.size(3));

      Object.keys(parser.timetableInfo.lessonAssoc).forEach(lessonId => {
        const lessonObj = parser.timetableInfo.lessonAssoc[lessonId];

        parser.timetableInfo.classAssoc.should.have.properties(
          lessonObj.classIdArr);
        parser.timetableInfo.subjectAssoc.should.have.property(
          lessonObj.subjectId);
        parser.timetableInfo.teacherAssoc.should.have.properties(
          lessonObj.teacherIdArr);
        parser.timetableInfo.classroomAssoc.should.have.properties(
          lessonObj.classroomIdArr);
        parser.timetableInfo.groupAssoc.should.have.properties(
          lessonObj.groupIdArr);
      });
    });

    it('read cards', async () => {
      const contents = fs.readFileSync(
        path.resolve('test_files/cards.xml'));
      await should(parser.parseTimetable(contents)).be.fulfilled();

      (parser.timetableInfo.cardArr.should.be.an.instanceOf(Array)
        .and.have.lengthOf(10));

      let minimumPeriod = 1e10;
      let maximumPeriod = -1;

      parser.timetableInfo.periodArr.forEach(period => {
        minimumPeriod = Math.min(parseInt(period.name), minimumPeriod);
        maximumPeriod = Math.max(parseInt(period.name), maximumPeriod);
      });

      parser.timetableInfo.cardArr.forEach(card => {
        parser.timetableInfo.lessonAssoc.should.have.property(card.lessonId);
        parser.timetableInfo.classroomAssoc.should.have.properties(
          card.classroomIdArr);

        card.day.should.be.a.Number();

        (card.period.should.be.greaterThanOrEqual(minimumPeriod)
          .and.lessThanOrEqual(maximumPeriod));
      });
    });

    afterEach(() => {
      parser = null;
    });
  });

  it('generate correct group renaming helper', async () => {
    parser = new TimetableParser();

    const contents = fs.readFileSync(
      path.resolve('test_files/cards.xml'));
    await should(parser.parseTimetable(contents)).be.fulfilled();

    const helper = parser.getDefaultRulesState();
    const correctObj = {
      1: {
        subjects: {
          '1FEEB2A7B878CD91': {
            id: '1FEEB2A7B878CD91',
            name: 'subject 1 (s1)',
            fullName: 'subject 1',
            groupNames: [],
            rules: []
          },
          '55975533D508769A': {
            id: '55975533D508769A',
            name: 'subject 2 (s2)',
            fullName: 'subject 2',
            groupNames: [
              'Group1',
              'Group2',
              'Group3'
            ],
            rules: []
          },
          '155EA7D345D37242': {
            id: '155EA7D345D37242',
            name: 'subject 3 (s3)',
            fullName: 'subject 3',
            groupNames: [],
            rules: []
          }
        }
      },
      2: {
        subjects: {
          '1FEEB2A7B878CD91': {
            id: '1FEEB2A7B878CD91',
            name: 'subject 1 (s1)',
            fullName: 'subject 1',
            groupNames: [],
            rules: []
          },
          '55975533D508769A': {
            id: '55975533D508769A',
            name: 'subject 2 (s2)',
            fullName: 'subject 2',
            groupNames: [
              'Group1',
              'Group2',
              'Group3'
            ],
            rules: []
          },
          '155EA7D345D37242': {
            id: '155EA7D345D37242',
            name: 'subject 3 (s3)',
            fullName: 'subject 3',
            groupNames: [
              'Group1',
              'Group3'
            ],
            rules: []
          }
        }
      }
    };

    helper.should.eql(correctObj);
  });
});
