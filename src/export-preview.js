/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * export-preview.js - main component for Export preview window
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Button } from './components/button.jsx';
import { Spinner } from './components/spinner.jsx';
import { PageHolder } from './components/page-holder.jsx';

const { ipcRenderer } = window.require('electron');

export class ExportPreviewWindow extends React.Component {
  constructor (props) {
    super(props);

    ipcRenderer.once('preview:rendered', (ev, data) => {
      ipcRenderer.send('preview:clear_global');

      this.setState({
        rendered: true,
        renderOutput: data
      });
    });

    ipcRenderer.send('preview:render', props.renderData);

    this.state = {
      exportMode: false,
      rendered: false,
      renderOutput: null,
      pageNumber: 1,
      zoomFactor: 1,
      pageCount: 1,
      pageInfo: null
    };

    this.keyDownBound = this.handleDocumentKeyDown.bind(this);
    this.keyUpBound = this.handleDocumentKeyUp.bind(this);
    this.mouseWheelBound = this.handleDocumentMouseWheel.bind(this);

    this.beginExportBound = this.handleExportBegin.bind(this);
    this.cancelExportBound = this.handleExportCancel.bind(this);

    this.pageContainerRef = React.createRef();

    this.exportObj = {
      pageLayout: null,
      contentLayout: null,
      isMulti: props.renderData.renderData.multi
    };

    this.pagesExported = [];
    this.isControlPressed = false;

    this.deltaAccumulator = 0;
  }

  trySendExportData () {
    let ok = true;
    Object.keys(this.exportObj).forEach(key => {
      if (this.exportObj[key] == null) {
        ok = false;
      }
    });

    if (ok) {
      if (!this.pagesExported[this.state.pageNumber]) {
        ipcRenderer.send('export:page', {
          progress: {
            current: this.state.pageNumber,
            total: this.state.pageCount
          },
          data: this.exportObj
        });
        this.pagesExported[this.state.pageNumber] = true;

        if (this.state.pageNumber < this.state.pageCount) {
          this.exportObj.contentLayout = null;
          setTimeout(() => {
            if (this.state.exportMode) {
              this.handlePageRightClick();
            }
          }, 0);
        } else {
          this.setState({
            exportMode: false
          }, () => {
            ipcRenderer.send('export:finish');
          });
        }
      }
    }
  }

  componentDidMount () {
    window.addEventListener('keydown', this.keyDownBound);
    window.addEventListener('keyup', this.keyUpBound);
    window.addEventListener('wheel', this.mouseWheelBound);

    ipcRenderer.addListener('export:begin', this.beginExportBound);
    ipcRenderer.addListener('export:progress:cancel', this.cancelExportBound);
  }

  componentDidUpdate () {
    window.requestAnimationFrame(() => {
      if (this.pageContainerRef.current) {
        const domRect = this.pageContainerRef.current.getBoundingClientRect();

        const pageLayout = {
          relativeX: domRect.x,
          relativeY: domRect.y,
          relativeWidth: domRect.width,
          relativeHeight: domRect.height
        };

        this.exportObj.pageLayout = pageLayout;
        this.exportObj.themeColors = (this.props.renderData.softConfig
          .appearanceOptions.themeColors);
        this.trySendExportData();
      }
    });
  }

  componentWillUnmount () {
    window.removeEventListener('keydown', this.keyDownBound);
    window.removeEventListener('keyup', this.keyUpBound);
    window.removeEventListener('wheel', this.mouseWheelBound);

    ipcRenderer.removeListener('export:begin', this.beginExportBound);
    ipcRenderer.removeListener('export:progress:cancel', this.cancelExportBound);
  }

  handleExportBegin () {
    this.pagesExported = [];
    this.exportObj = {
      pageLayout: null,
      contentLayout: null,
      isMulti: this.props.renderData.renderData.multi
    };
    this.setState({
      pageNumber: 1,
      exportMode: true
    });
  }

  handleExportCancel () {
    this.setState({
      exportMode: false
    });
  }

  handleExportDataReady (data) {
    this.exportObj.contentLayout = data;
    this.trySendExportData();
  }

  handleDocumentKeyDown (event) {
    if (event.keyCode === 37) { // Left arrow
      this.handlePageLeftClick();
    } else if (event.keyCode === 39) { // Right arrow
      this.handlePageRightClick();
    } else if (event.keyCode === 17) { // Ctrl
      this.isControlPressed = true;
    }
  }

  handleDocumentKeyUp (event) {
    if (event.keyCode === 17) { // Ctrl
      this.isControlPressed = false;
    }
  }

  handleDocumentMouseWheel (event) {
    if (event.ctrlKey) {
      this.deltaAccumulator += event.deltaY;
      if (this.deltaAccumulator < -60) {
        while (this.deltaAccumulator < -60) {
          this.handleZoomInClick();
          this.deltaAccumulator += 60;
        }
        this.deltaAccumulator = 0;
      } else if (this.deltaAccumulator > 60) {
        while (this.deltaAccumulator > 60) {
          this.handleZoomOutClick();
          this.deltaAccumulator -= 60;
        }
        this.deltaAccumulator = 0;
      }
    }
  }

  handleExportClick () {
    ipcRenderer.send('export:start', {
      metadata: {
        title: this.props.renderData.softConfig.ioOptions.pdfTitle,
        author: this.props.renderData.softConfig.ioOptions.pdfAuthor,
        subject: this.props.renderData.softConfig.ioOptions.pdfSubject
      },
      pageCount: this.state.pageCount,
      softConfig: this.props.renderData.softConfig,
      xmlFilePath: this.props.renderData.xmlFilePath
    });
  }

  handlePageInputChange (event) {
    let curval;
    try {
      curval = parseInt(event.target.value);
    } catch (e) { curval = 1; }
    if (!curval) curval = 1;
    if (curval < 1) curval = 1;
    if (curval > this.state.pageCount) curval = this.state.pageCount;

    this.setState({ pageNumber: curval });
  }

  handlePageLeftClick () {
    this.setState(state => {
      let newPage = state.pageNumber - 1;

      if (newPage < 1) newPage = 1;
      return {
        pageNumber: newPage
      };
    });
  }

  handlePageRightClick () {
    this.setState(state => {
      let newPage = state.pageNumber + 1;

      if (newPage > state.pageCount) newPage = state.pageCount;
      return {
        pageNumber: newPage
      };
    });
  }

  handleZoomOutClick () {
    this.setState(state => {
      let newZoom = state.zoomFactor - 0.1;

      if (newZoom < 0.4) newZoom = 0.4;

      return {
        zoomFactor: newZoom
      };
    });
  }

  handleZoomInClick () {
    this.setState(state => {
      let newZoom = state.zoomFactor + 0.1;

      if (newZoom > 4) newZoom = 4;

      return {
        zoomFactor: newZoom
      };
    });
  }

  handlePageCountReady (pageCount) {
    this.setState({
      pageCount: pageCount.totalPages,
      pageInfo: pageCount
    });
  }

  render () {
    if (!this.state.rendered) {
      return (
        <Spinner />
      );
    }

    return (
      <div className='vertical-flex'>
        <div className='preview-toolbar'>
          <Button
            text={this.props.i18n.__('Export PDF file') + '...'}
            onClick={this.handleExportClick.bind(this)}
          />

          &nbsp; <div className='toolbar-divider' /> &nbsp;

          {this.props.i18n.__('Page')}

          &nbsp;&nbsp;

          <input
            type='text'
            style={{
              width: '32px'
            }}
            className='text-field'
            onChange={this.handlePageInputChange.bind(this)}
            value={this.state.pageNumber}
          />

          &nbsp; {this.props.i18n.__('of')} &nbsp;

          <input
            type='text'
            style={{
              width: '32px'
            }}
            className='text-field'
            disabled
            value={this.state.pageCount}
          />

          &nbsp; &nbsp;

          <Button
            text='<'
            onClick={this.handlePageLeftClick.bind(this)}
            disabled={this.state.pageNumber === 1}
            title={this.props.i18n.__('Jump to previous page')}
          />

          &nbsp;

          <Button
            text='>'
            onClick={this.handlePageRightClick.bind(this)}
            disabled={this.state.pageNumber === this.state.pageCount}
            title={this.props.i18n.__('Jump to next page')}
          />

          &nbsp; <div className='toolbar-divider' /> &nbsp;

          {this.props.i18n.__('Zoom')}:

          &nbsp;&nbsp;

          <Button
            text='-'
            onClick={this.handleZoomOutClick.bind(this)}
            disabled={this.state.zoomFactor === 0.4}
            title={this.props.i18n.__('Zoom out')}
          />

          &nbsp;
          <span className='zoom-text'>
            {Math.round(this.state.zoomFactor * 100)}%
          </span>
          &nbsp;

          <Button
            text='+'
            onClick={this.handleZoomInClick.bind(this)}
            disabled={this.state.zoomFactor === 4}
            title={this.props.i18n.__('Zoom in')}
          />

        </div>
        <div className='preview-window expand-vert'>
          <div
            className='preview-page' ref={this.pageContainerRef} style={{
              zoom: this.state.zoomFactor,
              paddingLeft: this.props.renderData.softConfig.appearanceOptions.margins.left + 'cm',
              paddingRight: this.props.renderData.softConfig.appearanceOptions.margins.right + 'cm',
              paddingTop: this.props.renderData.softConfig.appearanceOptions.margins.top + 'cm',
              paddingBottom: this.props.renderData.softConfig.appearanceOptions.margins.bottom + 'cm'
            }}
          >
            <div className='page-content'>
              <PageHolder
                key={this.state.exportMode}
                i18n={this.props.i18n}
                exportMode={this.state.exportMode}
                fragmentData={this.props.renderData}
                fragmentOutput={this.state.renderOutput}
                onPageCountReady={this.handlePageCountReady.bind(this)}
                onExportDataReady={this.handleExportDataReady.bind(this)}
                pageNumber={this.state.pageNumber}
                pageInfo={this.state.pageInfo}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ExportPreviewWindow.propTypes = {
  i18n: PropTypes.object,
  renderData: PropTypes.object
};
