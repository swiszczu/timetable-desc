/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * pdf-exporter.js - generates PDF document using received layout information
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const PDFDocument = require('pdfkit');
const fs = require('fs');
const path = require('path');

const ver = require('./version');

class PDFExporter {
  constructor () {
    this.doc = new PDFDocument({
      autoFirstPage: false,
      margin: 0,
      size: 'a4',
      pdfVersion: '1.4'
    });

    this.doc.registerFont('lato300',
      path.resolve(__dirname, '../../dist/fonts/lato-v16-latin_latin-ext-300.ttf'));
    this.doc.registerFont('lato300i',
      path.resolve(__dirname, '../../dist/fonts/lato-v16-latin_latin-ext-300italic.ttf'));
    this.doc.registerFont('lato400',
      path.resolve(__dirname, '../../dist/fonts/lato-v16-latin_latin-ext-regular.ttf'));
    this.doc.registerFont('lato400i',
      path.resolve(__dirname, '../../dist/fonts/lato-v16-latin_latin-ext-italic.ttf'));
    this.doc.registerFont('lato700',
      path.resolve(__dirname, '../../dist/fonts/lato-v16-latin_latin-ext-700.ttf'));
    this.doc.registerFont('lato700i',
      path.resolve(__dirname, '../../dist/fonts/lato-v16-latin_latin-ext-700i.ttf'));

    this.doc.font('lato400');

    this.hasAnyPage = false;
    this.outputPath = null;
  }

  setDocumentMeta (title, author, subject) {
    this.doc.info.Title = title;
    this.doc.info.Author = author;
    this.doc.info.Subject = subject;
    this.doc.info.Creator = ('Timetable DESC ' + ver + ' (via ' +
      this.doc.info.Creator + ')');
  }

  setOutputPath (path) {
    this.outputPath = path;
  }

  getOutputPath () {
    return this.outputPath;
  }

  acceptPageData (pageData) {
    this.doc.addPage();
    this.hasAnyPage = true;
    // this.doc.text(JSON.stringify(pageData));

    const themeColors = { ...pageData.themeColors };

    if (pageData.contentLayout.headerLayout.isMono) {
      themeColors.accent = '#000000';
      themeColors.mainText = '#000000';
      themeColors.secondaryText = '#000000';
      themeColors.mainGrid = '#000000';
      themeColors.lessonTime = '#000000';
      themeColors.lessonDiv = '#000000';
    }

    const pageLayout = pageData.pageLayout;
    this.handleHeader(pageLayout, pageData.contentLayout.headerLayout, themeColors);
    this.handleTableHead(pageLayout, pageData.contentLayout.tableHeaderLayout, themeColors);
    this.handleTableData(pageLayout, pageData.contentLayout.tableLayout, themeColors);
    this.handleFooter(pageLayout, pageData.contentLayout.footerLayout, themeColors);
  }

  normalizePosition (pageLayout, coordX, coordY) {
    let tempX = coordX;
    let tempY = coordY;

    tempX -= pageLayout.relativeX;
    tempY -= pageLayout.relativeY;

    const xPt = 595;
    const yPt = 842;

    const xFactor = xPt / pageLayout.relativeWidth;
    const yFactor = yPt / pageLayout.relativeHeight;

    return { x: tempX * xFactor, y: tempY * yFactor };
  }

  normalizeSize (pageLayout, coordWidth, coordHeight) {
    const tempX = coordWidth;
    const tempY = coordHeight;

    const xPt = 595;
    const yPt = 842;

    const xFactor = xPt / pageLayout.relativeWidth;
    const yFactor = yPt / pageLayout.relativeHeight;

    return { w: tempX * xFactor, h: tempY * yFactor };
  }

  handleHeader (pageLayout, headerLayout, themeColors) {
    if (!headerLayout.headerRect) return;

    const headerPos = this.normalizePosition(pageLayout,
      headerLayout.headerRect.x, headerLayout.headerRect.y);
    const headerSize = this.normalizeSize(pageLayout,
      headerLayout.headerRect.width, headerLayout.headerRect.height);

    // Draw logo
    if (headerLayout.logo && headerLayout.logo.data.length > 10) {
      const logoPos = this.normalizePosition(pageLayout,
        headerLayout.logo.rect.x, headerLayout.logo.rect.y);
      const logoSize = this.normalizeSize(pageLayout,
        headerLayout.logo.rect.width, headerLayout.logo.rect.height);

      this.doc.image(headerLayout.logo.data, logoPos.x, logoPos.y,
        { width: logoSize.w, height: logoSize.h });
    }

    // Write branding text
    if (headerLayout.text) {
      const textPos = this.normalizePosition(pageLayout,
        headerLayout.text.rect.x, headerLayout.text.rect.y);
      const textSize = this.normalizeSize(pageLayout,
        headerLayout.text.rect.width, headerLayout.text.rect.height);

      textSize.w += 10;

      const textS = headerLayout.text.text.split('\n');

      this.doc.font('lato400').fillColor(themeColors.mainText);
      this.doc.fontSize(headerLayout.dates.fontSize);

      this.doc.text('', textPos.x, textPos.y, { continued: true });

      textS.forEach(text => {
        this.doc.text(text, {
          width: textSize.w,
          align: 'left',
          lineGap: -1
        });
      });
    }

    // Write first line
    if (headerLayout.title) {
      const titlePos = this.normalizePosition(pageLayout,
        headerLayout.title.rect.x, headerLayout.title.rect.y);
      const titleSize = this.normalizeSize(pageLayout,
        headerLayout.title.rect.width, headerLayout.title.rect.height);

      /// / DEBUG:
      // this.doc.rect(titlePos.x, titlePos.y, titleSize.w, titleSize.h).fill('#ffff00');

      titlePos.y -= headerLayout.title.fontSize * 0.15;

      // Hack to center rich text (in PDFKit it is totally fucked up)
      const firstWidth = (this.doc.font('lato400')
        .fontSize(headerLayout.title.fontSize)
        .widthOfString(headerLayout.title.text1));

      const lineBreakRegExp = /[\x20-\x2f\x3a-\x40\x5b-\x60]+/;
      const remainingWidth = titleSize.w - firstWidth;
      let secondWidth = 0;
      let secondString = '';

      const str = headerLayout.title.text2;
      let buffer = '';

      for (let i = 0; i < str.length; i++) {
        buffer += str.charAt(i);
        if (lineBreakRegExp.test(str.charAt(i))) {
          // Line break possible here
          const currentWidth = (this.doc.font('lato700')
            .fontSize(headerLayout.title.fontSize)
            .widthOfString(buffer.trimEnd()));

          if (currentWidth < remainingWidth) {
            secondWidth = currentWidth;
            secondString = buffer.trimEnd();
          } else {
            break;
          }
        }
      }

      // Check if entire string goes in one line
      const currentWidth = (this.doc.font('lato700')
        .fontSize(headerLayout.title.fontSize)
        .widthOfString(str));

      const breakForbidden = headerPos.y + headerSize.h < titlePos.y + 2 * headerLayout.title.fontSize;

      if (currentWidth < remainingWidth || breakForbidden) {
        secondWidth = currentWidth;
        secondString = str;
      }

      const leftPos = (titleSize.w - firstWidth - secondWidth) / 2 + titlePos.x;
      this.doc.font('lato400').fontSize(headerLayout.title.fontSize).fillColor(themeColors.secondaryText).text(
        headerLayout.title.text1, leftPos, titlePos.y, {
          align: 'left',
          continued: true
        }
      ).font('lato700').fillColor(themeColors.mainText).text(secondString);

      const string3 = str.substr(secondString.length).trim();

      if (string3.length > 0) {
        const transY = headerLayout.title.fontSize * 1.2;

        this.doc.font('lato700').fontSize(headerLayout.title.fontSize).fillColor(themeColors.mainText).text(
          string3, titlePos.x, titlePos.y + transY, {
            width: titleSize.w,
            height: titleSize.h - transY,
            align: 'center'
          }
        );
      }
    }

    // Write second line
    if (headerLayout.tutor) {
      const tutorPos = this.normalizePosition(pageLayout,
        headerLayout.tutor.rect.x, headerLayout.tutor.rect.y);
      const tutorSize = this.normalizeSize(pageLayout,
        headerLayout.tutor.rect.width, headerLayout.tutor.rect.height);

      /// / DEBUG:
      // this.doc.rect(tutorPos.x, tutorPos.y, tutorSize.w, tutorSize.h).fill('#00ffff');

      let totalWidth = 0;
      totalWidth += (this.doc.font('lato400')
        .fontSize(headerLayout.tutor.fontSize)
        .widthOfString(headerLayout.tutor.text1));
      totalWidth += (this.doc.font('lato700')
        .fontSize(headerLayout.tutor.fontSize)
        .widthOfString(headerLayout.tutor.text2));

      const leftPos = (tutorSize.w - totalWidth) / 2 + tutorPos.x;

      this.doc.font('lato400').fontSize(headerLayout.tutor.fontSize).fillColor(themeColors.secondaryText).text(
        headerLayout.tutor.text1, leftPos, tutorPos.y, {
          align: 'left',
          continued: true
        }
      ).fillColor(themeColors.mainText).font('lato700').text(headerLayout.tutor.text2);
    }

    // Write dates
    if (headerLayout.dates) {
      const datesPos = this.normalizePosition(pageLayout,
        headerLayout.dates.rect.x, headerLayout.dates.rect.y);
      const datesSize = this.normalizeSize(pageLayout,
        headerLayout.dates.rect.width, headerLayout.dates.rect.height);

      // datesPos.y -= headerLayout.title.fontSize * 0.15;

      /// / DEBUG:
      // this.doc.rect(datesPos.x, datesPos.y, datesSize.w, datesSize.h).fill('#ff80ff');

      const datesS = headerLayout.dates.text.split('\n');

      this.doc.font('lato400i').fillColor(themeColors.mainText).fontSize(headerLayout.dates.fontSize).text(
        datesS[0], headerPos.x, datesPos.y, {
          width: headerSize.w,
          height: datesSize.h,
          align: 'right',
          lineGap: -1
        }
      );

      this.doc.text(datesS[1], {
        width: headerSize.w,
        height: datesSize.h,
        align: 'right',
        lineGap: -1
      });
    }

    // Draw separator line

    this.doc.rect(headerPos.x, headerPos.y + headerSize.h - 1.5,
      headerSize.w, 1.5).fill(themeColors.accent);
  }

  handleTableHead (pageLayout, headLayout, themeColors) {
    this.doc.fontSize(headLayout.fontSize);
    this.doc.font('lato700');

    const ordinalPos = this.normalizePosition(pageLayout,
      headLayout.ordinalRect.x, headLayout.ordinalRect.y);
    const ordinalSize = this.normalizeSize(pageLayout,
      headLayout.ordinalRect.width, headLayout.ordinalRect.height);

    this.doc.rect(ordinalPos.x + ordinalSize.w - 2, ordinalPos.y,
      2, ordinalSize.h + 1).fill(themeColors.mainGrid);

    this.doc.fillColor(themeColors.lessonTime).text(headLayout.ordinalText, ordinalPos.x, ordinalPos.y,
      {
        align: 'center',
        width: ordinalSize.w,
        height: ordinalSize.h
      });
    for (let i = 0; i < headLayout.daysRects.length; i++) {
      const dayRect = headLayout.daysRects[i];
      const dayText = headLayout.daysTexts[i];

      const dayPos = this.normalizePosition(pageLayout, dayRect.x, dayRect.y);
      const daySize = this.normalizeSize(pageLayout, dayRect.width, dayRect.height);

      this.doc.rect(dayPos.x + daySize.w - 1, dayPos.y,
        1, daySize.h + 1).fill(themeColors.mainGrid);

      this.doc.fillColor(themeColors.accent).text(dayText, dayPos.x, dayPos.y,
        {
          align: 'center',
          width: daySize.w,
          height: daySize.h
        });
    }

    const containerPos = this.normalizePosition(pageLayout,
      headLayout.containerRect.x, headLayout.containerRect.y);
    const containerSize = this.normalizeSize(pageLayout,
      headLayout.containerRect.width, headLayout.containerRect.height);

    this.doc.rect(containerPos.x, containerPos.y + containerSize.h - 1.5,
      containerSize.w, 1.5).fill(themeColors.mainGrid);
  }

  handleTableData (pageLayout, tableLayout, themeColors) {
    if (tableLayout.length === 0) return;

    // Draw vertical dividers
    const firstPosition = this.normalizePosition(pageLayout,
      tableLayout[0].rowRect.x, tableLayout[0].rowRect.y);
    const lastPosition = this.normalizePosition(pageLayout,
      tableLayout[tableLayout.length - 1].rowRect.x,
      tableLayout[tableLayout.length - 1].rowRect.y);
    const lastSize = this.normalizeSize(pageLayout,
      tableLayout[tableLayout.length - 1].rowRect.width,
      tableLayout[tableLayout.length - 1].rowRect.height);

    const divHeight = lastPosition.y - firstPosition.y + lastSize.h + 1;

    const ordPos = this.normalizePosition(pageLayout,
      tableLayout[0].ordinalRect.x, tableLayout[0].ordinalRect.y);
    const ordSize = this.normalizeSize(pageLayout,
      tableLayout[0].ordinalRect.width, tableLayout[0].ordinalRect.height);

    // Draw each row
    tableLayout.forEach(row => {
      const rowPos = this.normalizePosition(pageLayout,
        row.rowRect.x, row.rowRect.y);
      const rowSize = this.normalizeSize(pageLayout,
        row.rowRect.width, row.rowRect.height);

      const ordinalPos = this.normalizePosition(pageLayout,
        row.ordinalRect.x, row.ordinalRect.y);
      const ordinalSize = this.normalizeSize(pageLayout,
        row.ordinalRect.width, row.ordinalRect.height);

      // Draw backgrounds
      if (!row.isMono && row.isInversed) {
        const grad = this.doc.linearGradient(ordinalPos.x, ordinalPos.y,
          ordinalPos.x + ordinalSize.w - 2, ordinalPos.y);
        grad.stop(0, 'white').stop(1, '#eeeeee');

        this.doc.rect(ordinalPos.x, ordinalPos.y,
          ordinalSize.w - 0.5, ordinalSize.h).fill(grad);

        this.doc.rect(rowPos.x + ordinalSize.w, rowPos.y,
          rowSize.w - ordinalSize.w, rowSize.h - 0.5).fill('#f0f0f0');
      }

      // Draw texts
      this.doc.fillColor(themeColors.lessonTime).font('lato700');

      const numPos = this.normalizePosition(pageLayout,
        row.number.rect.x, row.number.rect.y);

      const startPos = this.normalizePosition(pageLayout,
        row.start.rect.x, row.start.rect.y);

      const endPos = this.normalizePosition(pageLayout,
        row.end.rect.x, row.end.rect.y);

      this.doc.fontSize(row.number.fontSize).text(row.number.text,
        ordinalPos.x, numPos.y - row.number.fontSize * 0.2, {
          align: 'center',
          width: ordinalSize.w - 2
        }
      );

      this.doc.fontSize(row.start.fontSize).text(row.start.text,
        ordinalPos.x, startPos.y - row.start.fontSize * 0.2, {
          align: 'center',
          width: ordinalSize.w - 2
        }
      );

      this.doc.fontSize(row.end.fontSize).text(row.end.text,
        ordinalPos.x, endPos.y - row.end.fontSize * 0.2, {
          align: 'center',
          width: ordinalSize.w - 2
        }
      );

      // Draw cards
      row.days.forEach(day => {
        const daySize = this.normalizeSize(pageLayout,
          day.rect.width, day.rect.height);
        const dayPos = this.normalizePosition(pageLayout,
          day.rect.x, day.rect.y);

        daySize.w += dayPos.x;
        daySize.h += dayPos.y;

        day.data.forEach(card => {
          this.handleSingleCard(pageLayout, card, daySize, themeColors);
        });
      });

      // Draw horizontal bar
      this.doc.rect(rowPos.x, rowPos.y + rowSize.h - 0.75,
        rowSize.w, 0.75).fill(themeColors.lessonDiv);
    });

    this.doc.rect(ordPos.x + ordSize.w - 2,
      ordPos.y - 1, 2, divHeight).fill(themeColors.mainGrid);

    // Draw vertical bars
    tableLayout[0].days.forEach(day => {
      const dayPos = this.normalizePosition(pageLayout,
        day.rect.x, day.rect.y);
      const daySize = this.normalizeSize(pageLayout,
        day.rect.width, day.rect.height);

      this.doc.rect(dayPos.x + daySize.w - 1,
        dayPos.y - 1, 1, divHeight).fill(themeColors.mainGrid);
    });
  }

  handleSingleCard (pageLayout, cardLayout, fieldLayout, themeColors) {
    if (!cardLayout.cardRect) return; // Empty division placeholder

    const cardPos = this.normalizePosition(pageLayout,
      cardLayout.cardRect.x, cardLayout.cardRect.y);
    const cardSize = this.normalizeSize(pageLayout,
      cardLayout.cardRect.width, cardLayout.cardRect.height);

    // Prepare gradients
    let background;
    if (cardLayout.colors.length === 1) {
      background = cardLayout.colors[0];
    } else if (cardLayout.colors.length > 1) {
      background = this.doc.linearGradient(cardPos.x, cardPos.y,
        cardPos.x, cardPos.y + cardSize.h);

      const stopCount = cardLayout.colors.length * 3 - 1;

      for (let i = 0; i < cardLayout.colors.length; i++) {
        background.stop(i / stopCount * 3, cardLayout.colors[i]);
        background.stop((i * 3 + 2) / stopCount, cardLayout.colors[i]);
      }
    }

    const leftBarSize = !cardLayout.isMono ? 4 : 0;

    // Draw background and left bar

    if (!cardLayout.isMono) {
      this.doc.save();
      this.doc.opacity(0.2);
      this.doc.rect(cardPos.x, cardPos.y, cardSize.w, cardSize.h).fill(
        background);
      this.doc.restore();

      this.doc.rect(cardPos.x, cardPos.y, leftBarSize, cardSize.h).fill(background);
    }

    if (cardLayout.isMono) {
      if (cardPos.x + cardSize.w + 4 < fieldLayout.w) {
        this.doc.rect(cardPos.x + cardSize.w - 0.75,
          cardPos.y, 0.75, cardSize.h).fill('black');
      }

      if (cardPos.y + cardSize.h + 4 < fieldLayout.h) {
        this.doc.rect(cardPos.x, cardPos.y + cardSize.h - 0.75,
          cardSize.w, 0.75).fill('black');
      }
    }

    // Draw subject name
    const subjectPos = this.normalizePosition(pageLayout,
      cardLayout.subject.rect.x, cardLayout.subject.rect.y);
    const subjectSize = this.normalizeSize(pageLayout,
      cardLayout.subject.rect.width, cardLayout.subject.rect.height);

    let offsetFactor = 0.05;
    if (subjectPos.y - cardPos.y < 10) { // Sticks to top
      offsetFactor = 0.15;
    }

    this.doc.font('lato700').fillColor(themeColors.mainText).fontSize(
      cardLayout.subject.fontSize).text(
      cardLayout.subject.text, subjectPos.x,
      subjectPos.y - cardLayout.subject.fontSize * offsetFactor);

    // Draw group name (calculate best ellipsis size)
    const groupPos = this.normalizePosition(pageLayout,
      cardLayout.groups.rect.x, cardLayout.groups.rect.y);

    const ellipsis = '...';
    this.doc.font('lato400').fontSize(cardLayout.groups.fontSize);

    let maxWidth = cardSize.w - leftBarSize - 2;
    if (cardLayout.groups.isSquished) {
      maxWidth -= subjectSize.w - 1;
    }

    const fullWidth = this.doc.widthOfString(cardLayout.groups.text);
    if (fullWidth <= maxWidth) {
      // Just draw

      this.doc.text(cardLayout.groups.text, cardPos.x + leftBarSize + 1,
        groupPos.y - cardLayout.groups.fontSize * 0.1, {
          align: cardLayout.groups.isSquished ? 'right' : 'left',
          lineBreak: false,
          width: cardSize.w - leftBarSize - 2
        });
    } else {
      // Calculate ellipsis text cut position
      let lastOk = '';
      for (let i = 1; i < cardLayout.groups.text.length; i++) {
        const currText = cardLayout.groups.text.substr(0, i);
        const currWidth = this.doc.widthOfString(currText + ellipsis);

        if (currWidth <= maxWidth) {
          lastOk = currText;
        } else {
          this.doc.text(lastOk + ellipsis, cardPos.x + leftBarSize + 1,
            groupPos.y - cardLayout.groups.fontSize * 0.1, {
              align: cardLayout.groups.isSquished ? 'right' : 'left',
              lineBreak: false,
              width: cardSize.w - leftBarSize - 2
            });
          break;
        }
      }
    }

    // Draw teacher/info
    const infoPos = this.normalizePosition(pageLayout,
      cardLayout.info.rect.x, cardLayout.info.rect.y);

    this.doc.fontSize(cardLayout.info.fontSize).text(
      cardLayout.info.text, infoPos.x,
      infoPos.y - cardLayout.info.fontSize * 0.1);

    // Draw classroom
    const classroomPos = this.normalizePosition(pageLayout,
      cardLayout.classroom.rect.x, cardLayout.classroom.rect.y);
    const classroomSize = this.normalizeSize(pageLayout,
      cardLayout.classroom.rect.width, cardLayout.classroom.rect.height);

    this.doc.fontSize(cardLayout.classroom.fontSize).text(
      cardLayout.classroom.text, classroomPos.x,
      classroomPos.y - cardLayout.classroom.fontSize * 0.1, {
        align: 'right',
        width: classroomSize.w
      });
  }

  handleFooter (pageLayout, footerLayout, themeColors) {
    if (!footerLayout.footerRect) return;

    this.doc.font('lato400i');
    this.doc.fontSize(footerLayout.fontSize);

    const containerPos = this.normalizePosition(pageLayout,
      footerLayout.footerRect.x, footerLayout.footerRect.y);
    const containerSize = this.normalizeSize(pageLayout,
      footerLayout.footerRect.width, footerLayout.footerRect.height);

    // Draw separator line
    this.doc.rect(containerPos.x, containerPos.y,
      containerSize.w, 1.5).fill(themeColors.accent);

    // Draw left text
    const leftPos = this.normalizePosition(pageLayout,
      footerLayout.left.rect.x, footerLayout.left.rect.y);

    this.doc.fillColor(themeColors.mainText).text(footerLayout.left.text,
      leftPos.x, leftPos.y - footerLayout.fontSize * 0.1);

    // Draw center text
    const centerPos = this.normalizePosition(pageLayout,
      footerLayout.center.rect.x, footerLayout.center.rect.y);

    this.doc.fillColor(themeColors.mainText).text(footerLayout.center.text,
      centerPos.x, centerPos.y - footerLayout.fontSize * 0.1);

    // Draw right text
    const rightPos = this.normalizePosition(pageLayout,
      footerLayout.right.rect.x, footerLayout.right.rect.y);

    this.doc.fillColor(themeColors.mainText).text(footerLayout.right.text,
      rightPos.x, rightPos.y - footerLayout.fontSize * 0.1);
  }

  finish () {
    this.doc.end();

    if (this.outputPath && this.hasAnyPage) {
      this.doc.pipe(fs.createWriteStream(this.outputPath));
    }
  }
}

module.exports = PDFExporter;
