/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * parser.js - ASCTT XML database parser module
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const xmlparser = require('xml-parser');
const iconv = require('iconv-lite');

function splitNoEmpty (string, delimiter) {
  const ret = string.split(delimiter);
  for (let i = ret.length - 1; i >= 0; i--) {
    if (ret[i] === '') {
      ret.splice(i, 1);
    }
  }

  return ret;
}

class TimetableParser {
  constructor () {
    this.timetableDOM = null;

    this.fileInfo = {
      modified: new Date()
    };

    this.defaultRulesState = {};

    this.periodStats = {};

    this.timetableInfo = {
      ascVersion: '',
      periodArr: [],
      dayArr: new Array(6),
      subjectAssoc: {},
      teacherAssoc: {},
      classroomAssoc: {},
      gradeArr: [],
      classAssoc: {},
      classArrForGrade: {},
      groupAssoc: {},
      lessonAssoc: {},
      cardArr: []
    };
  }

  setXmlBuffer (buffer) {
    this.xmlString = buffer;
  }

  getXmlBuffer () {
    return this.xmlString;
  }

  parseTimetable (string) {
    return new Promise((resolve, reject) => {
      const encodingRegex = /^\s*<\?xml.+encoding="(.*?)".*\?>/;
      let encoding = encodingRegex.exec(string.toString())[1];

      if (!encoding) {
        encoding = 'UTF-8';
      }

      const xml = iconv.decode(string, encoding);
      this.timetableDOM = xmlparser(xml);

      if (this.timetableDOM.root.name === 'timetable') {
        this.parseDOM();
        delete this.xmlString;
        try {
          this.defaultRulesState = this.calcDefaultRulesState();

          if (!this.defaultRulesState) {
            throw new Error('Default rules state are undefined');
          }

          this.calcPeriodStats();
          resolve();
        } catch (error) {
          reject(error);
        }
      } else {
        reject(new Error());
      }
    });
  }

  parseDOM () {
    this.timetableInfo.ascVersion =
      this.timetableDOM.root.attributes.ascttversion;

    this.timetableDOM.root.children.forEach(nodeRoot => {
      // PERIODS
      if (nodeRoot.name === 'periods') {
        nodeRoot.children.forEach(node => {
          if (node.name === 'period') {
            const period = {
              name: node.attributes.name,
              short: node.attributes.short,
              period: node.attributes.period,
              startTime: node.attributes.starttime,
              endTime: node.attributes.endtime
            };

            this.timetableInfo.periodArr.push(period);
          }
        });
      }

      // DAYS
      if (nodeRoot.name === 'daysdefs') {
        nodeRoot.children.forEach(node => {
          if (node.name === 'daysdef') {
            const def = {
              id: node.attributes.id,
              name: node.attributes.name,
              short: node.attributes.short,
              dayArr: splitNoEmpty(node.attributes.days, ',')
            };

            const lookup = ['100000', '010000', '001000', '000100',
              '000010', '000001'];

            const firstDay = def.dayArr[0];
            if (lookup.indexOf(firstDay) >= 0) {
              this.timetableInfo.dayArr[lookup.indexOf(firstDay)] = def;
            } else {
              this.timetableInfo.dayArr.push(def);
            }
          }
        });
      }

      // SUBJECTS
      if (nodeRoot.name === 'subjects') {
        nodeRoot.children.forEach(node => {
          if (node.name === 'subject') {
            const subject = {
              id: node.attributes.id,
              name: node.attributes.name,
              short: node.attributes.short
            };

            this.timetableInfo.subjectAssoc[node.attributes.id] = subject;
          }
        });
      }

      // TEACHERS
      if (nodeRoot.name === 'teachers') {
        nodeRoot.children.forEach(node => {
          if (node.name === 'teacher') {
            const teacher = {
              id: node.attributes.id,
              name: node.attributes.name,
              short: node.attributes.short,
              color: node.attributes.color
            };

            this.timetableInfo.teacherAssoc[node.attributes.id] = teacher;
          }
        });
      }

      // CLASSROOMS
      if (nodeRoot.name === 'classrooms') {
        nodeRoot.children.forEach(node => {
          if (node.name === 'classroom') {
            const classroom = {
              id: node.attributes.id,
              name: node.attributes.name,
              short: node.attributes.short
            };

            this.timetableInfo.classroomAssoc[node.attributes.id] = classroom;
          }
        });
      }

      // GRADES
      if (nodeRoot.name === 'grades') {
        nodeRoot.children.forEach(node => {
          if (node.name === 'grade') {
            const grade = {
              name: node.attributes.name,
              short: node.attributes.short,
              grade: node.attributes.grade
            };

            this.timetableInfo.gradeArr.push(grade);
          }
        });
      }

      // CLASSES
      if (nodeRoot.name === 'classes') {
        nodeRoot.children.forEach(node => {
          if (node.name === 'class') {
            const classObj = {
              id: node.attributes.id,
              name: node.attributes.name,
              short: node.attributes.short,
              teacherId: node.attributes.teacherid
            };

            // Try to determine grade from the class name
            const regEx = /^[^\d]*(\d+)/;
            let classNum = regEx.exec(classObj.name);
            if (classNum && classNum.length === 2) {
              classObj.gradeNumber = classNum[1].toString();
              classNum = classObj.gradeNumber;
            } else {
              classNum = '?';
            }

            if (Array.isArray(this.timetableInfo.classArrForGrade[classNum])) {
              this.timetableInfo.classArrForGrade[classNum].push(classObj.id);
            } else {
              this.timetableInfo.classArrForGrade[classNum] = [classObj.id];
            }

            this.timetableInfo.classAssoc[node.attributes.id] = classObj;
          }
        });
      }

      // GROUPS
      if (nodeRoot.name === 'groups') {
        nodeRoot.children.forEach(node => {
          if (node.name === 'group') {
            const group = {
              id: node.attributes.id,
              name: node.attributes.name,
              classId: node.attributes.classid,
              entireClass: node.attributes.entireclass !== '0',
              divisionTag: node.attributes.divisiontag
            };

            this.timetableInfo.groupAssoc[node.attributes.id] = group;
          }
        });
      }

      // LESSONS
      if (nodeRoot.name === 'lessons') {
        nodeRoot.children.forEach(node => {
          if (node.name === 'lesson') {
            const lesson = {
              id: node.attributes.id,
              name: node.attributes.name,
              classIdArr: splitNoEmpty(node.attributes.classids, ','),
              subjectId: node.attributes.subjectid,
              periodsPerCard: node.attributes.periodspercard,
              teacherIdArr: splitNoEmpty(node.attributes.teacherids, ','),
              classroomIdArr: splitNoEmpty(node.attributes.classroomids, ','),
              groupIdArr: splitNoEmpty(node.attributes.groupids, ',')
            };

            this.timetableInfo.lessonAssoc[node.attributes.id] = lesson;
          }
        });
      }

      // CARDS
      if (nodeRoot.name === 'cards') {
        nodeRoot.children.forEach(node => {
          if (node.name === 'card') {
            const card = {
              lessonId: node.attributes.lessonid,
              classroomIdArr: splitNoEmpty(node.attributes.classroomids, ','),
              period: node.attributes.period
            };

            const lookup = ['100000', '010000', '001000', '000100',
              '000010', '000001'];

            const firstDay = node.attributes.days;
            if (lookup.indexOf(firstDay) >= 0) {
              card.day = lookup.indexOf(firstDay);
            } else {
              card.day = -1;
            }

            this.timetableInfo.cardArr.push(card);
          }
        });
      }
    });
  }

  calcDefaultRulesState () {
    const defObj = {};
    const subObj = {};

    // Fill object with an empty template for each subject
    Object.keys(this.timetableInfo.subjectAssoc).forEach(el => {
      subObj[el] = {
        id: this.timetableInfo.subjectAssoc[el].id,
        name: this.timetableInfo.subjectAssoc[el].name + ' (' +
          this.timetableInfo.subjectAssoc[el].short + ')',
        fullName: this.timetableInfo.subjectAssoc[el].name,
        groupNames: [],
        rules: []
      };
    });

    Object.keys(this.timetableInfo.classArrForGrade).forEach(el => {
      // Deep clone template object
      defObj[el] = {
        subjects: JSON.parse(JSON.stringify(subObj))
      };

      const nameHashMap = {};
      Object.keys(this.timetableInfo.subjectAssoc).forEach(subId => {
        nameHashMap[subId] = {};
      });

      // Prepare grade groups subject list
      Object.keys(this.timetableInfo.lessonAssoc).forEach(id => {
        const lesson = this.timetableInfo.lessonAssoc[id];

        // Check if class belongs to desired grade
        let belongsToGrade = false;
        lesson.classIdArr.forEach(classId => {
          if (this.timetableInfo.classArrForGrade[el].includes(classId)) {
            belongsToGrade = true;
          }
        });

        if (!belongsToGrade) return;

        // Insert group names if group is not an entire class
        lesson.groupIdArr.forEach(groupId => {
          const group = this.timetableInfo.groupAssoc[groupId];
          if (!group.entireClass) {
            nameHashMap[lesson.subjectId][group.name] = true;
          }
        });
      });

      // Set arrays
      Object.keys(defObj[el].subjects).forEach(subId => {
        defObj[el].subjects[subId].groupNames = Object.keys(nameHashMap[subId]).sort();
      });
    });

    return defObj;
  }

  getDefaultRulesState () {
    return this.defaultRulesState;
  }

  calcPeriodStats () {
    this.timetableInfo.cardArr.forEach(card => {
      if (!this.periodStats[card.period]) {
        this.periodStats[card.period] = 1;
      } else {
        this.periodStats[card.period]++;
      }
    });
  }

  getPeriodStats () {
    return this.periodStats;
  }

  setFileModified (newDate) {
    this.fileInfo.modified = newDate;
  }
}

module.exports = TimetableParser;
