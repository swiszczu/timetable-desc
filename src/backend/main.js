/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * main.js - backend electron script entry point
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const electron = require('electron');
const fs = require('fs');
const { app, BrowserWindow, ipcMain, dialog } = electron;

const TimetableParser = require('./parser');
const TimetableRenderer = require('./renderer');
const PDFExporter = require('./pdf-exporter');

const i18n = require('i18n');

const path = require('path');

i18n.configure({
  locales: ['pl', 'en'],
  defaultLocale: 'en',
  directory: path.resolve(__dirname, '../../locales'),
  updateFiles: process.env.NODE_ENV === 'development'
});

let mainWindow = null;
let addRuleWindow = null;
let previewWindow = null;
let exportProgressWindow = null;
let parserObj = null;

let isCurrentlyExporting = false;
let exporter = null;

// Background workers
let loaderWindow = null;
let rendererWindow = null;

/*
const { default: installExtension, REACT_DEVELOPER_TOOLS } = require('electron-devtools-installer');

installExtension(REACT_DEVELOPER_TOOLS)
    .then((name) => console.log(`Added Extension:  ${name}`))
    .catch((err) => console.log('An error occurred: ', err));
*/

global.reportedErrors = [];

app.on('ready', () => {
  console.log('Detected locale: ' + app.getLocale());
  i18n.setLocale(app.getLocale().toString());

  global.i18n = i18n;

  mainWindow = new BrowserWindow({
    width: 950,
    height: 680,
    minWidth: 700,
    minHeight: 400,
    title: i18n.__('Timetable DESC'),
    backgroundColor: '#191d19',
    webPreferences: {
      nodeIntegration: true
    },
    frame: false,
    show: false
  });

  if (process.env.NODE_ENV !== 'development') {
    mainWindow.setMenu(null);
  }

  mainWindow.webContents.once('dom-ready', () => {
    mainWindow.show();

    // Check command line arguments
    const argSize = process.argv.length;

    for (let i = 0; i < argSize; i++) {
      const argv = process.argv[i];

      if (argv.toLowerCase().endsWith('.xml')) {
        mainWindow.webContents.send('file:opened', argv);
        openFileByPath(argv);
      }
    }
  });

  mainWindow.once('closed', () => {
    app.quit();
  });

  if (process.env.NODE_ENV === 'development') {
    mainWindow.loadURL('http://localhost:3000');
  } else {
    mainWindow.loadFile(path.resolve(__dirname, '../../dist/index.html'));
  }
});

function openFileByPath (pth) {
  const stats = fs.statSync(pth);
  const xml = fs.readFileSync(pth);
  parserObj = new TimetableParser();
  parserObj.setXmlBuffer(xml);
  parserObj.setFileModified(stats.mtime);

  parserObj.filePath = pth;

  global.timetableParser = parserObj;

  // Start background process
  loaderWindow = new BrowserWindow({
    show: false,
    webPreferences: {
      nodeIntegration: true
    }
  });

  if (process.env.NODE_ENV !== 'development') {
    loaderWindow.setMenu(null);
  }

  if (process.env.NODE_ENV === 'development') {
    loaderWindow.loadURL('http://localhost:3000/loader-worker.html');
  } else {
    loaderWindow.loadFile(path.resolve(__dirname, '../../dist/loader-worker.html'));
  }
}

ipcMain.on('file:browse', (event) => {
  dialog.showOpenDialog(mainWindow, {
    title: i18n.__('Open XML timetable database...'),
    filters: [
      { name: i18n.__('aSc Timetable Database') + ' (*.xml)', extensions: ['xml'] },
      { name: i18n.__('All files') + ' (*.*)', extensions: ['*'] }
    ],
    properties: ['openFile'],
    message: i18n.__('Select an XML file exported from aSc Timetable to load timetable information.')
  }).then(result => {
    if (!result.cancelled && result.filePaths[0]) {
      try {
        event.reply('file:opened', result.filePaths[0]);
        openFileByPath(result.filePaths[0]);
      } catch (error) {
        mainWindow.webContents.send('file:loaderror');
        console.log(error);
      }
    }
  }).catch(error => {
    console.error(error);
  });
});

ipcMain.on('file:worker:ready', () => {
  mainWindow.webContents.send('file:loaded');

  // Try to load configuration from file
  const path = parserObj.filePath + '.json';

  fs.readFile(path, 'utf8', (err, contents) => {
    if (!err) {
      mainWindow.webContents.send('json:loaded', {
        data: contents,
        ok: true,
        initial: true
      });
    }
  });

  loaderWindow = null;
});

ipcMain.on('file:worker:error', (ev, error) => {
  mainWindow.webContents.send('file:loaderror');
  console.error(error);

  loaderWindow = null;
});

ipcMain.on('logo:open', (ev, data) => {
  dialog.showOpenDialog(mainWindow, {
    title: i18n.__('Open logo file'),
    filters: [
      { name: i18n.__('All supported images') + ' (*.jpg|*.jpeg|*.png|*.gif|*.bmp)', extensions: ['jpg', 'jpeg', 'png', 'gif', 'bmp'] },
      { name: i18n.__('JPEG Image') + ' (*.jpg|*.jpeg)', extensions: ['jpg', 'jpeg'] },
      { name: i18n.__('Portable Network Graphics') + ' (*.png)', extensions: ['png'] },
      { name: i18n.__('Graphics Interchange Format') + ' (*.gif)', extensions: ['gif'] },
      { name: i18n.__('All files') + ' (*.*)', extensions: ['*'] }
    ],
    properties: ['openFile'],
    message: i18n.__('Select an XML file exported from aSc Timetable to load timetable information.')
  }).then((result) => {
    if (!result.cancelled && result.filePaths[0]) {
      ev.reply('logo:opened', {
        path: result.filePaths[0],
        callerId: data.callerId
      });
    }
  }).catch(error => {
    console.error(error);
  });
});

ipcMain.on('rule:add', (ev, data) => {
  global.ruleOpenData = data;

  addRuleWindow = new BrowserWindow({
    width: 450,
    height: 600,
    title: (data.currentState) ? (i18n.__('Edit rule')) : (i18n.__('Add rule')),
    backgroundColor: '#191d19',
    webPreferences: {
      nodeIntegration: true
    },
    parent: mainWindow,
    resizable: false,
    minimizable: false,
    modal: true,
    frame: false
  });

  if (process.env.NODE_ENV !== 'development') {
    addRuleWindow.setMenu(null);
  }

  if (process.env.NODE_ENV === 'development') {
    addRuleWindow.loadURL('http://localhost:3000/add-rule.html');
  } else {
    addRuleWindow.loadFile(path.resolve(__dirname, '../../dist/add-rule.html'));
  }
});

ipcMain.on('rule:added', (ev, data) => {
  mainWindow.webContents.send('rule:add-to-subject', data);
});

ipcMain.on('json:save', (ev, data) => {
  if (data.dialog) {
    dialog.showSaveDialog(mainWindow, {
      title: i18n.__('Save configuration file'),
      filters: [
        { name: i18n.__('JavaScript Object Notation') + ' (*.json)', extensions: ['json'] }
      ],
      properties: [],
      buttonLabel: i18n.__('Export')
    }).then((result) => {
      if (!result.cancelled && result.filePath) {
        fs.writeFile(result.filePath, JSON.stringify(data.data),
          'utf8', () => {});
        ev.reply('json:saved');
      }
    }).catch(error => {
      console.error(error);
    });
  } else if (data.path) {
    fs.writeFile(data.path, JSON.stringify(data.data),
      'utf8', () => {});
  }
});

ipcMain.on('json:load', (ev, data) => {
  if (data.dialog) {
    dialog.showOpenDialog(mainWindow, {
      title: i18n.__('Open configuration file'),
      filters: [
        { name: i18n.__('JavaScript Object Notation') + ' (*.json)', extensions: ['json'] }
      ],
      buttonLabel: i18n.__('Import'),
      properties: []
    }).then((result) => {
      if (!result.cancelled && result.filePaths[0]) {
        ev.reply('json:loaded', {
          data: fs.readFileSync(result.filePaths[0], 'utf8'),
          ok: true
        });
      } else {
        ev.reply('json:loaded', {
          ok: false
        });
      }
    }).catch(error => {
      console.error(error);
      ev.reply('json:loaded', {
        ok: false
      });
    });
  } else if (data.path) {
    ev.reply('json:loaded', {
      data: fs.readFileSync(data.path, 'utf8'),
      ok: true
    });
  }
});

ipcMain.on('preview:create', (ev, data) => {
  if (global.renderData) return;

  // TODO: Remove this
  if (data.renderData.condensed) {
    dialog.showMessageBoxSync(mainWindow, {
      type: 'info',
      title: i18n.__('Info'),
      message: i18n.__('Not Yet Implemented!'),
      buttons: [i18n.__('OK')]
    });
    return;
  }

  if (data.renderData.id.length === 0) {
    dialog.showMessageBoxSync(mainWindow, {
      type: 'warning',
      title: i18n.__('Warning'),
      message: i18n.__('No entries for export selected! Go to the "Input/Output" tab and make sure that at least one class, teacher or classroom is checked.'),
      buttons: [i18n.__('OK')]
    });
    return;
  }

  global.renderData = data;

  // If autosave is enabled, save configuration to file
  if (data.softConfig.generalOptions.autoSaveEnabled) {
    const outPath = data.xmlFilePath + '.json';
    try {
      fs.writeFile(outPath, JSON.stringify(data.softConfig),
        'utf8', () => {});
    } catch (error) {
      console.error(error);
    }
  }

  ipcMain.once('preview:clear_global', () => {
    global.renderData = null;
  });

  previewWindow = new BrowserWindow({
    width: 1000,
    height: 600,
    minWidth: 500,
    minHeight: 350,
    title: i18n.__('Export preview') + ' – Timetable DESC',
    backgroundColor: '#191d19',
    webPreferences: {
      nodeIntegration: true
    },
    resizable: true,
    modal: false,
    frame: false,
    show: false
  });

  previewWindow.once('closed', () => {
    global.renderData = null;
  });

  if (process.env.NODE_ENV !== 'development') {
    previewWindow.setMenu(null);
  }

  if (process.env.NODE_ENV === 'development') {
    previewWindow.loadURL('http://localhost:3000/export-preview.html');
  } else {
    previewWindow.loadFile(path.resolve(__dirname, '../../dist/export-preview.html'));
  }

  previewWindow.maximize();
  previewWindow.show();
});

ipcMain.on('preview:render', (ev, data) => {
  const renderer = new TimetableRenderer();
  renderer.setParserResult(parserObj.timetableInfo);
  renderer.setFileInfo(parserObj.fileInfo);
  renderer.setRenderConfiguration(data);

  global.renderer = renderer;

  // Start background process
  rendererWindow = new BrowserWindow({
    show: false,
    webPreferences: {
      nodeIntegration: true
    }
  });

  if (process.env.NODE_ENV === 'development') {
    rendererWindow.loadURL('http://localhost:3000/renderer-worker.html');
  } else {
    rendererWindow.loadFile(path.resolve(__dirname, '../../dist/renderer-worker.html'));
  }
});

ipcMain.on('preview:worker:rendered', () => {
  previewWindow.webContents.send('preview:rendered',
    global.renderer.getRenderData());

  rendererWindow = null;
});

ipcMain.on('preview:worker:error', (ev, data) => {
  previewWindow.close();
  console.error(data);

  previewWindow = null;
});

ipcMain.on('export:start', (ev, data) => {
  if (!isCurrentlyExporting) {
    dialog.showSaveDialog(previewWindow, {
      title: i18n.__('Export timetable as'),
      filters: [
        { name: i18n.__('Portable Document Format') + ' (*.pdf)', extensions: ['pdf'] }
      ],
      properties: [],
      buttonLabel: i18n.__('Export')
    }).then((result) => {
      if (!result.cancelled && result.filePath) {
        isCurrentlyExporting = true;
        exporter = new PDFExporter();
        exporter.setDocumentMeta(data.metadata.title,
          data.metadata.author, data.metadata.subject);
        exporter.setOutputPath(result.filePath);

        exportProgressWindow = new BrowserWindow({
          width: 400,
          height: 270,
          title: i18n.__('Export progress'),
          backgroundColor: '#191d19',
          webPreferences: {
            nodeIntegration: true
          },
          resizable: false,
          parent: previewWindow,
          modal: true,
          frame: false
        });

        if (process.env.NODE_ENV !== 'development') {
          exportProgressWindow.setMenu(null);
        }

        if (process.env.NODE_ENV === 'development') {
          exportProgressWindow.loadURL('http://localhost:3000/export-progress.html');
        } else {
          exportProgressWindow.loadFile(path.resolve(__dirname, '../../dist/export-progress.html'));
        }

        exportProgressWindow.webContents.once('dom-ready', () => {
          previewWindow.webContents.send('export:begin');
        });
      }
    }).catch(error => {
      console.error(error);
    });
  }
});

ipcMain.on('export:page', (ev, data) => {
  if (!exporter) return;

  const progress = data.progress;
  exportProgressWindow.webContents.send('export:progress:update', progress);

  exporter.acceptPageData(data.data);

  if (data.data.isMulti) {
    const pathToExchange = exporter.getOutputPath();
    const filename = (data.data.contentLayout.headerInfo.name
      .toLowerCase().replace(/[^\w]/g, '-'));
    const newPath = path.dirname(pathToExchange) + path.sep + filename + '.pdf';

    exporter.setOutputPath(newPath);

    if (data.data.contentLayout.lastPage) {
      exporter.finish();
      const metaTmp = exporter.doc.info;

      exporter = new PDFExporter();
      exporter.setOutputPath(pathToExchange);
      exporter.setDocumentMeta(metaTmp.Title, metaTmp.Author,
        metaTmp.Subject);
    }
  }
});

ipcMain.on('export:progress:cancel', () => {
  previewWindow.webContents.send('export:progress:cancel');

  isCurrentlyExporting = false;
  exportProgressWindow.close();
  exporter = null;
});

ipcMain.on('export:finish', () => {
  exportProgressWindow.close();
  isCurrentlyExporting = false;
  exporter.finish();
  exporter = null;
});
