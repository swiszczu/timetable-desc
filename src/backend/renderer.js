/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * renderer.js - extracts timetable information using given configuration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const dateFormat = require('dateformat');
const asyncForEach = require('async-await-foreach');

class TimetableRenderer {
  constructor () {
    this.renderData = {};

    this.parserResult = null;
    this.renderConfig = null;
    this.fileInfo = null;

    this.currentDate = new Date();
  }

  setParserResult (result) {
    this.parserResult = result;
  }

  setRenderConfiguration (config) {
    this.renderConfig = config;
  }

  setFileInfo (info) {
    this.fileInfo = info;
  }

  getRenderData () {
    return this.renderData;
  }

  /// ////////////////////////////////////
  processTimetableHeader (fragmentData, rd) {
    const headerBlock = {};

    if (rd.type === 'class') {
      headerBlock.type = 'class';
      if (this.parserResult.classAssoc[rd.id]) {
        headerBlock.name = this.parserResult.classAssoc[rd.id].name;

        // For class, find class tutor
        headerBlock.tutorName = (
          this.parserResult.teacherAssoc[
            this.parserResult.classAssoc[rd.id].teacherId].name
        );
      } else {
        headerBlock.name = '???';
        headerBlock.tutorName = '???';
      }
    }
    if (rd.type === 'teacher') {
      headerBlock.type = 'teacher';
      if (this.parserResult.teacherAssoc[rd.id]) {
        headerBlock.name = this.parserResult.teacherAssoc[rd.id].name;
      } else {
        headerBlock.name = '???';
      }
    }
    if (rd.type === 'classroom') {
      headerBlock.type = 'classroom';
      if (this.parserResult.classroomAssoc[rd.id]) {
        headerBlock.name = this.parserResult.classroomAssoc[rd.id].name;
      } else {
        headerBlock.name = '???';
      }
    }

    headerBlock.exportDate = dateFormat(this.fileInfo.modified, 'dd.mm.yyyy');
    headerBlock.renderDate = dateFormat(this.currentDate, 'dd.mm.yyyy');

    fragmentData.headerInfo = headerBlock;
  }

  processBells (fragmentData) {
    this.parserResult.periodArr.forEach(period => {
      fragmentData.data.push({
        startTime: period.startTime,
        endTime: period.endTime,
        number: period.period
      });
    });
  }

  associateCards (periodCardArr) {
    this.parserResult.cardArr.forEach(card => {
      const period = card.period;
      const dayOfWeek = card.day;

      if (dayOfWeek >= 0 && dayOfWeek < 7) {
        periodCardArr[period][dayOfWeek].push(card);
      }
    });
  }

  processCardsForClass (fragmentData, periodCardArr, classId) {
    // If there is no such class, return immediately
    if (!this.parserResult.classAssoc[classId]) return;
    const classObj = this.parserResult.classAssoc[classId];

    // Prepare renaming rules object
    const gradeRules = {};
    this.renderConfig.softConfig.groupRenamingOptions.rules.forEach(rule => {
      if (rule.gradeName !== classObj.gradeNumber) return;

      if (Array.isArray(gradeRules[rule.subjectName])) {
        gradeRules[rule.subjectName].push(rule);
      } else {
        gradeRules[rule.subjectName] = [rule];
      }
    });

    // Process division tag information
    const divisionTagCount = [];
    for (const gk in this.parserResult.groupAssoc) {
      const groupObj = this.parserResult.groupAssoc[gk];
      if (groupObj.classId !== classId) continue;

      const divTag = groupObj.divisionTag;

      if (divisionTagCount[divTag]) {
        divisionTagCount[divTag]++;
      } else {
        divisionTagCount[divTag] = 1;
      }
    }

    for (const i in periodCardArr) {
      const period = periodCardArr[i];

      period.forEach((dayOfWeek, j) => {
        let divisionCount = 0;
        let divisionId = -1;

        dayOfWeek.forEach(card => {
          const lessonObj = this.parserResult.lessonAssoc[card.lessonId];
          if (!lessonObj) return;

          // Check if requested class has this lesson
          if (!lessonObj.classIdArr.includes(classId)) return;

          const resultObj = {
            subjectName: this.parserResult.subjectAssoc[lessonObj.subjectId].short,
            isEmpty: false
          };

          resultObj.colorRGBs = [];

          resultObj.teacherNames = lessonObj.teacherIdArr.map(teacherId => {
            const teacher = this.parserResult.teacherAssoc[teacherId];

            resultObj.colorRGBs.push(teacher.color);
            return teacher.short;
          });

          resultObj.classroomNames = card.classroomIdArr.map(classroomId => {
            return this.parserResult.classroomAssoc[classroomId].short;
          });

          let subjectRules = [];
          if (gradeRules) {
            const subjectName = (this.parserResult.subjectAssoc[lessonObj.subjectId].name +
              ' (' + this.parserResult.subjectAssoc[lessonObj.subjectId].short +
              ')');

            subjectRules = gradeRules[subjectName];
          }

          const groupRenamingAssoc = {};
          if (subjectRules) {
            subjectRules.forEach(rule => {
              rule.rule.groupNames.forEach(groupName => {
                groupRenamingAssoc[groupName] = rule.rule.newName;
              });
            });
          }

          const processedGroups = [];

          // Apply group filtering
          resultObj.groupNames = lessonObj.groupIdArr.reduce((result, groupId) => {
            const groupObj = this.parserResult.groupAssoc[groupId];
            const groupName = groupObj.name;

            if (groupObj.entireClass) { // Do not add entire class groups
              return result;
            }

            if (groupObj.classId !== classId) { // Ignore if it isn't selected class' group
              return result;
            }

            if (divisionId === -1) {
              divisionId = groupObj.divisionTag;
            }

            if (groupObj.divisionTag === divisionId &&
              !processedGroups.includes(groupName)) {
              processedGroups.push(groupName);
              divisionCount++;
            }

            if (groupRenamingAssoc[groupName]) {
              const renamed = groupRenamingAssoc[groupName];
              if (!result.includes(renamed)) {
                result.push(renamed);
              }
            } else {
              if (!result.includes(groupName)) {
                result.push(groupName);
              }
            }

            return result;
          }, []);

          // Sort group names lexically
          resultObj.groupNames.sort();

          // Add result card to output object
          fragmentData.cards[i][j].push(resultObj);
        });

        // If there is someone who has no lessons now, add empty card
        if (divisionId >= 0 && divisionCount < divisionTagCount[divisionId]) {
          fragmentData.cards[i][j].push({
            subjectName: '',
            isEmpty: true,
            colorRGBs: []
          });
        }
      });
    }
  }

  normalizeClassListName (classList) {
    if (classList.length < 1) return '';
    if (classList.length === 1) return classList[0];

    let commonPrefix = classList[0];
    let commonSuffix = classList[0];

    let minClassLength = classList[0].length;

    classList.forEach(className => {
      if (className === '') return;

      if (className.length < minClassLength) minClassLength = className.length;

      while (!className.startsWith(commonPrefix)) {
        commonPrefix = commonPrefix.substr(0, commonPrefix.length - 1);
      }

      while (!className.endsWith(commonSuffix)) {
        commonSuffix = commonSuffix.substr(1, commonSuffix.length - 1);
      }
    });

    if (commonPrefix === '') {
      commonSuffix = '';
    }

    if (commonPrefix.length + commonSuffix.length > minClassLength) {
      const diffToCut = (commonPrefix.length + commonSuffix.length -
         minClassLength);

      commonSuffix = commonSuffix.substring(diffToCut);
    }

    let output = commonPrefix;
    classList.forEach(className => {
      if (className === '') return;

      output += className.substr(commonPrefix.length,
        className.length - commonPrefix.length - commonSuffix.length);
    });
    output += commonSuffix;

    return output;
  }

  processCardsForTeacher (fragmentData, periodCardArr, teacherId) {
    // If there is no such teacher, return immediately
    if (!this.parserResult.teacherAssoc[teacherId]) return;

    // Prepare renaming rules object
    const gradeRules = [];
    Object.keys(this.parserResult.classAssoc).forEach(key => {
      const classObj = this.parserResult.classAssoc[key];
      gradeRules[classObj.gradeNumber] = {};
    });

    this.renderConfig.softConfig.groupRenamingOptions.rules.forEach(rule => {
      if (Array.isArray(gradeRules[rule.gradeName][rule.subjectName])) {
        gradeRules[rule.gradeName][rule.subjectName].push(rule);
      } else {
        gradeRules[rule.gradeName][rule.subjectName] = [rule];
      }
    });

    for (const i in periodCardArr) {
      const period = periodCardArr[i];

      period.forEach((dayOfWeek, j) => {
        dayOfWeek.forEach(card => {
          const lessonObj = this.parserResult.lessonAssoc[card.lessonId];
          if (!lessonObj) return;

          // Check if requested teacher has this lesson
          if (!lessonObj.teacherIdArr.includes(teacherId)) return;

          const resultObj = {
            teacherNames: [this.parserResult.subjectAssoc[
              lessonObj.subjectId].short],
            isEmpty: false
          };

          const sortedClassIdArr = [...lessonObj.classIdArr];
          sortedClassIdArr.sort((a, b) => {
            const aName = this.parserResult.classAssoc[a].name.toUpperCase();
            const bName = this.parserResult.classAssoc[b].name.toUpperCase();

            if (aName < bName) {
              return -1;
            }
            if (aName > bName) {
              return 1;
            }

            return 0;
          });

          const classesReadable = sortedClassIdArr.map(classId => {
            return this.parserResult.classAssoc[classId].name;
          });

          resultObj.subjectName = this.normalizeClassListName(classesReadable);

          resultObj.classroomNames = card.classroomIdArr.map(classroomId => {
            return this.parserResult.classroomAssoc[classroomId].short;
          });

          resultObj.colorRGBs = sortedClassIdArr.map(classId => {
            const className = this.parserResult.classAssoc[classId].name;
            return (this.renderConfig.softConfig
              .appearanceOptions.colors[className].color);
          });

          let firstClassObj = null;
          if (this.parserResult.classAssoc[lessonObj.classIdArr[0]]) {
            firstClassObj = this.parserResult.classAssoc[lessonObj.classIdArr[0]];
          }

          let subjectRules = [];
          if (gradeRules) {
            const subjectName = (this.parserResult.subjectAssoc[lessonObj.subjectId].name +
              ' (' + this.parserResult.subjectAssoc[lessonObj.subjectId].short +
              ')');

            if (firstClassObj) {
              subjectRules = gradeRules[firstClassObj.gradeNumber][subjectName];
            }
          }

          const groupRenamingAssoc = {};
          if (subjectRules) {
            subjectRules.forEach(rule => {
              rule.rule.groupNames.forEach(groupName => {
                groupRenamingAssoc[groupName] = rule.rule.newName;
              });
            });
          }

          const processedGroups = [];

          // Apply group filtering
          resultObj.groupNames = lessonObj.groupIdArr.reduce((result, groupId) => {
            const groupObj = this.parserResult.groupAssoc[groupId];
            const groupName = groupObj.name;

            if (groupObj.entireClass) { // Do not add entire class groups
              return result;
            }

            if (!processedGroups.includes(groupName)) {
              processedGroups.push(groupName);
            }

            if (groupRenamingAssoc[groupName]) {
              const renamed = groupRenamingAssoc[groupName];
              if (!result.includes(renamed)) {
                result.push(renamed);
              }
            } else {
              if (!result.includes(groupName)) {
                result.push(groupName);
              }
            }

            return result;
          }, []);

          // Sort group names lexically
          resultObj.groupNames.sort();

          // Add result card to output object
          fragmentData.cards[i][j].push(resultObj);
        });
      });
    }
  }

  processCardsForClassroom (fragmentData, periodCardArr, classroomId) {
    // If there is no such classroom, return immediately
    if (!this.parserResult.classroomAssoc[classroomId]) return;

    // Prepare renaming rules object
    const gradeRules = [];
    Object.keys(this.parserResult.classAssoc).forEach(key => {
      const classObj = this.parserResult.classAssoc[key];
      gradeRules[classObj.gradeNumber] = {};
    });

    this.renderConfig.softConfig.groupRenamingOptions.rules.forEach(rule => {
      if (Array.isArray(gradeRules[rule.gradeName][rule.subjectName])) {
        gradeRules[rule.gradeName][rule.subjectName].push(rule);
      } else {
        gradeRules[rule.gradeName][rule.subjectName] = [rule];
      }
    });

    for (const i in periodCardArr) {
      const period = periodCardArr[i];

      period.forEach((dayOfWeek, j) => {
        dayOfWeek.forEach(card => {
          const lessonObj = this.parserResult.lessonAssoc[card.lessonId];
          if (!lessonObj) return;

          // Check if this lesson takes place in requested classroom
          if (!card.classroomIdArr.includes(classroomId)) return;

          const resultObj = {
            classroomNames: [this.parserResult.subjectAssoc[
              lessonObj.subjectId].short],
            isEmpty: false
          };

          const sortedClassIdArr = [...lessonObj.classIdArr];
          sortedClassIdArr.sort((a, b) => {
            const aName = this.parserResult.classAssoc[a].name.toUpperCase();
            const bName = this.parserResult.classAssoc[b].name.toUpperCase();

            if (aName < bName) {
              return -1;
            }
            if (aName > bName) {
              return 1;
            }

            return 0;
          });

          const classesReadable = sortedClassIdArr.map(classId => {
            return this.parserResult.classAssoc[classId].name;
          });

          resultObj.subjectName = this.normalizeClassListName(classesReadable);

          resultObj.teacherNames = lessonObj.teacherIdArr.map(teacherId => {
            return this.parserResult.teacherAssoc[teacherId].short;
          });

          resultObj.colorRGBs = sortedClassIdArr.map(classId => {
            const className = this.parserResult.classAssoc[classId].name;
            return (this.renderConfig.softConfig
              .appearanceOptions.colors[className].color);
          });

          let firstClassObj = null;
          if (this.parserResult.classAssoc[lessonObj.classIdArr[0]]) {
            firstClassObj = this.parserResult.classAssoc[lessonObj.classIdArr[0]];
          }

          let subjectRules = [];
          if (gradeRules) {
            const subjectName = (this.parserResult.subjectAssoc[lessonObj.subjectId].name +
              ' (' + this.parserResult.subjectAssoc[lessonObj.subjectId].short +
              ')');

            if (firstClassObj) {
              subjectRules = gradeRules[firstClassObj.gradeNumber][subjectName];
            }
          }

          const groupRenamingAssoc = {};
          if (subjectRules) {
            subjectRules.forEach(rule => {
              rule.rule.groupNames.forEach(groupName => {
                groupRenamingAssoc[groupName] = rule.rule.newName;
              });
            });
          }

          const processedGroups = [];

          // Apply group filtering
          resultObj.groupNames = lessonObj.groupIdArr.reduce((result, groupId) => {
            const groupObj = this.parserResult.groupAssoc[groupId];
            const groupName = groupObj.name;

            if (groupObj.entireClass) { // Do not add entire class groups
              return result;
            }

            if (!processedGroups.includes(groupName)) {
              processedGroups.push(groupName);
            }

            if (groupRenamingAssoc[groupName]) {
              const renamed = groupRenamingAssoc[groupName];
              if (!result.includes(renamed)) {
                result.push(renamed);
              }
            } else {
              if (!result.includes(groupName)) {
                result.push(groupName);
              }
            }

            return result;
          }, []);

          // Sort group names lexically
          resultObj.groupNames.sort();

          // Add result card to output object
          fragmentData.cards[i][j].push(resultObj);
        });
      });
    }
  }

  async processSingle (renderData) {
    const fragmentData = {};
    this.processTimetableHeader(fragmentData, renderData);

    fragmentData.data = [];
    this.processBells(fragmentData);

    const tempCards = {};
    fragmentData.cards = {};

    fragmentData.data.forEach(period => {
      tempCards[period.number] = [[], [], [], [], [], []];
      fragmentData.cards[period.number] = [[], [], [], [], [], []];
    });

    this.associateCards(tempCards);

    if (renderData.type === 'class') {
      this.processCardsForClass(fragmentData, tempCards,
        renderData.id);
    } else if (renderData.type === 'teacher') {
      this.processCardsForTeacher(fragmentData, tempCards,
        renderData.id);
    } else if (renderData.type === 'classroom') {
      this.processCardsForClassroom(fragmentData, tempCards,
        renderData.id);
    }

    // Order cards by group names, then by subject names
    Object.keys(fragmentData.cards).forEach(periodGroup => {
      fragmentData.cards[periodGroup].forEach(period => {
        period.sort((a, b) => {
          if (b.isEmpty) return -1;
          if (a.isEmpty) return +1;

          const aGroups = a.groupNames.join(',').toUpperCase();
          const bGroups = b.groupNames.join(',').toUpperCase();

          if (aGroups === bGroups) {
            if (a.subjectName.toUpperCase() < b.subjectName.toUpperCase()) { return -1; } else if (a.subjectName.toUpperCase() > b.subjectName.toUpperCase()) { return +1; }
            return 0;
          }

          if (aGroups < bGroups) {
            return -1;
          } else {
            return 1;
          }
        });
      });
    });

    // this.renderData = fragmentData;
    return fragmentData;
  }

  async process () {
    this.renderData = [];

    if (this.renderConfig.renderData.id != null) {
      if (typeof this.renderConfig.renderData.id === 'string') {
        this.renderData.push(
          await this.processSingle(this.renderConfig.renderData));
      } else if (this.renderConfig.renderData.id instanceof Array) {
        const rdCopy = Object.assign({}, this.renderConfig.renderData);

        await asyncForEach(this.renderConfig.renderData.id, async id => {
          rdCopy.id = id;
          this.renderData.push(
            await this.processSingle(rdCopy));
        });
      }
    } else {
      const rdCopy = Object.assign({}, this.renderConfig.renderData);

      if (rdCopy.type === 'class') {
        for (const id in this.parserResult.classAssoc) {
          rdCopy.id = id;
          this.renderData.push(
            await this.processSingle(rdCopy));
        }
      } else if (rdCopy.type === 'teacher') {
        for (const id in this.parserResult.teacherAssoc) {
          rdCopy.id = id;
          this.renderData.push(
            await this.processSingle(rdCopy));
        }
      } else if (rdCopy.type === 'classroom') {
        for (const id in this.parserResult.classroomAssoc) {
          rdCopy.id = id;
          this.renderData.push(
            await this.processSingle(rdCopy));
        }
      }
    }
  }
}

module.exports = TimetableRenderer;
