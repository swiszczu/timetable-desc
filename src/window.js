/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * window.js - browser entry point
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Append scss as this module
import './style.scss';

// Add focus polyfill
import 'focus-visible/dist/focus-visible.min';

import React from 'react';
import ReactDOM from 'react-dom';

import { TimetableApp } from './timetable-app';

const { remote } = window.require('electron');
const i18n = remote.getGlobal('i18n');

const customTitlebar = window.require('../lib/custom-electron-titlebar');
const titlebar = new customTitlebar.Titlebar({
  backgroundColor: customTitlebar.Color.fromHex('#191d19'),
  shadow: true,
  overflow: 'hidden'
});

titlebar.setHorizontalAlignment('left');
titlebar.updateMenu({ items: [] });

// UI script
const root = <TimetableApp i18n={i18n} />;
ReactDOM.render(root, document.getElementById('app'));
