/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * add-rule-window.js - browser entry point of Add rule dialog
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Add focus polyfill
import 'focus-visible/dist/focus-visible.min';

import React from 'react';
import ReactDOM from 'react-dom';

import { AddRuleDialog } from './add-rule';

const { remote } = window.require('electron');
const i18n = remote.getGlobal('i18n');
const openData = remote.getGlobal('ruleOpenData');

const customTitlebar = window.require('../lib/custom-electron-titlebar');
const titlebar = new customTitlebar.Titlebar({
  backgroundColor: customTitlebar.Color.fromHex('#191d19'),
  shadow: true,
  minimizable: false,
  maximizable: false
});

titlebar.updateTitle((openData.currentState)
  ? (i18n.__('Edit rule')) : (i18n.__('Add rule')));
titlebar.setHorizontalAlignment('left');
titlebar.updateMenu({ items: [] });

// UI script
const root = (
  <AddRuleDialog
    i18n={i18n}
    dialogData={openData}
  />

);
ReactDOM.render(root, document.getElementById('app'));
