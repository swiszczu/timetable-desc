/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * export-preview-window.js - browser entry point for Export preview window
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Append scss to this module
import './style.scss';
import './timetable.scss';

// Add focus polyfill
import 'focus-visible/dist/focus-visible.min';

import React from 'react';
import ReactDOM from 'react-dom';

import { ExportPreviewWindow } from './export-preview';

const { remote } = window.require('electron');
const i18n = remote.getGlobal('i18n');

// Clone data from main process
const renderData = JSON.parse(JSON.stringify(remote.getGlobal('renderData')));
console.log(renderData);

const customTitlebar = window.require('../lib/custom-electron-titlebar');
const titlebar = new customTitlebar.Titlebar({
  backgroundColor: customTitlebar.Color.fromHex('#191d19'),
  shadow: true
});

titlebar.updateTitle(i18n.__('Export preview') + ' – Timetable DESC');
titlebar.setHorizontalAlignment('left');
titlebar.updateMenu({ items: [] });

// UI script
const root = (
  <ExportPreviewWindow
    i18n={i18n}
    renderData={renderData}
  />

);
ReactDOM.render(root, document.getElementById('app'));
