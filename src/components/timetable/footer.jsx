/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * footer.jsx - page footer with pagination and software attribution
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

export class Footer extends React.PureComponent {
  constructor () {
    super();

    this.containerRef = React.createRef();
    this.leftRef = React.createRef();
    this.centerRef = React.createRef();
    this.rightRef = React.createRef();
  }

  componentDidMount () {
    if (this.props.exportMode) {
      window.requestAnimationFrame(() => {
        if (this.props.onExportDataReady) {
          const exportObj = {};

          const containerRect = this.containerRef.current.getBoundingClientRect();
          exportObj.footerRect = {
            x: containerRect.x,
            y: containerRect.y,
            width: containerRect.width,
            height: containerRect.height
          };

          exportObj.isMono = this.props.mono;
          exportObj.fontSize = 10;

          exportObj.left = {};
          exportObj.left.text = this.props.leftText;

          const leftRect = this.leftRef.current.getBoundingClientRect();
          exportObj.left.rect = {
            x: leftRect.x,
            y: leftRect.y,
            width: leftRect.width,
            height: leftRect.height
          };

          exportObj.center = {};
          exportObj.center.text = this.props.centerText;

          const centerRect = this.centerRef.current.getBoundingClientRect();
          exportObj.center.rect = {
            x: centerRect.x,
            y: centerRect.y,
            width: centerRect.width,
            height: centerRect.height
          };

          exportObj.right = {};
          exportObj.right.text = this.props.rightText;

          const rightRect = this.rightRef.current.getBoundingClientRect();
          exportObj.right.rect = {
            x: rightRect.x,
            y: rightRect.y,
            width: rightRect.width,
            height: rightRect.height
          };

          this.props.onExportDataReady(exportObj);
        }
      });
    }
  }

  render () {
    return (
      <div className='tt-footer' ref={this.containerRef} style={{ borderTopColor: this.props.themeColors.accent, color: this.props.themeColors.mainText }}>
        <div className='tt-footer-left' ref={this.leftRef}>{this.props.leftText}</div>
        <div className='tt-footer-center' ref={this.centerRef}>{this.props.centerText}</div>
        <div className='tt-footer-right' ref={this.rightRef}>{this.props.rightText}</div>
      </div>
    );
  }
}

Footer.propTypes = {
  leftText: PropTypes.string,
  centerText: PropTypes.string,
  rightText: PropTypes.string,
  exportMode: PropTypes.bool,
  onExportDataReady: PropTypes.func,
  mono: PropTypes.bool,
  themeColors: PropTypes.object
};
