/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * lesson-card.jsx - single lesson in table field
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

const cardDefColor = '#cccccc';
const squishedThreshold = 20;

function hexToRGB (hex, alpha) {
  var r = parseInt(hex.slice(1, 3), 16);
  var g = parseInt(hex.slice(3, 5), 16);
  var b = parseInt(hex.slice(5, 7), 16);

  if (alpha) {
    return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + alpha + ')';
  } else {
    return 'rgb(' + r + ', ' + g + ', ' + b + ')';
  }
}

export class LessonCard extends React.PureComponent {
  constructor (props) {
    super(props);

    this.state = {
      squished: true
    };

    this.containerRef = React.createRef();
    this.subjectRef = React.createRef();
    this.groupsRef = React.createRef();

    this.infoRef = React.createRef();
    this.teacherRef = React.createRef();
    this.classroomRef = React.createRef();

    this.subjectDisplayer = React.createRef();
    this.subjectContent = React.createRef();
  }

  sendExportInformation () {
    if (!this.props.exportMode) return;
    const exportObj = {};

    if (this.props.cardData.isEmpty) {
      this.props.onExportDataReady({});
      return;
    }

    const containerRect = this.containerRef.current.getBoundingClientRect();

    exportObj.isMono = this.props.mono;

    exportObj.cardRect = {
      x: containerRect.x,
      y: containerRect.y,
      width: containerRect.width,
      height: containerRect.height
    };

    exportObj.colors = [...this.props.cardData.colorRGBs];
    if (exportObj.colors.length === 0) {
      exportObj.colors.push(cardDefColor);
    }

    if (this.subjectRef.current) {
      exportObj.subject = {};
      exportObj.subject.fontSize = 10;
      exportObj.subject.text = this.props.cardData.subjectName;

      const subjectRect = this.subjectRef.current.getBoundingClientRect();
      exportObj.subject.rect = {
        x: subjectRect.x,
        y: subjectRect.y,
        width: subjectRect.width,
        height: subjectRect.height
      };
    } else if (this.subjectContent.current) {
      exportObj.subject = {};
      exportObj.subject.fontSize = parseInt(
        this.subjectContent.current.style.fontSize);

      exportObj.subject.text = this.props.cardData.subjectName;

      const subjectRect = this.subjectContent.current.getBoundingClientRect();
      exportObj.subject.rect = {
        x: subjectRect.x,
        y: subjectRect.y,
        width: subjectRect.width,
        height: subjectRect.height
      };
    }

    exportObj.groups = {};
    exportObj.groups.text = this.getGroupNames();
    exportObj.groups.fontSize = this.props.wideRows ? 9 : 7;
    exportObj.groups.isSquished = (this.groupsRef.current
      .getAttribute('data-squished') === 'true');

    const groupsRect = this.groupsRef.current.getBoundingClientRect();
    exportObj.groups.rect = {
      x: groupsRect.x,
      y: groupsRect.y,
      width: groupsRect.width,
      height: groupsRect.height
    };

    exportObj.info = {};
    exportObj.info.text = this.props.cardData.teacherNames.join(',');
    exportObj.info.fontSize = this.props.wideRows ? 9 : 8;

    const infoRect = this.infoRef.current.getBoundingClientRect();
    const teacherRect = this.teacherRef.current.getBoundingClientRect();
    exportObj.info.rect = {
      x: teacherRect.x,
      y: teacherRect.y,
      width: infoRect.width,
      height: teacherRect.height
    };

    exportObj.classroom = {};
    exportObj.classroom.text = this.props.cardData.classroomNames.join(',');
    exportObj.classroom.fontSize = this.props.wideRows ? 9 : 8;

    const classroomRect = this.classroomRef.current.getBoundingClientRect();
    exportObj.classroom.rect = {
      x: classroomRect.x,
      y: classroomRect.y,
      width: classroomRect.width,
      height: classroomRect.height
    };

    if (this.props.onExportDataReady) {
      this.props.onExportDataReady(exportObj);
    }
  }

  componentDidMount () {
    window.requestAnimationFrame(() => {
      // setTimeout(() => {
      if (this.subjectDisplayer.current) {
        if (this.subjectDisplayer.current.offsetHeight > squishedThreshold) {
          this.setState({ squished: false });
        } else {
          this.sendExportInformation();
        }
      } else {
        this.sendExportInformation();
      }
      // }, 0);
    });
  }

  componentDidUpdate () {
    // Set font size according to field height
    window.requestAnimationFrame(() => {
      // setTimeout(() => {
      if (this.subjectDisplayer.current && this.subjectContent.current) {
        const containerWidth = this.subjectDisplayer.current.offsetWidth;
        const containerHeight = this.subjectDisplayer.current.offsetHeight;

        const textWidth = this.subjectContent.current.offsetWidth;
        const textHeight = this.subjectContent.current.offsetHeight;

        const scaling = Math.min(containerWidth / textWidth,
          containerHeight / textHeight);

        let fontSize = Math.floor(scaling * 9 * 0.5);

        if (fontSize > 20) fontSize = 20;
        if (fontSize < 10) fontSize = 10;

        this.subjectContent.current.style.fontSize = fontSize + 'pt';

        window.requestAnimationFrame(() => this.sendExportInformation());
      }
      // }, 0);
    });
  }

  getBackground () {
    if (this.props.mono) return 'white';

    if (this.props.cardData.colorRGBs.length === 0) {
      return hexToRGB(cardDefColor, 0.2);
    } else if (this.props.cardData.colorRGBs.length === 1) {
      return hexToRGB(this.props.cardData.colorRGBs[0], 0.2);
    } else {
      const colorArr = [];
      this.props.cardData.colorRGBs.forEach(color => {
        colorArr.push(hexToRGB(color, 0.2));
        colorArr.push(hexToRGB(color, 0.2));
        colorArr.push(hexToRGB(color, 0.2));
      });

      return 'linear-gradient(to bottom, ' + colorArr.join(',') + ')';
    }
  }

  getBorderStroke () {
    if (this.props.mono) return undefined;

    if (this.props.cardData.colorRGBs.length === 0) {
      return 'linear-gradient(to bottom, ' + cardDefColor + ', ' + cardDefColor + ') 1 100%';
    } else if (this.props.cardData.colorRGBs.length === 1) {
      return 'linear-gradient(to bottom, ' + this.props.cardData.colorRGBs[0] + ',' + this.props.cardData.colorRGBs[0] + ') 1 100%';
    } else {
      const colorArr = [];
      this.props.cardData.colorRGBs.forEach(color => {
        colorArr.push(color);
        colorArr.push(color);
        colorArr.push(color);
      });

      return 'linear-gradient(to bottom, ' + colorArr.join(',') + ') 1 100%';
    }
  }

  getGroupNames () {
    const out = this.props.cardData.groupNames.join(',');
    return out;
  }

  getTopRowContent () {
    if (!this.state.squished) {
      return <span ref={this.groupsRef} data-squished='false' className='tt-groups'>{this.getGroupNames()}</span>;
    }
    return (
      <>
        <span ref={this.subjectRef} className='tt-topsubject'>{
          this.props.cardData.subjectName
        }&nbsp;
        </span>
        <div className='expand-horz' />
        <span ref={this.groupsRef} data-squished='true' className='tt-groups'>{
          this.getGroupNames()
        }
        </span>
      </>
    );
  }

  render () {
    return (
      <div
        ref={this.containerRef} className={'tt-card' +
          ((this.props.cardData.isEmpty) ? ' empty' : '') +
          ((this.props.wideRows) ? ' wide' : '')}
        style={{
          background: this.getBackground(),
          borderImage: this.getBorderStroke()
        }}
      >
        {!this.props.cardData.isEmpty &&
        (
          <>
            <div className='tt-top'>{this.getTopRowContent()}</div>
            <div className='tt-subject' ref={this.subjectDisplayer}>
              {!this.state.squished &&
                <div className='tt-subject-content' ref={this.subjectContent}>
                  {this.props.cardData.subjectName}
                </div>}
            </div>
            <div className='tt-info' ref={this.infoRef}>
              <div className='tt-classroom' ref={this.classroomRef}>{
                this.props.cardData.classroomNames.join(',')
              }
              </div>
              <div className='tt-teacher' ref={this.teacherRef}>{
                this.props.cardData.teacherNames.join(',')
              }
              </div>
            </div>
          </>
        )}
      </div>
    );
  }
}

LessonCard.propTypes = {
  cardData: PropTypes.object.isRequired,
  wideRows: PropTypes.bool,
  mono: PropTypes.bool,
  exportMode: PropTypes.bool,
  onExportDataReady: PropTypes.func
};
