/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * text-header.jsx - page header with textual branding
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

export class TextHeader extends React.PureComponent {
  constructor (props) {
    super(props);

    this.headerContainerRef = React.createRef();
    this.titleRef = React.createRef();
    this.tutorRef = React.createRef();
    this.datesRef = React.createRef();
    this.textRef = React.createRef();
  }

  componentDidMount () {
    if (this.props.exportMode) {
      window.requestAnimationFrame(() => {
        if (this.props.onExportDataReady) {
          const exportObj = {};
          const headerRect = this.headerContainerRef.current.getBoundingClientRect();

          exportObj.isMono = this.props.mono;

          exportObj.headerRect = {
            x: headerRect.x,
            y: headerRect.y,
            width: headerRect.width,
            height: headerRect.height
          };

          const titleRect = this.titleRef.current.getBoundingClientRect();

          exportObj.title = {};
          exportObj.title.rect = {
            x: titleRect.x,
            y: titleRect.y,
            width: titleRect.width,
            height: titleRect.height
          };
          exportObj.title.fontSize = 20;
          if (this.props.categoryClass === 'teacher') {
            exportObj.title.fontSize = 16;
          }

          exportObj.title.text1 = this.props.categoryText + ' ';
          exportObj.title.text2 = this.props.sourceText;

          exportObj.tutor = null;

          if (this.tutorRef.current) {
            const tutorRect = this.tutorRef.current.getBoundingClientRect();

            exportObj.tutor = {};
            exportObj.tutor.rect = {
              x: tutorRect.x,
              y: tutorRect.y,
              width: tutorRect.width,
              height: tutorRect.height
            };
            exportObj.tutor.fontSize = 10;

            exportObj.tutor.text1 = this.props.tutorLabel + ' ';
            exportObj.tutor.text2 = this.props.tutorName;
          }

          exportObj.dates = null;

          if (this.datesRef.current) {
            const datesRect = this.datesRef.current.getBoundingClientRect();

            exportObj.dates = {};
            exportObj.dates.rect = {
              x: datesRect.x,
              y: datesRect.y,
              width: datesRect.width,
              height: datesRect.height
            };
            exportObj.dates.fontSize = 10;

            exportObj.dates.text = (this.props.exportDateText +
              '\n' + this.props.renderDateText);
          }

          exportObj.text = {};
          const textRect = this.textRef.current.getBoundingClientRect();

          exportObj.text.rect = {
            x: textRect.x,
            y: textRect.y,
            width: textRect.width,
            height: textRect.height
          };
          exportObj.text.fontSize = 10;
          exportObj.text.text = this.props.brandingText;

          this.props.onExportDataReady(exportObj);
        }
      });
    }
  }

  render () {
    return (
      <div
        className='tt-header horizontal-flex' ref={this.headerContainerRef}
        style={{ color: this.props.themeColors.mainText, borderBottomColor: this.props.themeColors.accent }}
      >
        <div className='tt-branding' ref={this.textRef}>
          <span>{
            this.props.brandingText.split('\n').map(function (item, idx) {
              return (
                <span key={idx}>
                  {item}
                  <br />
                </span>
              );
            })
          }
          </span>
        </div>
        <div className='expand-horz' style={{ flexBasis: 0 }}>
          <div className={'tt-title ' + this.props.categoryClass} ref={this.titleRef}>
            <span className='tt-category' style={{ color: this.props.themeColors.secondaryText }}>{this.props.categoryText} </span>
            <span className='tt-source'>{this.props.sourceText}</span>
          </div>
          {
            (this.props.categoryClass === 'class') &&
            (
              <div className='tt-tutor' ref={this.tutorRef}>
                <span style={{ fontSize: '10pt' }}>
                  <span className='tt-category' style={{ color: this.props.themeColors.secondaryText }}>{this.props.tutorLabel} </span>
                  <span className='tt-source'>{this.props.tutorName}</span>
                </span>
              </div>
            )
          }
        </div>
        {this.props.showDates &&
          <div className='tt-dates' ref={this.datesRef}>
            <span>{this.props.exportDateText}</span>
            <br />
            <span>{this.props.renderDateText}</span>
          </div>}
      </div>
    );
  }
}

TextHeader.propTypes = {
  brandingText: PropTypes.string,
  categoryText: PropTypes.string,
  categoryClass: PropTypes.string,
  sourceText: PropTypes.string,
  exportDateText: PropTypes.string,
  renderDateText: PropTypes.string,
  showDates: PropTypes.bool,
  tutorLabel: PropTypes.string,
  tutorName: PropTypes.string,
  exportMode: PropTypes.bool,
  onExportDataReady: PropTypes.func,
  mono: PropTypes.bool,
  themeColors: PropTypes.object
};
