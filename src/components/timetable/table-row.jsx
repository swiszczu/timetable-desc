/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * table-row.jsx - single table row containing lesson number,
 * start and end time
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { LessonCard } from './lesson-card.jsx';

export class TableRow extends React.PureComponent {
  constructor (props) {
    super(props);

    this.containerRef = React.createRef();

    this.ordinalRef = React.createRef();
    this.ordinalNoRef = React.createRef();
    this.ordinalStartRef = React.createRef();
    this.ordinalEndRef = React.createRef();

    this.mondayRef = React.createRef();
    this.tuesdayRef = React.createRef();
    this.wednesdayRef = React.createRef();
    this.thursdayRef = React.createRef();
    this.fridayRef = React.createRef();
    this.saturdayRef = React.createRef();

    this.exportObj = {
      rowRect: null,
      days: new Array(props.showSaturday ? 6 : 5)
    };

    for (let i = 0; i < this.exportObj.days.length; i++) {
      this.exportObj.days[i] = {};
    }

    this.totalCardsToReceive = 0;
    this.cardsReceived = 0;
  }

  clientBoundToRect (bound) {
    return {
      x: bound.x,
      y: bound.y,
      width: bound.width,
      height: bound.height
    };
  }

  trySendExportData () {
    let ok = true;
    Object.keys(this.exportObj).forEach(key => {
      if (this.exportObj[key] == null) {
        ok = false;
      }
    });

    this.exportObj.days.forEach(day => {
      if (!day) ok = false;
    });

    if (this.cardsReceived < this.totalCardsToReceive) ok = false;

    if (ok && this.props.onExportDataReady) {
      this.props.onExportDataReady(this.exportObj);
    }
  }

  componentDidMount () {
    if (this.props.exportMode) {
      window.requestAnimationFrame(() => {
        if (this.props.onExportDataReady) {
          const exportObj = this.exportObj;

          exportObj.isMono = this.props.mono;
          exportObj.isInversed = this.props.rowObject.number % 2;

          const containerRect = this.containerRef.current.getBoundingClientRect();

          exportObj.rowRect = this.clientBoundToRect(containerRect);

          const ordinalRect = this.ordinalRef.current.getBoundingClientRect();
          exportObj.ordinalRect = this.clientBoundToRect(ordinalRect);

          exportObj.number = {};
          exportObj.number.fontSize = 22;
          exportObj.number.text = this.props.rowObject.number;

          const numberRect = this.ordinalNoRef.current.getBoundingClientRect();
          exportObj.number.rect = this.clientBoundToRect(numberRect);

          exportObj.start = {};
          exportObj.start.fontSize = 8.5;
          exportObj.start.text = this.props.rowObject.startTime;

          const startRect = this.ordinalStartRef.current.getBoundingClientRect();
          exportObj.start.rect = this.clientBoundToRect(startRect);

          exportObj.end = {};
          exportObj.end.fontSize = 8.5;
          exportObj.end.text = this.props.rowObject.endTime;

          const endRect = this.ordinalEndRef.current.getBoundingClientRect();
          exportObj.end.rect = this.clientBoundToRect(endRect);

          exportObj.days[0].rect = this.clientBoundToRect(
            this.mondayRef.current.getBoundingClientRect());

          exportObj.days[1].rect = this.clientBoundToRect(
            this.tuesdayRef.current.getBoundingClientRect());

          exportObj.days[2].rect = this.clientBoundToRect(
            this.wednesdayRef.current.getBoundingClientRect());

          exportObj.days[3].rect = this.clientBoundToRect(
            this.thursdayRef.current.getBoundingClientRect());

          exportObj.days[4].rect = this.clientBoundToRect(
            this.fridayRef.current.getBoundingClientRect());

          if (this.saturdayRef.current) {
            exportObj.days[5].rect = this.clientBoundToRect(
              this.saturdayRef.current.getBoundingClientRect());
          }

          this.trySendExportData();
        }
      });
    }
  }

  handleCardExportData (day, id, data) {
    if (!day.data[id]) {
      day.data[id] = data;
      this.cardsReceived++;
      this.trySendExportData();
    }
  }

  getCardsForDay (day) {
    this.exportObj.days[day].data = new Array(this.props.rowData[day].length);

    return this.props.rowData[day].map((cardData, i) => {
      const key = this.props.probeId + '|' + day + '|' + i;
      this.totalCardsToReceive++;

      return (
        <LessonCard
          key={key}
          cardData={cardData}
          wideRows={this.props.wideRows}
          mono={this.props.mono}
          exportMode={this.props.exportMode}
          onExportDataReady={this.handleCardExportData.bind(this,
            this.exportObj.days[day], i)}
        />
      );
    });
  }

  render () {
    this.totalCardsToReceive = 0;
    this.cardsReceived = 0;
    return (
      <div
        ref={this.containerRef}
        className={'tt-row' + ((this.props.rowObject.number % 2) ? ' inversed' : '') +
          ' horizontal-flex' + (this.props.isLastPage ? ' last-page' : '')}
        data-period-no={this.props.rowObject.number}
        style={{ color: this.props.themeColors.mainText, borderBottomColor: this.props.themeColors.lessonDiv }}
      >
        <div className='tt-row-num' ref={this.ordinalRef} style={{ color: this.props.themeColors.lessonTime, borderRightColor: this.props.themeColors.mainGrid }}>
          <div className='tt-row-number' ref={this.ordinalNoRef}>{
            this.props.rowObject.number
          }
          </div>
          <div className='tt-row-start' ref={this.ordinalStartRef}>{
            this.props.rowObject.startTime
          }
          </div>
          <div className='tt-row-end' ref={this.ordinalEndRef}>{
            this.props.rowObject.endTime
          }
          </div>
        </div>
        <div className='tt-row-day' ref={this.mondayRef} style={{ borderRightColor: this.props.themeColors.mainGrid }}>
          {this.getCardsForDay(0)}
        </div>
        <div className='tt-row-day' ref={this.tuesdayRef} style={{ borderRightColor: this.props.themeColors.mainGrid }}>
          {this.getCardsForDay(1)}
        </div>
        <div className='tt-row-day' ref={this.wednesdayRef} style={{ borderRightColor: this.props.themeColors.mainGrid }}>
          {this.getCardsForDay(2)}
        </div>
        <div className='tt-row-day' ref={this.thursdayRef} style={{ borderRightColor: this.props.themeColors.mainGrid }}>
          {this.getCardsForDay(3)}
        </div>
        <div className='tt-row-day' ref={this.fridayRef} style={{ borderRightColor: this.props.themeColors.mainGrid }}>
          {this.getCardsForDay(4)}
        </div>
        {
          this.props.showSaturday &&
            <div className='tt-row-day' ref={this.saturdayRef} style={{ borderRightColor: this.props.themeColors.mainGrid }}>
              {this.getCardsForDay(5)}
            </div>
        }
      </div>
    );
  }
}

TableRow.propTypes = {
  rowObject: PropTypes.object,
  rowData: PropTypes.array,
  showSaturday: PropTypes.bool,
  isLastPage: PropTypes.bool,
  wideRows: PropTypes.bool,
  probeId: PropTypes.number,
  mono: PropTypes.bool,
  exportMode: PropTypes.bool,
  onExportDataReady: PropTypes.func,
  themeColors: PropTypes.object
};
