/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * table-head.jsx - first row of table containing names of days of the week
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

export class TableHead extends React.PureComponent {
  constructor (props) {
    super(props);

    this.containerRef = React.createRef();
    this.ordinalRef = React.createRef();
    this.mondayRef = React.createRef();
    this.tuesdayRef = React.createRef();
    this.wednesdayRef = React.createRef();
    this.thursdayRef = React.createRef();
    this.fridayRef = React.createRef();
    this.saturdayRef = React.createRef();
  }

  componentDidMount () {
    if (this.props.exportMode) {
      window.requestAnimationFrame(() => {
        if (this.props.onExportDataReady) {
          const exportObj = {};

          const containerRect = this.containerRef.current.getBoundingClientRect();
          const ordinalRect = this.ordinalRef.current.getBoundingClientRect();
          const mondayRect = this.mondayRef.current.getBoundingClientRect();
          const tuesdayRect = this.tuesdayRef.current.getBoundingClientRect();
          const wednesdayRect = this.wednesdayRef.current.getBoundingClientRect();
          const thursdayRect = this.thursdayRef.current.getBoundingClientRect();
          const fridayRect = this.fridayRef.current.getBoundingClientRect();

          exportObj.isMono = this.props.mono;
          exportObj.fontSize = 12;

          exportObj.containerRect = {
            x: containerRect.x,
            y: containerRect.y,
            width: containerRect.width,
            height: containerRect.height
          };

          exportObj.ordinalRect = {
            x: ordinalRect.x,
            y: ordinalRect.y,
            width: ordinalRect.width,
            height: ordinalRect.height
          };

          exportObj.ordinalText = this.props.i18n.__('No.');

          exportObj.daysRects = [];
          exportObj.daysTexts = [];

          exportObj.daysRects[0] = {
            x: mondayRect.x,
            y: mondayRect.y,
            width: mondayRect.width,
            height: mondayRect.height
          };

          exportObj.daysTexts[0] = this.props.i18n.__('Monday');

          exportObj.daysRects[1] = {
            x: tuesdayRect.x,
            y: tuesdayRect.y,
            width: tuesdayRect.width,
            height: tuesdayRect.height
          };

          exportObj.daysTexts[1] = this.props.i18n.__('Tuesday');

          exportObj.daysRects[2] = {
            x: wednesdayRect.x,
            y: wednesdayRect.y,
            width: wednesdayRect.width,
            height: wednesdayRect.height
          };

          exportObj.daysTexts[2] = this.props.i18n.__('Wednesday');

          exportObj.daysRects[3] = {
            x: thursdayRect.x,
            y: thursdayRect.y,
            width: thursdayRect.width,
            height: thursdayRect.height
          };

          exportObj.daysTexts[3] = this.props.i18n.__('Thursday');

          exportObj.daysRects[4] = {
            x: fridayRect.x,
            y: fridayRect.y,
            width: fridayRect.width,
            height: fridayRect.height
          };

          exportObj.daysTexts[4] = this.props.i18n.__('Friday');

          if (this.saturdayRef.current) {
            const saturdayRect = this.saturdayRef.current.getBoundingClientRect();

            exportObj.daysRects[5] = {
              x: saturdayRect.x,
              y: saturdayRect.y,
              width: saturdayRect.width,
              height: saturdayRect.height
            };

            exportObj.daysTexts[5] = this.props.i18n.__('Saturday');
          }

          this.props.onExportDataReady(exportObj);
        }
      });
    }
  }

  render () {
    return (
      <div className='tt-head horizontal-flex' ref={this.containerRef} style={{ borderBottomColor: this.props.themeColors.mainGrid }}>
        <div className='tt-head-num' ref={this.ordinalRef} style={{ borderRightColor: this.props.themeColors.mainGrid, color: this.props.themeColors.lessonTime }}>{this.props.i18n.__('No.')}</div>
        <div className='tt-head-day' ref={this.mondayRef} style={{ borderRightColor: this.props.themeColors.mainGrid, color: this.props.themeColors.accent }}>{this.props.i18n.__('Monday')}</div>
        <div className='tt-head-day' ref={this.tuesdayRef} style={{ borderRightColor: this.props.themeColors.mainGrid, color: this.props.themeColors.accent }}>{this.props.i18n.__('Tuesday')}</div>
        <div className='tt-head-day' ref={this.wednesdayRef} style={{ borderRightColor: this.props.themeColors.mainGrid, color: this.props.themeColors.accent }}>{this.props.i18n.__('Wednesday')}</div>
        <div className='tt-head-day' ref={this.thursdayRef} style={{ borderRightColor: this.props.themeColors.mainGrid, color: this.props.themeColors.accent }}>{this.props.i18n.__('Thursday')}</div>
        <div className='tt-head-day' ref={this.fridayRef} style={{ borderRightColor: this.props.themeColors.mainGrid, color: this.props.themeColors.accent }}>{this.props.i18n.__('Friday')}</div>
        {
          this.props.showSaturday &&
            <div className='tt-head-day' ref={this.saturdayRef}>{this.props.i18n.__('Saturday')}</div>
        }
      </div>
    );
  }
}

TableHead.propTypes = {
  i18n: PropTypes.object,
  showSaturday: PropTypes.bool,
  exportMode: PropTypes.bool,
  onExportDataReady: PropTypes.func,
  mono: PropTypes.bool,
  themeColors: PropTypes.object
};
