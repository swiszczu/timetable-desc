/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * grade-selector.jsx - horizontal radio button
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

export class GradeSelector extends React.PureComponent {
  constructor (props) {
    super(props);

    this.state = {
      currentGrade: this.props.defaultGrade || 0
    };
  }

  handleClick (i) {
    this.setState({
      currentGrade: i
    });

    if (this.props.onChange) {
      this.props.onChange(i);
    }
  }

  handleKeyPress (i, event) {
    if (event.key === 'Enter') {
      this.setState({
        currentGrade: i
      });

      if (this.props.onChange) {
        this.props.onChange(i);
      }
    }
  }

  getGrades () {
    return this.props.grades.map((element, i) => {
      return (
        <div
          key={i}
          tabIndex='0'
          className={'grade' + ((i === this.state.currentGrade) ? ' active' : '')}
          onClick={this.handleClick.bind(this, i)}
          onKeyPress={this.handleKeyPress.bind(this, i)}
        >
          {element}
        </div>
      );
    });
  }

  render () {
    return (
      <div className='property'>
        <div className='title'>{this.props.label}</div>
        <div className='space' />
        <div className='value'>
          <div className='grade-selector'>
            {this.getGrades()}
          </div>
        </div>
      </div>
    );
  }
}

GradeSelector.propTypes = {
  label: PropTypes.string.isRequired,
  grades: PropTypes.arrayOf(PropTypes.string).isRequired,
  defaultGrade: PropTypes.number,
  onChange: PropTypes.func
};
