/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * logo-browser.jsx - text field with the browse button next to it
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Button } from './button.jsx';

const { ipcRenderer } = window.require('electron');

export class LogoBrowser extends React.PureComponent {
  constructor (props) {
    super(props);

    this.uid = new Date().getTime();

    this.handleIPCBound = this.handleLogoOpened.bind(this);
  }

  componentDidMount () {
    ipcRenderer.on('logo:opened', this.handleIPCBound);
  }

  componentWillUnmount () {
    ipcRenderer.removeListener('logo:opened', this.handleIPCBound);
  }

  handleLogoOpened (event, data) {
    if (data.callerId === this.uid) {
      if (this.props.onPathChanged) { this.props.onPathChanged(data.path); }
    }
  }

  handleClick () {
    ipcRenderer.send('logo:open', {
      callerId: this.uid
    });
  }

  render () {
    return (
      <div className='horizontal-flex'>
        <input
          type='text'
          className='text-field expand-horz'
          disabled={this.props.disabled}
          value={this.props.filePath} readOnly
        />
        <Button
          text={this.props.i18n.__('Open...')}
          disabled={this.props.disabled}
          onClick={this.handleClick.bind(this)}
        />
      </div>
    );
  }
}

LogoBrowser.propTypes = {
  i18n: PropTypes.object,
  disabled: PropTypes.bool,
  onPathChanged: PropTypes.func,
  filePath: PropTypes.string
};
