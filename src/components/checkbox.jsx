/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * checkbox.jsx - basic checkbox
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

export class Checkbox extends React.Component {
  handleChange (event) {
    if (this.props.onChange) {
      this.props.onChange(event.target.checked);
    }
  }

  render () {
    return (
      <label className='select-control'>
        <div className='select-text'>{this.props.text}</div>
        <input
          type='checkbox'
          checked={this.props.checked}
          onChange={this.handleChange.bind(this)}
        />
        <div className='checkmark' />
      </label>
    );
  }
}

Checkbox.propTypes = {
  checked: PropTypes.bool.isRequired,
  text: PropTypes.string,
  onChange: PropTypes.func
};
