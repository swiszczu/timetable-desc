/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * tab-appearance.jsx - Appearance tab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Checkbox } from './checkbox.jsx';
import { RadioDisabler } from './radio-disabler.jsx';
import { RadioController } from './radio-controller.jsx';
import { Label } from './label.jsx';
import { LogoBrowser } from './logo-browser.jsx';
import { ColorPickerGroup } from './colorpicker-group.jsx';
import { MarginSelector } from './margin-selector.jsx';
import { ColorProperty } from './color-property.jsx';

import memo from 'memoize-one';

const fs = window.require('fs');
const defaultMargin = 0.5;

// https://godsnotwheregodsnot.blogspot.com/2012/09/color-distribution-methodology.html
const defaultPaletteArr = [
  // Grade 1
  [
    '#FFDBE5', '#7A4900', '#0000A6', '#63FFAC', '#B79762', '#004D43', '#8FB0FF', '#997D87',
    '#5A0007', '#809693'
  ],

  // Grade 2
  [
    '#61615A', '#BA0900', '#6B7900', '#00C2A0', '#FFAA92', '#FF90C9', '#B903AA', '#D16100',
    '#DDEFFF', '#000035'
  ],

  // Grade 3
  [
    '#00489C', '#6F0062', '#0CBD66', '#EEC3FF', '#456D75', '#B77B68',
    '#7A87A1', '#788D66'
  ]
];

export class AppearanceTab extends React.PureComponent {
  constructor (props) {
    super(props);

    this.classroomObj = memo(this.getClassroomsColors);
    this.classObj = memo(this.getClassesObjects);
    this.themeColors = memo(this.getThemeColors);

    this.propertyColorLUT = {
      accent: { label: props.i18n.__('Accent'), def: '#294E8E' },
      mainText: { label: props.i18n.__('Main text'), def: '#000000' },
      secondaryText: { label: props.i18n.__('Secondary text'), def: '#888888' },
      lessonTime: { label: props.i18n.__('Lesson number and time'), def: '#888888' },
      mainGrid: { label: props.i18n.__('Main table grid line'), def: '#888888' },
      lessonDiv: { label: props.i18n.__('Lesson divider'), def: '#AAAAAA' }
    };

    const themeColors = {};

    // Convert lookup table to state
    Object.keys(this.propertyColorLUT).forEach(key => {
      const colorDef = this.propertyColorLUT[key];
      themeColors[key] = colorDef.def;
    });

    this.state = {
      logoPath: '',
      logoBase64: '',
      validityVisible: true,
      saturdayVisible: false,
      mono: false,
      margins: {
        left: defaultMargin,
        right: defaultMargin,
        top: defaultMargin,
        bottom: defaultMargin
      },
      brandingType: 0,
      brandingText: '',
      classColors: this.getDefaultClassColors(props.parser.timetableInfo),
      themeColors
    };
  }

  sendChange () {
    if (this.props.onChange) {
      this.props.onChange({
        margins: this.internal.margins,
        logoPath: this.state.logoPath
      });
    }
  }

  getDefaultClassColors (timetableInfo) {
    const colorArray = [];

    const gradeCount = [0, 0, 0];
    Object.keys(timetableInfo.classAssoc).forEach((key, i) => {
      const element = timetableInfo.classAssoc[key];

      const set = ((element.gradeNumber || 1) - 1) % 3;
      const el = gradeCount[set] % 10;
      colorArray[i] = defaultPaletteArr[set][el];

      gradeCount[set]++;
    });

    return colorArray;
  }

  handleThemeColorChange (colorId, newColor) {
    this.setState(state => {
      const themeColors = { ...state.themeColors };

      themeColors[colorId] = newColor;

      return { themeColors };
    });
  }

  getThemeColors (themeColors) {
    console.log(themeColors);
    return Object.keys(themeColors).map(key => {
      return (
        <ColorProperty
          key={key}
          i18n={this.props.i18n}
          label={this.propertyColorLUT[key].label}
          color={themeColors[key]}
          onChange={this.handleThemeColorChange.bind(this, key)}
        />
      );
    });
  }

  handleLogoPathChange (path) {
    try {
      const buf = fs.readFileSync(path);
      const base64 = buf.toString('base64');

      let mime = 'image/*';

      if (path.endsWith('.jpg') || path.endsWith('.jpeg')) {
        mime = 'image/jpeg';
      } else if (path.endsWith('.png')) {
        mime = 'image/png';
      } else if (path.endsWith('.gif')) {
        mime = 'image/gif';
      } else if (path.endsWith('.bmp')) {
        mime = 'image/bmp';
      }

      const dataUrl = 'data:' + mime + ';base64,' + base64;

      this.setState({
        logoPath: path,
        logoBase64: dataUrl
      });

      this.sendChange();
    } catch (error) {
      console.error(error);
    }
  }

  getClassesObjects (timetableInfo) {
    return Object.keys(timetableInfo.classAssoc).map((element) => {
      return {
        id: timetableInfo.classAssoc[element].id,
        name: timetableInfo.classAssoc[element].name,
        grade: timetableInfo.classAssoc[element].gradeNumber - 1
      };
    });
  }

  getClassroomsColors (timetableInfo) {
    return (
      <ColorPickerGroup objects={
        Object.keys(this.props.parser.timetableInfo.classroomAssoc).map((element, i) => {
          return {
            id: timetableInfo.classroomAssoc[element].id,
            name: timetableInfo.classroomAssoc[element].name,
            grade: i % 10
          };
        })
      }
      />
    );
  }

  handleMarginChange (newMargins) {
    this.setState({
      margins: newMargins
    });
  }

  handleUpdateState (stateName, newValue) {
    const updateObj = {};
    updateObj[stateName] = newValue;
    this.setState(updateObj);
  }

  handleChangeText (event) {
    this.setState({
      brandingText: event.target.value
    });
  }

  handleChangeColor (index, newColor) {
    this.setState(state => {
      const colorsClone = [...state.classColors];

      colorsClone[index] = newColor;

      return {
        classColors: colorsClone
      };
    });
  }

  render () {
    return (
      <div className={this.props.className}>
        <div className='property'>
          <span className='title'>{this.props.i18n.__('General rendering options')}:</span>
          <div className='section-spacer' />
        </div>
        <div className='hierarchy-container'>
          <Checkbox
            text={this.props.i18n.__('Append validity information on render')}
            checked={this.state.validityVisible}
            onChange={this.handleUpdateState.bind(this, 'validityVisible')}
          />
          <Checkbox
            text={this.props.i18n.__('Show Saturday on timetable render')}
            checked={this.state.saturdayVisible}
            onChange={this.handleUpdateState.bind(this, 'saturdayVisible')}
          />
          <Checkbox
            text={this.props.i18n.__('Generate monochromatic (gray-scale) timetable version')}
            checked={this.state.mono}
            onChange={this.handleUpdateState.bind(this, 'mono')}
          />
        </div>
        <br />
        <div className='property'>
          <span className='title'>{this.props.i18n.__('Page margins')}:</span>
          <div className='section-spacer' />
        </div>
        <div className='hierarchy-container'>
          <MarginSelector
            i18n={this.props.i18n}
            onChange={this.handleMarginChange.bind(this)}
            margins={this.state.margins}
          />
        </div>
        <div className='property'>
          <span className='title'>{this.props.i18n.__('Branding type')}:</span>
          <div className='section-spacer' />
        </div>
        <div className='hierarchy-container'>
          <RadioController
            name='app_brand_type'
            checkedId={this.state.brandingType}
            onChange={this.handleUpdateState.bind(this, 'brandingType')}
          >

            <RadioDisabler text={this.props.i18n.__('Use graphical logo')}>
              <Label text={this.props.i18n.__('Select a file you want to show as logo on rendered timetables:')} />
              {this.state.logoBase64.length > 10 && (
                <>
                  <img
                    className='logo-preview'
                    src={this.state.logoBase64}
                  />
                </>
              )}
              <LogoBrowser
                i18n={this.props.i18n}
                onPathChanged={this.handleLogoPathChange.bind(this)}
                filePath={this.state.logoPath}
              />
            </RadioDisabler>
            <RadioDisabler text={this.props.i18n.__('Use text')}>
              <Label text={this.props.i18n.__('Enter text to show on rendered timetables:')} />
              <input
                type='text'
                className='text-field full-width'
                value={this.state.brandingText}
                onChange={this.handleChangeText.bind(this)}
              />
            </RadioDisabler>
            <RadioDisabler text={this.props.i18n.__('None')} />
          </RadioController>
        </div>
        <div className='property'>
          <span className='title'>{this.props.i18n.__('Color palette')}:</span>
          <div className='section-spacer' />
        </div>
        <div className='hierarchy-container'>
          {this.themeColors(this.state.themeColors)}

          <Label text={this.props.i18n.__('Classes') + ':'} />
          <div className='hierarchy-container'>
            <ColorPickerGroup
              i18n={this.props.i18n}
              objects={this.classObj(this.props.parser.timetableInfo)}
              colors={this.state.classColors}
              onChange={this.handleChangeColor.bind(this)}
            />
          </div>
        </div>
      </div>
    );
  }

  // IO
  serializeState () {
    const output = Object.assign({}, this.state);
    delete output.classColors;

    output.colors = {};

    Object.keys(this.props.parser.timetableInfo.classAssoc).forEach((key, i) => {
      const classObj = this.props.parser.timetableInfo.classAssoc[key];

      output.colors[classObj.name] = {
        color: this.state.classColors[i]
      };
    });

    return output;
  }

  deserializeState (obj) {
    if (!obj) return;

    const input = Object.assign({}, obj);
    input.classColors = this.getDefaultClassColors(
      this.props.parser.timetableInfo);

    Object.keys(this.props.parser.timetableInfo.classAssoc).forEach((key, i) => {
      const classObj = this.props.parser.timetableInfo.classAssoc[key];

      if (input.colors[classObj.name]) {
        input.classColors[i] = input.colors[classObj.name].color;
      }
    });

    delete input.colors;

    const themeColors = { ...this.state.themeColors };

    if (typeof input.themeColors === 'object') {
      Object.keys(input.themeColors).forEach(key => {
        if (this.state.themeColors[key]) {
          themeColors[key] = input.themeColors[key];
        }
      });
    }

    delete input.themeColors;

    input.themeColors = themeColors;
    this.setState(input);
  }
}

AppearanceTab.propTypes = {
  className: PropTypes.string,
  parser: PropTypes.object,
  i18n: PropTypes.object,
  onChange: PropTypes.func
};
