/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * spinner.jsx - busyness indicator
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';

export class Spinner extends React.Component {
  render () {
    return (
      <div className='sk-cube-grid'>
        <div className='sk-cube sk-cube1' />
        <div className='sk-cube sk-cube2' />
        <div className='sk-cube sk-cube3' />
        <div className='sk-cube sk-cube4' />
        <div className='sk-cube sk-cube5' />
        <div className='sk-cube sk-cube6' />
        <div className='sk-cube sk-cube7' />
        <div className='sk-cube sk-cube8' />
        <div className='sk-cube sk-cube9' />
      </div>
    );
  }
}
