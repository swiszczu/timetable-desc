/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * tab-about.jsx - About tab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

const version = require('../backend/version');

export class AboutTab extends React.PureComponent {
  render () {
    return (
      <div className={this.props.className}>
        <div className='center'>
          <img src='img/icon256.png' style={{ width: '192px', height: '192px' }} />
          <p style={{ fontSize: '15pt' }}>
            <b>Timetable DESC</b><br />
            {this.props.i18n.__('version')} {version}
          </p>
          <br />
          <p>Copyright © 2020 Łukasz Świszcz</p>
          <br />
          <p>{this.props.i18n.__('gpl3_text')}</p>
          <br />
          <p><b>{this.props.i18n.__('Used libraries:')}</b></p>
          <p>electron</p>
          <p>i18n</p>
          <p>React</p>
          <p>react-color</p>
          <p>PDFKit</p>
          <p>memoize-one</p>
          <p>custom-electron-titlebar</p>
        </div>
      </div>
    );
  }
}

AboutTab.propTypes = {
  i18n: PropTypes.object,
  className: PropTypes.string
};
