/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * rule-row.jsx - represents a single rule in the group renamer/merger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

export class RuleRow extends React.PureComponent {
  getGroupString () {
    if (this.props.groupArr.length === 0) {
      return this.props.i18n.__('no groups');
    }

    let buffer = '(' + this.props.i18n.__n('%s groups',
      this.props.groupArr.length) + ') ';

    buffer += this.props.groupArr[0];
    for (let i = 1; i < this.props.groupArr.length; i++) {
      buffer += ', ' + this.props.groupArr[i];
    }

    return buffer;
  }

  handleClick () {
    this.props.onDelete();
  }

  handleClickEdit () {
    this.props.onEdit();
  }

  render () {
    return (
      <div className='horizontal-flex rule-row'>
        <div style={{ marginRight: '16px', fontWeight: 700, flexShrink: 0 }}>
          {this.props.name}
        </div>
        <div
          className='expand-horz ellipsis'
          style={{ textAlign: 'right', marginRight: '16px' }}
        >
          {this.getGroupString()}
        </div>
        <div className='text-right'>
          <a
            href='#'
            onClick={this.handleClickEdit.bind(this)}
          >
            {this.props.i18n.__('Edit') + '...'}
          </a>&nbsp;&nbsp;
          <a
            href='#'
            onClick={this.handleClick.bind(this)}
          >
            {this.props.i18n.__('Delete') + '...'}
          </a>
        </div>
      </div>
    );
  }
}

RuleRow.propTypes = {
  name: PropTypes.string.isRequired,
  groupArr: PropTypes.array.isRequired,
  i18n: PropTypes.object.isRequired,
  onDelete: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired
};
