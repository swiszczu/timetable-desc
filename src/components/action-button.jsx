/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * action-button.jsx - big button with image and large text
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

export class ActionButton extends React.PureComponent {
  handleKeyPress (event) {
    if (event.key === 'Enter' && this.props.onClick) {
      this.props.onClick();
    }
  }

  render () {
    return (
      <div
        tabIndex='0'
        className={(this.props.className || '') + ' action-button'}
        onClick={this.props.onClick}
        onKeyPress={this.handleKeyPress.bind(this)}
      >
        <img src={this.props.imagePath} className='ab-image' />
        <div className='ab-text'>{this.props.text}</div>
      </div>
    );
  }
}

ActionButton.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
  imagePath: PropTypes.string,
  className: PropTypes.string
};
