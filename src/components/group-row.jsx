/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * group-row.jsx - checkable group list element
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

export class GroupRow extends React.PureComponent {
  handleClick () {
    if (this.props.onToggle) {
      this.props.onToggle();
    }
  }

  handleKeyPress (event) {
    if (event.key === 'Enter') {
      if (this.props.onToggle) {
        this.props.onToggle();
      }
    }
  }

  render () {
    return (
      <div
        tabIndex='0'
        className={'group-row' + (this.props.active ? ' active' : '')}
        onClick={this.handleClick.bind(this)}
        onKeyPress={this.handleKeyPress.bind(this)}
      >

        <img
          className='row-tick'
          src='img/mark-check.svg'
        />
        <p className='group-name'>{this.props.groupName}</p>
      </div>
    );
  }
}

GroupRow.propTypes = {
  groupName: PropTypes.string.isRequired,
  active: PropTypes.bool,
  onToggle: PropTypes.func
};
