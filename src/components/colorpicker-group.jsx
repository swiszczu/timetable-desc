/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * colorpicker-group.jsx - colorpicker button manager
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { TimetableColorPicker } from './colorpicker.jsx';

export class ColorPickerGroup extends React.PureComponent {
  handleChange (index, color) {
    if (this.props.onChange) {
      this.props.onChange(index, color);
    }
  }

  getContent () {
    return this.props.objects.map((element, i) => {
      return (
        <TimetableColorPicker
          key={i}
          ref={element.id}
          i18n={this.props.i18n}
          text={element.name}
          color={this.props.colors[i]}
          onChange={this.handleChange.bind(this, i)}
        />
      );
    });
  }

  render () {
    return (
      <div className='color-group'>
        {this.getContent()}
      </div>
    );
  }
}

ColorPickerGroup.propTypes = {
  i18n: PropTypes.object,
  objects: PropTypes.array,
  colors: PropTypes.array,
  onChange: PropTypes.func
};
