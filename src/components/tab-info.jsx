/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * tab-info.jsx - Info tab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

const dateFormat = window.require('dateformat');

export class InfoTab extends React.PureComponent {
  calcProperties () {
    return [
      {
        title: this.props.i18n.__('aSc Timetable version'),
        value: this.props.parser.timetableInfo.ascVersion
      },
      {
        title: this.props.i18n.__('Exported on'),
        value: dateFormat(this.props.parser.fileInfo.modified, this.props.i18n.__('dd.mm.yyyy HH:MM:ss'))
      },
      {
        title: this.props.i18n.__('Class count'),
        value: Object.keys(this.props.parser.timetableInfo.classAssoc).length
      },
      {
        title: this.props.i18n.__('Teachers count'),
        value: Object.keys(this.props.parser.timetableInfo.teacherAssoc).length
      },
      {
        title: this.props.i18n.__('Classroom count'),
        value: Object.keys(this.props.parser.timetableInfo.classroomAssoc).length
      },
      {
        title: this.props.i18n.__('Subjects count'),
        value: Object.keys(this.props.parser.timetableInfo.subjectAssoc).length
      }
    ];
  }

  render () {
    const properties = this.calcProperties();
    return (
      <div className={this.props.className}>
        {
          properties.map((prop, i) => {
            return (
              <div key={i} className='property'>
                <span className='title'>{prop.title}</span>
                <div className='dash' />
                <span className='value'>{prop.value}</span>
              </div>
            );
          })
        }
        <br />
        <div className='property'>
          <span className='title'>{this.props.i18n.__('Lesson units')}:</span>
          <div className='section-spacer' />
        </div>
        <div className='lesson-units'>
          {
            this.props.parser.timetableInfo.periodArr.map((period, i) => {
              const cardCnt = this.props.parser.getPeriodStats()[period.name] || 0;
              return (
                <div key={i} className='lesson-unit'>
                  <div>
                    <span className='lesson-number'>{period.name}</span><br />
                    <span className='lesson-time'>{period.startTime} – {period.endTime}</span><br />
                    <span className='lesson-cards'>{this.props.i18n.__n('%s cards', cardCnt)}</span>
                  </div>
                </div>
              );
            })
          }
        </div>
      </div>
    );
  }
}

InfoTab.propTypes = {
  className: PropTypes.string,
  parser: PropTypes.object,
  i18n: PropTypes.object
};
