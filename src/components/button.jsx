/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * button.jsx - basic push button with text
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

export class Button extends React.PureComponent {
  handleOnClick () {
    if (!this.props.disabled) {
      this.props.onClick();
    }
  }

  handleKeyPress (event) {
    if (!this.props.disabled && event.key === 'Enter') {
      this.props.onClick();
    }
  }

  render () {
    return (
      <div
        style={this.props.style} className={
          ('button' + ((this.props.disabled) ? ' button-disabled' : '') +
          ((this.props.small) ? ' button-small' : ''))
        }
      >
        <div
          tabIndex='0'
          className='button-inner'
          title={this.props.title}
          onClick={this.handleOnClick.bind(this)}
          onKeyPress={this.handleKeyPress.bind(this)}
        >
          {this.props.children}
          {this.props.text}
        </div>
        <div className='button-anim'>
          {this.props.children}
          {this.props.text}
        </div>
      </div>
    );
  }
}

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  small: PropTypes.bool,
  children: PropTypes.object,
  title: PropTypes.string,
  style: PropTypes.object
};
