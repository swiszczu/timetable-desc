/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * groups-subjects.jsx - container for all subjects in the group renamer/merger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { SubjectRow } from './subject-row.jsx';

import memo from 'memoize-one';

export class GroupsSubjects extends React.PureComponent {
  constructor (props) {
    super(props);

    this.subjectContent = memo(this.getSubjects);
  }

  handleDeleteSubject (subjectKey, i) {
    this.props.onDelete(subjectKey, i);
  }

  getSubjects (subjects, i18n) {
    return Object.keys(subjects).map((element) => {
      if (subjects[element].groupNames.length === 0) {
        return null;
      }

      return (
        <SubjectRow
          key={subjects[element].id}
          i18n={i18n}
          gradeId={this.props.gradeId}
          subjectId={element}
          name={subjects[element].name}
          groupArr={subjects[element].groupNames}
          fullName={subjects[element].fullName}
          rulesArr={subjects[element].rules}
          onDelete={this.handleDeleteSubject.bind(this, element)}
        />
      );
    });
  }

  render () {
    return (
      <div>
        {this.subjectContent(this.props.rulesState.subjects, this.props.i18n)}
      </div>
    );
  }
}

GroupsSubjects.propTypes = {
  rulesState: PropTypes.object.isRequired,
  gradeId: PropTypes.string.isRequired,
  i18n: PropTypes.object.isRequired,
  onDelete: PropTypes.func.isRequired
};
