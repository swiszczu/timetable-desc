/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * subject-row.jsx - group of rules in the group renamer/merger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { RuleRow } from './rule-row.jsx';

const { ipcRenderer } = window.require('electron');

export class SubjectRow extends React.PureComponent {
  handleAddClick () {
    ipcRenderer.send('rule:add', {
      subjectId: this.props.subjectId,
      subjectName: this.props.fullName,
      groupArr: this.props.groupArr,
      gradeId: this.props.gradeId,
      id: null,
      currentState: null
    });
  }

  handleEditClick (i) {
    ipcRenderer.send('rule:add', {
      subjectId: this.props.subjectId,
      subjectName: this.props.fullName,
      groupArr: this.props.groupArr,
      gradeId: this.props.gradeId,
      id: this.props.rulesArr[i].id,
      currentState: this.props.rulesArr[i]
    });
  }

  handleDeleteClick (i) {
    this.props.onDelete(i);
  }

  getContent () {
    if (this.props.rulesArr.length === 0) {
      return (
        <i className='empty-container-info'>
          {this.props.i18n.__('There are currently no rules ' +
            'defined for this subject') + '...'}
        </i>
      );
    } else {
      return (
        <>
          {
            this.props.rulesArr.map((element, i) => {
              return (
                <RuleRow
                  key={element.id}
                  name={element.newName}
                  groupArr={element.groupNames}
                  i18n={this.props.i18n}
                  onDelete={this.handleDeleteClick.bind(this, i)}
                  onEdit={this.handleEditClick.bind(this, i)}
                />
              );
            })
          }
        </>
      );
    }
  }

  render () {
    return (
      <>
        <div className='subject-header'>
          {this.props.name}
          <div className='text-right'>
            <a
              href='#'
              onClick={this.handleAddClick.bind(this)}
            >
              {'+ ' + this.props.i18n.__('Add rule') + '...'}
            </a>
          </div>
        </div>
        <div className='hierarchy-container nosel'>
          {this.getContent()}
        </div>
      </>
    );
  }
}

SubjectRow.propTypes = {
  name: PropTypes.string.isRequired,
  fullName: PropTypes.string.isRequired,
  i18n: PropTypes.object.isRequired,
  subjectId: PropTypes.string.isRequired,
  gradeId: PropTypes.string.isRequired,
  groupArr: PropTypes.array.isRequired,
  rulesArr: PropTypes.array.isRequired,
  onDelete: PropTypes.func.isRequired
};
