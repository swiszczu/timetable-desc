/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * page.jsx - single preview page containing only timetable components
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { BasicHeader } from './timetable/basic-header.jsx';
import { TextHeader } from './timetable/text-header.jsx';
import { LogoHeader } from './timetable/logo-header.jsx';

import { TableHead } from './timetable/table-head.jsx';
import { TableRow } from './timetable/table-row.jsx';
import { Footer } from './timetable/footer.jsx';

// const path = window.require("path");
const version = require('../backend/version');

export class Page extends React.PureComponent {
  constructor (props) {
    super(props);

    this.tableRef = React.createRef();
    this.exportObj = {
      headerLayout: null,
      tableHeaderLayout: null,
      tableLayout: [],
      footerLayout: null
    };

    this.tableLayoutSize = 0;
  }

  trySendExportData () {
    if (!this.props.hasHeader) {
      this.exportObj.headerLayout = {};
    }

    if (!this.props.hasFooter) {
      this.exportObj.footerLayout = {};
    }

    let ok = true;
    Object.keys(this.exportObj).forEach(key => {
      if (this.exportObj[key] == null) {
        ok = false;
      }
    });

    if (this.tableLayoutSize - 1 < this.props.lastRow - this.props.firstRow) {
      ok = false;
    }

    if (ok && this.props.onExportDataReady) {
      this.props.onExportDataReady(this.exportObj);
    }
  }

  componentDidMount () {
    if (this.props.exportMode) {
      //
    } else {
      window.requestAnimationFrame(() => {
        const pageRange = [];
        let currentStart = 0;
        let currentPage = -1;
        let lastX = -1;
        let lastNumber = 0;

        for (const childElem of this.tableRef.current.children) {
          const num = childElem.getAttribute('data-period-no');
          if (num === undefined || num == null) {
            continue;
          }

          lastNumber = num;

          if (childElem.offsetLeft !== lastX) {
            currentPage++;
            if (currentPage > 0) {
              pageRange.push({
                pageNumber: currentPage - 1,
                firstRow: currentStart,
                lastRow: num - 1
              });
            }
            currentStart = num;
            lastX = childElem.offsetLeft;
          }
        }

        pageRange.push({
          pageNumber: currentPage,
          firstRow: currentStart,
          lastRow: lastNumber
        });

        if (this.props.onPageCountReady) {
          this.props.onPageCountReady(pageRange, this.props.probeId);
        }
      });
    }
  }

  handleHeaderDataReady (data) {
    this.exportObj.headerLayout = data;
    this.trySendExportData();
  }

  getHeader () {
    const headerInfo = this.props.fragmentOutput.headerInfo;
    // let rd = this.props.fragmentData.renderData;
    let typeText = '';

    if (headerInfo.type === 'class') {
      typeText = this.props.i18n.__('Class');
    } else if (headerInfo.type === 'teacher') {
      typeText = this.props.i18n.__('Teacher');
    } else if (headerInfo.type === 'classroom') {
      typeText = this.props.i18n.__('Classroom');
    }

    const options = this.props.fragmentData.softConfig.appearanceOptions;
    const brandType = options.brandingType;

    if (brandType === 0) { // Logo
      return (
        <LogoHeader
          exportMode={this.props.exportMode}
          onExportDataReady={this.handleHeaderDataReady.bind(this)}
          logoBase64={options.logoBase64}
          categoryText={typeText}
          categoryClass={headerInfo.type}
          sourceText={headerInfo.name}
          exportDateText={this.props.i18n.__mf('Exported on: %s',
            headerInfo.exportDate)}
          renderDateText={this.props.i18n.__mf('Rendered on: %s',
            headerInfo.renderDate)}
          showDates={options.validityVisible}
          tutorLabel={this.props.i18n.__('Tutor')}
          tutorName={headerInfo.tutorName}
          mono={this.props.mono}
          themeColors={this.props.fragmentData.softConfig.appearanceOptions.themeColors}
        />
      );
    } else if (brandType === 1) { // Text
      return (
        <TextHeader
          exportMode={this.props.exportMode}
          onExportDataReady={this.handleHeaderDataReady.bind(this)}
          brandingText={options.brandingText.replace(/\\n/g, '\n')}
          categoryText={typeText}
          categoryClass={headerInfo.type}
          sourceText={headerInfo.name}
          exportDateText={this.props.i18n.__mf('Exported on: %s',
            headerInfo.exportDate)}
          renderDateText={this.props.i18n.__mf('Rendered on: %s',
            headerInfo.renderDate)}
          showDates={options.validityVisible}
          tutorLabel={this.props.i18n.__('Tutor')}
          tutorName={headerInfo.tutorName}
          mono={this.props.mono}
          themeColors={this.props.fragmentData.softConfig.appearanceOptions.themeColors}
        />
      );
    } else if (brandType === 2) {
      return (
        <BasicHeader
          exportMode={this.props.exportMode}
          onExportDataReady={this.handleHeaderDataReady.bind(this)}
          categoryText={typeText}
          categoryClass={headerInfo.type}
          sourceText={headerInfo.name}
          exportDateText={this.props.i18n.__mf('Exported on: %s',
            headerInfo.exportDate)}
          renderDateText={this.props.i18n.__mf('Rendered on: %s',
            headerInfo.renderDate)}
          showDates={options.validityVisible}
          tutorLabel={this.props.i18n.__('Tutor')}
          tutorName={headerInfo.tutorName}
          mono={this.props.mono}
          themeColors={this.props.fragmentData.softConfig.appearanceOptions.themeColors}
        />
      );
    }
  }

  handleTableDataReady (id, data) {
    this.exportObj.tableLayout[id] = data;
    this.tableLayoutSize++;
    this.trySendExportData();
  }

  getRows () {
    const headerInfo = this.props.fragmentOutput.headerInfo;

    return this.props.fragmentOutput.data.map(periodRow => {
      if (this.props.firstRow !== undefined && this.props.lastRow !== undefined) {
        if (periodRow.number < this.props.firstRow || periodRow.number > this.props.lastRow) {
          return null;
        }
      }

      const info = periodRow.number - this.props.firstRow;
      return (
        <TableRow
          exportMode={this.props.exportMode}
          onExportDataReady={this.handleTableDataReady.bind(this, info)}
          key={periodRow.number}
          rowObject={periodRow}
          isLastPage={this.props.isLastPage}
          rowData={this.props.fragmentOutput.cards[periodRow.number]}
          showSaturday={this.props.fragmentData.softConfig.appearanceOptions.saturdayVisible}
          wideRows={headerInfo.type !== 'class'}
          probeId={this.props.probeId}
          mono={this.props.mono}
          themeColors={this.props.fragmentData.softConfig.appearanceOptions.themeColors}
        />
      );
    });
  }

  handleFooterDataReady (data) {
    this.exportObj.footerLayout = data;
    this.trySendExportData();
  }

  getFooter () {
    return (
      <Footer
        exportMode={this.props.exportMode}
        onExportDataReady={this.handleFooterDataReady.bind(this)}
        leftText={this.props.i18n.__mf('Generated by aSc Timetables %s',
          this.props.fragmentData.renderData.ascttVersion)}
        centerText={this.props.pageString}
        rightText={this.props.i18n.__mf('Rendered by TTDESC %s', version)}
        mono={this.props.mono}
        themeColors={this.props.fragmentData.softConfig.appearanceOptions.themeColors}
      />
    );
  }

  handleTableHeadDataReady (data) {
    this.exportObj.tableHeaderLayout = data;
    this.trySendExportData();
  }

  render () {
    return (
      <div className={
        'page-frame' +
        ((this.props.mono) ? ' mono' : '')
      }
      >
        <div className='vertical-flex'>
          {this.props.hasHeader && this.getHeader()}
          <div className='tt-data expand-vert' ref={this.tableRef}>
            <TableHead
              exportMode={this.props.exportMode}
              onExportDataReady={this.handleTableHeadDataReady.bind(this)}
              i18n={this.props.i18n}
              showSaturday={this.props.fragmentData.softConfig.appearanceOptions.saturdayVisible}
              mono={this.props.mono}
              themeColors={this.props.fragmentData.softConfig.appearanceOptions.themeColors}
            />
            {this.getRows()}
          </div>
          {this.props.hasFooter && this.getFooter()}
        </div>
      </div>
    );
  }
}

Page.propTypes = {
  i18n: PropTypes.object,
  hasHeader: PropTypes.bool,
  hasFooter: PropTypes.bool,
  fragmentData: PropTypes.object.isRequired,
  fragmentOutput: PropTypes.object.isRequired,
  onPageCountReady: PropTypes.func,
  firstRow: PropTypes.number,
  lastRow: PropTypes.number,
  isLastPage: PropTypes.bool,
  probeOnly: PropTypes.bool,
  probeId: PropTypes.number,
  mono: PropTypes.bool,
  pageString: PropTypes.string,
  exportMode: PropTypes.bool,
  onExportDataReady: PropTypes.func
};
