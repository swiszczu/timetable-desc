/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * radio-controller.jsx - radio button group manager
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

export class RadioController extends React.PureComponent {
  handleChange (checked, element) {
    if (checked) {
      if (this.props.onChange) {
        this.props.onChange(element);
      }
    }
  }

  getChildren () {
    return this.props.children.map((element, i) => {
      return React.cloneElement(element, {
        key: i,
        name: this.props.name,
        ordinal: i,
        checked: i === this.props.checkedId,
        onChange: this.handleChange.bind(this)
      });
    });
  }

  render () {
    return (
      <>
        {this.getChildren()}
      </>
    );
  }
}

RadioController.propTypes = {
  children: PropTypes.array,
  checkedId: PropTypes.number.isRequired,
  name: PropTypes.string,
  onChange: PropTypes.func
};
