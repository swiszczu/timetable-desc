/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * helper-tooltip.jsx - helper icon that shows an image while hovering
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

export class HelperTooltip extends React.PureComponent {
  constructor (props) {
    super(props);

    this.state = {
      active: false
    };
  }

  handleMouseOut () {
    this.setState({ active: false });
  }

  handleClick () {
    this.setState(state => {
      return {
        active: !state.active
      };
    });
  }

  render () {
    return (
      <div
        className={'helper-tooltip' + ((this.state.active) ? ' active' : '')}
        title={this.props.tooltip}
        style={{
          backgroundSize: `${this.props.size}px ${this.props.size}px`,
          width: `${this.props.size}px`,
          height: `${this.props.size}px`
        }}
        onClick={this.handleClick.bind(this)}
        onMouseOut={this.handleMouseOut.bind(this)}
      >
        <img
          src={this.props.imageUrl}
          style={{
            width: this.props.imageWidth
          }}
        />
      </div>
    );
  }
}

HelperTooltip.propTypes = {
  tooltip: PropTypes.string,
  imageUrl: PropTypes.string,
  size: PropTypes.number,
  imageWidth: PropTypes.number
};
