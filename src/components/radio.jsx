/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * radio.jsx - simple radio button
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

export class Radio extends React.Component {
  handleChange (event) {
    if (this.props.onChange) {
      this.props.onChange(event.target.checked, this.props.ordinal);
    }
  }

  render () {
    return (
      <label className='select-control'>
        <div className='select-text'>{this.props.text}</div>
        <input
          type='radio'
          checked={this.props.checked}
          name={this.props.name}
          onChange={this.handleChange.bind(this)}
        />
        <div className='radiomark' />
      </label>
    );
  }
}

Radio.propTypes = {
  checked: PropTypes.bool,
  text: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  ordinal: PropTypes.number
};
