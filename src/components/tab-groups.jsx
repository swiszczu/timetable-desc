/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * tab-groups.jsx - Groups tab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { GroupsSubjects } from './groups-subjects.jsx';
import { GradeSelector } from './grade-selector.jsx';

import memo from 'memoize-one';

const { ipcRenderer } = window.require('electron');

export class GroupsTab extends React.PureComponent {
  constructor (props) {
    super(props);

    this.defaultRules = props.parser.getDefaultRulesState();

    this.state = {
      rules: JSON.parse(JSON.stringify(this.defaultRules)),
      gradeId: 0
    };

    this.gradeKeyArr = Object.keys(this.props.parser.timetableInfo.classArrForGrade);
    this.subjectsArr = memo(this.getSubjectsArray);
    this.gradesArr = memo(this.getGradesArray);
    this.handleIPCBound = this.handleAddGroupExt.bind(this);
  }

  cloneStateRules (state) {
    const rulesCopy = {};

    // Copy one level deep
    Object.keys(state.rules).forEach(key => {
      rulesCopy[key] = {
        subjects: Object.assign({}, state.rules[key].subjects)
      };
    });

    return rulesCopy;
  }

  getGradesArray (timetableInfo, i18n) {
    return Object.keys(timetableInfo.classArrForGrade).map(element => {
      return i18n.__('Grade ') + element;
    });
  }

  getSubjectsArray (timetableInfo) {
    return Object.keys(timetableInfo.subjectAssoc).map((element) => {
      const retObj = {
        id: timetableInfo.subjectAssoc[element].id,
        name: timetableInfo.subjectAssoc[element].name + ' (' +
              timetableInfo.subjectAssoc[element].short + ')'
      };

      return retObj;
    });
  }

  handleChangeGrade (newId) {
    this.setState({
      gradeId: newId
    });
  }

  handleAddGroupExt (event, data) {
    this.setState(state => {
      const rulesCopy = this.cloneStateRules(state);

      if (rulesCopy[this.gradeKeyArr[state.gradeId]].subjects[data.subjectId]) {
        if (data.id) {
          // If an id is given, the user was editing the record
          const arr = (rulesCopy[this.gradeKeyArr[state.gradeId]]
            .subjects[data.subjectId].rules);

          for (let i = 0; i < arr.length; i++) {
            if (arr[i].id === data.id) {
              arr[i].newName = data.name;
              arr[i].groupNames = data.groupNames;
            }
          }
        } else {
          // If there is no id, it is a new record
          rulesCopy[this.gradeKeyArr[state.gradeId]]
            .subjects[data.subjectId].rules =

            rulesCopy[this.gradeKeyArr[state.gradeId]]
              .subjects[data.subjectId].rules.concat({
                id: new Date().getTime(),
                newName: data.name,
                groupNames: data.groupNames
              });
        }
      }

      return {
        rules: rulesCopy
      };
    });
  }

  componentDidMount () {
    ipcRenderer.on('rule:add-to-subject', this.handleIPCBound);
  }

  componentWillUnmount () {
    ipcRenderer.removeListener('rule:add-to-subject', this.handleIPCBound);
  }

  handleDeleteSubject (subjectKey, i) {
    this.setState(state => {
      const rulesCopy = this.cloneStateRules(state);

      if (rulesCopy[this.gradeKeyArr[state.gradeId]].subjects[subjectKey]) {
        const tempArr = [...rulesCopy[this.gradeKeyArr[state.gradeId]]
          .subjects[subjectKey].rules];

        tempArr.splice(i, 1);

        rulesCopy[this.gradeKeyArr[state.gradeId]]
          .subjects[subjectKey].rules = tempArr;
      }

      return {
        rules: rulesCopy
      };
    });
  }

  render () {
    return (
      <div className={this.props.className}>

        <GradeSelector
          label={this.props.i18n.__('Grade') + ':'}
          defaultGrade={this.state.gradeId}
          onChange={this.handleChangeGrade.bind(this)}
          grades={this.gradesArr(this.props.parser.timetableInfo,
            this.props.i18n)}
        />

        <div className='property' style={{ marginTop: '32px' }}>
          <div className='title'>{this.props.i18n.__('This is a list of ' +
            'renaming rules applied to groups on different subjects for ' +
            'selected grade') + ':'}
          </div>
          <div className='section-spacer' />
        </div>

        <GroupsSubjects
          rulesState={this.state.rules[this.gradeKeyArr[this.state.gradeId]]}
          gradeId={this.gradeKeyArr[this.state.gradeId]}
          i18n={this.props.i18n}
          onDelete={this.handleDeleteSubject.bind(this)}
        />
      </div>
    );
  }

  // IO
  serializeState () {
    const allRules = [];

    Object.keys(this.state.rules).forEach(key1 => {
      const grade = this.state.rules[key1];

      Object.keys(grade.subjects).forEach(key2 => {
        const subject = grade.subjects[key2];

        subject.rules.forEach(rule => {
          const ruleObj = {
            gradeName: key1,
            subjectName: subject.name,
            rule: rule
          };

          allRules.push(ruleObj);
        });
      });
    });

    return {
      rules: allRules
    };
  }

  deserializeState (obj) {
    if (!obj) return;

    const subjArr = this.subjectsArr(this.props.parser.timetableInfo);
    const subjObj = {};
    subjArr.forEach(element => {
      subjObj[element.name] = element.id;
    });

    const rules = JSON.parse(JSON.stringify(this.defaultRules));

    obj.rules.forEach(rule => {
      if (rules[rule.gradeName]) {
        if (rules[rule.gradeName].subjects[subjObj[rule.subjectName]]) {
          rules[rule.gradeName].subjects[subjObj[rule.subjectName]].rules.push(rule.rule);
        }
      }
    });

    this.setState({
      rules: rules
    });
  }
}

GroupsTab.propTypes = {
  className: PropTypes.string,
  parser: PropTypes.object,
  i18n: PropTypes.object,
  onChange: PropTypes.func
};
