/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * radio-disabler.jsx - disables all children if radio button is unchecked
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Radio } from './radio.jsx';

export class RadioDisabler extends React.PureComponent {
  handleChange (checked, ordinal) {
    if (this.props.onChange) {
      this.props.onChange(checked, ordinal);
    }
  }

  getChildren () {
    if (!this.props.children) return null;
    return this.props.children.map((element, i) => {
      if (element) {
        return React.cloneElement(element, {
          key: i,
          disabled: !this.props.checked
        });
      }
      return null;
    });
  }

  render () {
    return (
      <>
        <Radio
          checked={this.props.checked}
          text={this.props.text}
          name={this.props.name}
          ordinal={this.props.ordinal}
          onChange={this.handleChange.bind(this)}
        />

        <div className='hierarchy-container'>
          {this.getChildren()}
        </div>
        <br />
      </>
    );
  }
}

RadioDisabler.propTypes = {
  checked: PropTypes.bool,
  text: PropTypes.string,
  name: PropTypes.string,
  children: PropTypes.array,
  ordinal: PropTypes.number,
  onChange: PropTypes.func
};
