/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * margin-selector.jsx - four number pickers to adjust page margin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Label } from './label.jsx';

export class MarginSelector extends React.PureComponent {
  sendChange (margin, newValue) {
    if (this.props.onChange) {
      const changeObj = {
        left: this.props.margins.left,
        right: this.props.margins.right,
        top: this.props.margins.top,
        bottom: this.props.margins.bottom
      };

      changeObj[margin] = newValue;
      this.props.onChange(changeObj);
    }
  }

  handleChangeLeft (event) {
    const newValue = parseFloat(event.target.value);
    this.sendChange('left', newValue);
  }

  handleChangeRight (event) {
    const newValue = parseFloat(event.target.value);
    this.sendChange('right', newValue);
  }

  handleChangeTop (event) {
    const newValue = parseFloat(event.target.value);
    this.sendChange('top', newValue);
  }

  handleChangeBottom (event) {
    const newValue = parseFloat(event.target.value);
    this.sendChange('bottom', newValue);
  }

  render () {
    return (
      <div className='flex-distribute'>
        <div>
          <Label text={this.props.i18n.__('Left') + ' (cm):'} />
          <input
            style={{ width: '64px' }}
            type='number'
            min='0'
            max='5'
            step='0.01'
            value={this.props.margins.left}
            onChange={this.handleChangeLeft.bind(this)}
            className='text-field'
          />
        </div>

        <div>
          <Label text={this.props.i18n.__('Right') + ' (cm):'} />
          <input
            style={{ width: '64px' }}
            type='number'
            min='0'
            max='5'
            step='0.01'
            value={this.props.margins.right}
            onChange={this.handleChangeRight.bind(this)}
            className='text-field'
          />
        </div>

        <div>
          <Label text={this.props.i18n.__('Top') + ' (cm):'} />
          <input
            style={{ width: '64px' }}
            type='number'
            min='0'
            max='5'
            step='0.01'
            value={this.props.margins.top}
            onChange={this.handleChangeTop.bind(this)}
            className='text-field'
          />
        </div>

        <div>
          <Label text={this.props.i18n.__('Bottom') + ' (cm):'} />
          <input
            style={{ width: '64px' }}
            type='number'
            min='0'
            max='5'
            step='0.01'
            value={this.props.margins.bottom}
            onChange={this.handleChangeBottom.bind(this)}
            className='text-field'
          />
        </div>
      </div>
    );
  }
}

MarginSelector.propTypes = {
  margins: PropTypes.object,
  i18n: PropTypes.object,
  onChange: PropTypes.func
};
