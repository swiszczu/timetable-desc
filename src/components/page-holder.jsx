/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * page-holder.jsx - preview page manager
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Page } from './page.jsx';

import memo from 'memoize-one';

export class PageHolder extends React.PureComponent {
  constructor (props) {
    super(props);

    this.pageInfo = new Array(props.fragmentOutput.length);
    this.pagesReady = 0;

    this.normalPage = memo(this.getNormalPage);

    // console.log(props.pageInfo);
  }

  handlePageCountReady (pageCountInfo, pageNumber) {
    if (this.pageInfo[pageNumber] === undefined) {
      this.pageInfo[pageNumber] = pageCountInfo;
      this.pagesReady++;
    }

    if (this.pagesReady === this.pageInfo.length) {
      if (this.props.onPageCountReady) {
        let total = 0;
        const offsetArr = [];
        this.pageInfo.forEach(docPageInfo => {
          offsetArr.push(total);
          total += docPageInfo.length;
        });

        this.props.onPageCountReady({
          totalPages: total,
          offsets: offsetArr,
          info: this.pageInfo
        });
      }
    }
  }

  getProbePages () {
    return this.props.fragmentOutput.map((fragOut, i) => {
      return (
        <Page
          key={i}
          i18n={this.props.i18n}
          fragmentData={this.props.fragmentData}
          fragmentOutput={this.props.fragmentOutput[i]}
          hasHeader
          hasFooter
          onPageCountReady={(!this.props.pageInfo) ? (this.handlePageCountReady.bind(this)) : undefined}
          isLastPage={false}
          probeOnly
          probeId={i}
        />
      );
    });
  }

  handleExportDataReady (headerData, lastPage, data) {
    if (this.props.onExportDataReady) {
      data.headerInfo = headerData;
      data.lastPage = lastPage;

      this.props.onExportDataReady(data);
    }
  }

  getNormalPage (pageNumber, exportMode) {
    pageNumber--;
    let documentId;
    this.props.pageInfo.offsets.forEach((offset, i) => {
      if (offset <= pageNumber) {
        documentId = i;
      }
    });

    const documentPage = pageNumber - this.props.pageInfo.offsets[documentId];
    return (
      <Page
        key={exportMode ? ('exporting' + this.props.pageNumber) : 'normal'}
        i18n={this.props.i18n}
        exportMode={exportMode}
        onExportDataReady={this.handleExportDataReady.bind(this,
          this.props.fragmentOutput[documentId].headerInfo,
          documentPage + 1 === this.props.pageInfo.info[documentId].length)}
        fragmentData={this.props.fragmentData}
        fragmentOutput={this.props.fragmentOutput[documentId]}
        hasHeader={documentPage === 0}
        hasFooter
        firstRow={parseInt(this.props.pageInfo.info[documentId][documentPage].firstRow)}
        lastRow={parseInt(this.props.pageInfo.info[documentId][documentPage].lastRow)}
        isLastPage={documentPage + 1 === this.props.pageInfo.info[documentId].length &&
          documentPage > 0}
        probeId={pageNumber}
        mono={this.props.fragmentData.softConfig.appearanceOptions.mono}
        pageString={(documentPage + 1) + ' / ' + this.props.pageInfo.info[documentId].length}
      />
    );
  }

  render () {
    if (this.props.pageNumber === 1 && !this.props.pageInfo &&
      !this.props.exportMode) {
      return this.getProbePages();
    } else {
      return this.normalPage(this.props.pageNumber, this.props.exportMode);
    }
  }
}

PageHolder.propTypes = {
  i18n: PropTypes.object,
  exportMode: PropTypes.bool,
  fragmentData: PropTypes.object.isRequired,
  fragmentOutput: PropTypes.array.isRequired,
  onPageCountReady: PropTypes.func,
  onExportDataReady: PropTypes.func,
  pageNumber: PropTypes.number.isRequired,
  pageInfo: PropTypes.array
};
