/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * tab-start.jsx - Start tab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Checkbox } from './checkbox.jsx';
import { ActionButton } from './action-button.jsx';

export class StartTab extends React.PureComponent {
  constructor (props) {
    super(props);

    this.state = {
      autoSaveEnabled: true
    };
  }

  handleAutoSaveChange (enabled) {
    this.setState({
      autoSaveEnabled: enabled
    });
  }

  getStatusText () {
    if (!this.props.configState) {
      return this.props.i18n.__('Configuration for this file not found. Loaded default values.');
    } else if (this.props.configState === 1) {
      return this.props.i18n.__('Auto-save config file loaded. Everyting is OK.');
    } else if (this.props.configState === 2) {
      return this.props.i18n.__('Config has been manually imported from file.');
    } else if (this.props.configState === 3) {
      return this.props.i18n.__('Config has been manually exported to file.');
    }
    return '?';
  }

  handleClickImport () {
    if (this.props.onImport) {
      this.props.onImport();
    }
  }

  handleClickExport () {
    if (this.props.onExport) {
      this.props.onExport();
    }
  }

  render () {
    return (
      <div className={this.props.className} style={{ minHeight: '100%' }}>
        <div className='vertical-flex'>
          <div>
            <b>{this.props.i18n.__('Status')}: </b>
            <span>{this.getStatusText()}</span>
          </div>
          <div className='expand-vert horizontal-flex'>
            <ActionButton
              className='expand-horz'
              imagePath='img/config-import.svg'
              text={this.props.i18n.__('Import configuration...')}
              onClick={this.handleClickImport.bind(this)}
            />
            <ActionButton
              className='expand-horz'
              imagePath='img/config-export.svg'
              text={this.props.i18n.__('Export configuration...')}
              onClick={this.handleClickExport.bind(this)}
            />
          </div>
          <div>
            <Checkbox
              checked={this.state.autoSaveEnabled}
              text={this.props.i18n.__('Enable configuration auto-save on export')}
              onChange={this.handleAutoSaveChange.bind(this)}
            />
          </div>
        </div>
      </div>
    );
  }

  // IO
  serializeState () {
    return {
      autoSaveEnabled: this.state.autoSaveEnabled
    };
  }

  deserializeState (obj) {
    if (!obj) return;

    this.setState({
      autoSaveEnabled: obj.autoSaveEnabled
    });
  }
}

StartTab.propTypes = {
  className: PropTypes.string,
  parser: PropTypes.object,
  i18n: PropTypes.object,
  configState: PropTypes.number,
  onImport: PropTypes.func,
  onExport: PropTypes.func
};
