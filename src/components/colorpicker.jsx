/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * colorpicker.jsx - small push button that opens color picker dialog
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Button } from './button.jsx';
import { PhotoshopPicker } from '../../lib/react-color';
import Modal from 'react-modal';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    background: 'transparent',
    border: 'none'
  },
  overlay: {
    background: 'rgba(0,0,0,0.5)'
  }
};

Modal.setAppElement('#app');

export class TimetableColorPicker extends React.PureComponent {
  constructor (props) {
    super(props);

    this.state = {
      showBox: false,
      activeBox: false
    };

    this.lastColor = null;
  }

  handleClick () {
    this.lastColor = this.props.color;

    this.setState({
      showBox: true,
      activeBox: !this.state.activeBox
    });
  }

  handleChange (color) {
    if (this.props.onChange) {
      this.props.onChange(color.hex);
    }
  }

  closeModal () {
    this.setState({
      showBox: false
    });

    if (this.props.onChange) {
      this.props.onChange(this.lastColor);
    }
  }

  handlePickerOK () {
    this.setState({
      showBox: false
    });
  }

  handlePickerCancel () {
    this.setState({
      showBox: false
    });

    if (this.props.onChange) {
      this.props.onChange(this.lastColor);
    }
  }

  render () {
    return (
      <div style={{
        position: 'relative',
        marginRight: '8px',
        marginBottom: '8px',
        display: 'inline-block'
      }}
      >
        <Button
          text={this.props.text}
          onClick={this.handleClick.bind(this)}
          small
        >

          <div
            style={{ backgroundColor: this.props.color }}
            className='color-block'
          >&nbsp;
          </div>
        </Button>

        <Modal
          isOpen={this.state.showBox}
          onRequestClose={this.closeModal.bind(this)}
          style={customStyles}
        >
          <PhotoshopPicker
            header={this.props.text}
            className='timetable-color-picker'
            color={this.props.color}
            onAccept={this.handlePickerOK.bind(this)}
            onCancel={this.handlePickerCancel.bind(this)}
            onChange={this.handleChange.bind(this)}
          />
        </Modal>
      </div>
    );
  }
}

TimetableColorPicker.propTypes = {
  i18n: PropTypes.object,
  text: PropTypes.string,
  color: PropTypes.string,
  onChange: PropTypes.func
};
