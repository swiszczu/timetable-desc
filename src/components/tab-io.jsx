/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * tab-io.jsx - Input/Output tab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Label } from './label.jsx';
import { Button } from './button.jsx';
import { Checkbox } from './checkbox.jsx';
import { GradeSelector } from './grade-selector.jsx';

import memo from 'memoize-one';

export class IOTab extends React.PureComponent {
  constructor (props) {
    super(props);

    this.state = {
      pdfTitle: this.props.i18n.__('Timetable export'),
      pdfAuthor: '',
      pdfSubject: this.props.i18n.__('Timetable'),
      exportType: 0,
      marks: {
        classMarks: Object.keys(props.parser.timetableInfo
          .classAssoc).map(() => false),
        teacherMarks: Object.keys(props.parser.timetableInfo
          .teacherAssoc).map(() => false),
        classroomMarks: Object.keys(props.parser.timetableInfo
          .classroomAssoc).map(() => false)
      }
    };

    this.classExports = memo(this.getClassExports);
    this.teacherExports = memo(this.getTeacherExports);
    this.classroomExports = memo(this.getClassroomExports);

    this.exportTypes = [
      this.props.i18n.__('Class\' timetable'),
      this.props.i18n.__('Teacher\'s timetable'),
      this.props.i18n.__('Classroom\'s timetable')
    ];

    this.allExports = memo(this.getExports);
  }

  handleChangeText (stateName, event) {
    const newState = {};
    newState[stateName] = event.target.value;

    this.setState(newState);
  }

  getClassExports (timetableInfo) {
    return Object.keys(timetableInfo.classAssoc).map(key => {
      const classObj = timetableInfo.classAssoc[key];

      return (
        <div
          key={key}
          style={{
            position: 'relative',
            display: 'inline-block',
            marginRight: '8px',
            marginBottom: '8px'
          }}
        >
          <Button
            text={classObj.name}
            small
            onClick={this.handleClickClass.bind(this, key)}
          />
        </div>
      );
    });
  }

  getTeacherExports (timetableInfo) {
    return Object.keys(timetableInfo.teacherAssoc).map(key => {
      const teacherObj = timetableInfo.teacherAssoc[key];

      return (
        <div
          key={key}
          style={{
            position: 'relative',
            display: 'inline-block',
            marginRight: '8px',
            marginBottom: '8px'
          }}
        >
          <Button
            text={teacherObj.short}
            title={teacherObj.name}
            small
            onClick={this.handleClickTeacher.bind(this, key)}
          />
        </div>
      );
    });
  }

  getClassroomExports (timetableInfo) {
    return Object.keys(timetableInfo.classroomAssoc).map(key => {
      const classroomObj = timetableInfo.classroomAssoc[key];

      return (
        <div
          key={key}
          style={{
            position: 'relative',
            display: 'inline-block',
            marginRight: '8px',
            marginBottom: '8px'
          }}
        >
          <Button
            key={key}
            text={classroomObj.name}
            small
            onClick={this.handleClickClassroom.bind(this, key)}
          />
        </div>
      );
    });
  }

  getExports (timetableInfo, marks, exportType) {
    switch (exportType) {
      case 0: return this.getClassExports2(timetableInfo);
      case 1: return this.getTeacherExports2(timetableInfo);
      case 2: return this.getClassroomExports2(timetableInfo);
    }
  }

  onElementChecked (arrName, index, newVal) {
    this.setState(state => {
      const clone = { ...state.marks };

      clone[arrName][index] = newVal;

      return {
        marks: clone
      };
    });
  }

  getClassExports2 (timetableInfo) {
    return [
      this.getSelectDeselectBar('classMarks'),
      Object.keys(timetableInfo.classAssoc).map((key, i) => {
        const classObj = timetableInfo.classAssoc[key];

        return (
          <Checkbox
            key={key}
            text={classObj.name}
            checked={this.state.marks.classMarks[i]}
            onChange={this.onElementChecked.bind(this, 'classMarks', i)}
          />
        );
      })
    ];
  }

  getTeacherExports2 (timetableInfo) {
    return [
      this.getSelectDeselectBar('teacherMarks'),
      Object.keys(timetableInfo.teacherAssoc).map((key, i) => {
        const teacherObj = timetableInfo.teacherAssoc[key];

        return (
          <Checkbox
            key={key}
            text={teacherObj.name}
            checked={this.state.marks.teacherMarks[i]}
            onChange={this.onElementChecked.bind(this, 'teacherMarks', i)}
          />
        );
      })
    ];
  }

  getClassroomExports2 (timetableInfo) {
    return [
      this.getSelectDeselectBar('classroomMarks'),
      Object.keys(timetableInfo.classroomAssoc).map((key, i) => {
        const classroomObj = timetableInfo.classroomAssoc[key];

        return (
          <Checkbox
            key={key}
            text={classroomObj.name}
            checked={this.state.marks.classroomMarks[i]}
            onChange={this.onElementChecked.bind(this, 'classroomMarks', i)}
          />
        );
      })
    ];
  }

  handleSelectDeselectAction (arrName, action) {
    this.setState(state => {
      const clone = { ...state.marks };

      for (let i = 0; i < clone[arrName].length; i++) {
        if (action === 'select') {
          clone[arrName][i] = true;
        } else if (action === 'deselect') {
          clone[arrName][i] = false;
        } else if (action === 'invert') {
          clone[arrName][i] = !clone[arrName][i];
        }
      }

      return {
        marks: clone
      };
    });
  }

  getSelectDeselectBar (arrName) {
    return (
      <div key='selectdeselect' style={{ marginBottom: '8px' }}>
        <div style={{ display: 'inline-block', marginRight: '4px' }}>
          <Button
            text={this.props.i18n.__('Select all')}
            onClick={this.handleSelectDeselectAction.bind(
              this, arrName, 'select')}
            small
          />
        </div>
        <div style={{ display: 'inline-block', marginRight: '4px' }}>
          <Button
            text={this.props.i18n.__('Deselect all')}
            onClick={this.handleSelectDeselectAction.bind(
              this, arrName, 'deselect')}
            small
          />
        </div>
        <div style={{ display: 'inline-block', marginRight: '4px' }}>
          <Button
            text={this.props.i18n.__('Invert selection')}
            onClick={this.handleSelectDeselectAction.bind(
              this, arrName, 'invert')}
            small
          />
        </div>
      </div>
    );
  }

  onTypeChanged (newValue) {
    this.setState({ exportType: newValue });
  }

  render () {
    return (
      <div className={this.props.className}>
        <div className='property'>
          <div className='title'>{this.props.i18n.__('PDF Metadata') + ':'}</div>
          <div className='section-spacer' />
        </div>
        <div className='hierarchy-container spacing'>
          <Label text={this.props.i18n.__('Document title') + ':'} />
          <input
            type='text'
            className='text-field full-width spacing'
            onChange={this.handleChangeText.bind(this, 'pdfTitle')}
            value={this.state.pdfTitle}
          />

          <Label text={this.props.i18n.__('Document author') + ':'} />
          <input
            type='text'
            className='text-field full-width spacing'
            onChange={this.handleChangeText.bind(this, 'pdfAuthor')}
            value={this.state.pdfAuthor}
          />

          <Label text={this.props.i18n.__('Subject') + ':'} />
          <input
            type='text'
            className='text-field full-width'
            onChange={this.handleChangeText.bind(this, 'pdfSubject')}
            value={this.state.pdfSubject}
          />
        </div>
        <br />
        <div className='property'>
          <div className='title'>{this.props.i18n.__('Possible exports') + ':'}</div>
          <div className='section-spacer' />
        </div>
        <div className='hierarchy-container spacing'>
          <GradeSelector
            label={this.props.i18n.__('Export type') + ':'}
            defaultGrade={this.state.exportType}
            grades={this.exportTypes}
            onChange={this.onTypeChanged.bind(this)}
          />
          <div className='hierarchy-container spacing'>
            {this.allExports(this.props.parser.timetableInfo,
              this.state.marks, this.state.exportType)}
          </div>
        </div>
      </div>
    );
  }

  getExportConfiguration () {
    const exportConfig = {
      type: ['class', 'teacher', 'classroom'][this.state.exportType],
      condensed: false,
      id: [],
      ascttVersion: this.props.parser.timetableInfo.ascVersion
    };

    let keysArr;
    switch (this.state.exportType) {
      case 0:
        keysArr = Object.keys(this.props.parser.timetableInfo.classAssoc);
        this.state.marks.classMarks.forEach((val, id) => {
          if (val) {
            exportConfig.id.push(keysArr[id]);
          }
        });
        break;
      case 1:
        keysArr = Object.keys(this.props.parser.timetableInfo.teacherAssoc);
        this.state.marks.teacherMarks.forEach((val, id) => {
          if (val) {
            exportConfig.id.push(keysArr[id]);
          }
        });
        break;
      case 2:
        keysArr = Object.keys(this.props.parser.timetableInfo.classroomAssoc);
        this.state.marks.classroomMarks.forEach((val, id) => {
          if (val) {
            exportConfig.id.push(keysArr[id]);
          }
        });
        break;
    }

    return exportConfig;
  }

  // IO
  serializeState () {
    return {
      pdfTitle: this.state.pdfTitle,
      pdfAuthor: this.state.pdfAuthor,
      pdfSubject: this.state.pdfSubject
    };
  }

  deserializeState (obj) {
    if (!obj) return;

    this.setState({
      pdfTitle: obj.pdfTitle,
      pdfAuthor: obj.pdfAuthor,
      pdfSubject: obj.pdfSubject
    });
  }
}

IOTab.propTypes = {
  className: PropTypes.string,
  parser: PropTypes.object,
  i18n: PropTypes.object,
  onChange: PropTypes.func,
  onShowPreview: PropTypes.func
};
