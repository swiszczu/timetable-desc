/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * placeholder.jsx - big information with image about software state
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

export class Placeholder extends React.Component {
  getContent () {
    switch (this.props.type) {
      case PlaceholderConsts.NO_FILE_LOADED:
        return (
          <>
            <img src='img/file-open.svg' />
            <p>{this.props.i18n.__('Select a file before continuing')}</p>
          </>
        );
      case PlaceholderConsts.FILE_LOADING_ERROR:
        return (
          <>
            <img src='img/file-error.svg' />
            <p>{this.props.i18n.__("Can't load this file! Try again.")}</p>
          </>
        );
    }
    return null;
  }

  render () {
    return (
      <div className='placeholder'>
        {this.getContent()}
      </div>
    );
  }
}

export const PlaceholderConsts = {
  NO_FILE_LOADED: 1,
  FILE_LOADING_ERROR: 2
};

Placeholder.propTypes = {
  type: PropTypes.number,
  i18n: PropTypes.object
};
