/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * Component library
 * tabbox.jsx - tab container and manager
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

export class Tabbox extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      tabSelected: 0
    };
  }

  getTabs () {
    return (
      <>
        {
          this.props.tabs.map((tabText, i) => {
            return (
              <div
                key={i}
                tabIndex='0'
                className={'tab' + ((i === this.state.tabSelected) ? ' tab-active' : '')}
                onClick={() => { this.setState({ tabSelected: i }); }}
                onKeyPress={(event) => {
                  if (event.key === 'Enter') { this.setState({ tabSelected: i }); }
                }}
              >

                {tabText}
                <div className='tab-dash' />
              </div>);
          })
        }
      </>
    );
  }

  render () {
    return (
      <div className='tabbox'>
        <div className='tab-bar'>
          {this.getTabs()}
          <div className='tab-fill' />
        </div>
        <div className='tab-content'>
          <div className='tab-wrapper'>
            {
              this.props.children.props.children.map((node, i) => {
                return React.cloneElement(node, {
                  key: i,
                  className: 'tab-container' + ((this.state.tabSelected === i) ? ' active' : '')
                });
              })
            }
          </div>
        </div>
      </div>
    );
  }
}

Tabbox.propTypes = {
  tabs: PropTypes.array,
  children: PropTypes.object
};
