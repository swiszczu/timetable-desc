/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * timetable-app.js - main component
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Button } from './components/button.jsx';
import { Label } from './components/label.jsx';
import { HelperTooltip } from './components/helper-tooltip.jsx';
import { Spinner } from './components/spinner.jsx';
import { Placeholder, PlaceholderConsts } from './components/placeholder.jsx';
import { Tabbox } from './components/tabbox.jsx';

// Tabs
import { StartTab } from './components/tab-start.jsx';
import { InfoTab } from './components/tab-info.jsx';
import { AppearanceTab } from './components/tab-appearance.jsx';
import { GroupsTab } from './components/tab-groups.jsx';
import { IOTab } from './components/tab-io.jsx';
import { AboutTab } from './components/tab-about.jsx';

const version = require('./backend/version');

const { ipcRenderer, remote } = window.require('electron');

export class TimetableApp extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      filePath: this.props.i18n.__('No file selected'),
      fileState: TimetableAppConsts.STATE_NO_FILE,
      parser: null,
      configState: 0
    };

    this.tabs = [
      props.i18n.__('Start'),
      props.i18n.__('Info'),
      props.i18n.__('Appearance'),
      props.i18n.__('Groups'),
      props.i18n.__('Input/Output'),
      props.i18n.__('About')
    ];

    ipcRenderer.on('json:saved', () => {
      this.setState({ configState: 3 });
    });

    this.tabRef = this.tabs.map(() => {
      return React.createRef();
    });
  }

  componentDidMount () {
    ipcRenderer.on('file:opened', (ev, path) => {
      this.setState({
        filePath: path,
        fileState: TimetableAppConsts.STATE_LOAD_PENDING
      });
    });

    ipcRenderer.on('file:loaderror', () => {
      this.setState({
        fileState: TimetableAppConsts.STATE_LOAD_ERROR
      });
    });

    ipcRenderer.on('file:loaded', () => {
      this.setState({
        fileState: TimetableAppConsts.STATE_LOADED,
        parser: remote.getGlobal('timetableParser')
      });

      this.listenForImportData();
    });

    ipcRenderer.on('json:loaded', (ev, data) => {
      if (data.ok) {
        if (data.initial) {
          this.setState({ configState: 1 });
        } else {
          this.setState({ configState: 2 });
        }
      }
    });
  }

  componentWillUnmount () {
    ipcRenderer.removeAllListeners('file:opened');
    ipcRenderer.removeAllListeners('file:loaderror');
    ipcRenderer.removeAllListeners('file:loaded');
    ipcRenderer.removeAllListeners('json:loaded');
  }

  getTabContent () {
    return (
      <>
        <StartTab
          ref={this.tabRef[4]}
          parser={this.state.parser}
          onImport={this.handleImport.bind(this)}
          onExport={this.handleExport.bind(this)}
          configState={this.state.configState}
          i18n={this.props.i18n}
        />
        <InfoTab
          ref={this.tabRef[0]}
          parser={this.state.parser}
          i18n={this.props.i18n}
        />
        <AppearanceTab
          ref={this.tabRef[1]}
          parser={this.state.parser}
          i18n={this.props.i18n}
        />
        <GroupsTab
          ref={this.tabRef[2]}
          key={this.state.parser}
          parser={this.state.parser}
          i18n={this.props.i18n}
        />
        <IOTab
          ref={this.tabRef[3]}
          key={this.state.parser}
          parser={this.state.parser}
          i18n={this.props.i18n}
          onShowPreview={this.handleShowPreview.bind(this)}
        />
        <AboutTab
          ref={this.tabRef[5]}
          key={this.state.parser}
          parser={this.state.parser}
          i18n={this.props.i18n}
        />
      </>
    );
  }

  getContent () {
    switch (this.state.fileState) {
      case TimetableAppConsts.STATE_NO_FILE:
        return (
          <Placeholder
            type={PlaceholderConsts.NO_FILE_LOADED}
            i18n={this.props.i18n}
          />
        );
      case TimetableAppConsts.STATE_LOAD_PENDING:
        return <Spinner />;
      case TimetableAppConsts.STATE_LOADED:
        return (
          <Tabbox tabs={this.tabs}>
            {this.getTabContent()}
          </Tabbox>
        );
      case TimetableAppConsts.STATE_LOAD_ERROR:
        return (
          <Placeholder
            type={PlaceholderConsts.FILE_LOADING_ERROR}
            i18n={this.props.i18n}
          />
        );
    }
  }

  render () {
    return (
      <div className='app-container'>
        <div className='vertical-flex'>
          <div>
            <Label text={this.props.i18n.__('Select an XML database file exported from aSc Timetable software') + ':'}>
              &nbsp;
              <HelperTooltip
                tooltip={this.props.i18n.__('See how to export XML database from aSc Timetables')}
                imageUrl='img/helper-export.png'
                size={16}
                imageWidth={700}
              />
            </Label>
            <div className='horizontal-flex'>
              <input
                type='text'
                style={{ fontWeight: 300 }}
                className={'text-field expand-horz' + ((this.state.fileState === 0) ? (' text-field-gray') : (''))}
                value={this.state.filePath} readOnly
              />
              <Button
                text={this.props.i18n.__('Browse...')}
                onClick={() => {
                  if (this.state.fileState !== TimetableAppConsts.STATE_LOAD_PENDING) {
                    ipcRenderer.send('file:browse');
                  }
                }}
              />
            </div>
          </div>
          <div className='expand-vert'>
            <div className='container'>
              {this.getContent()}
            </div>
          </div>
          {
            this.state.fileState === TimetableAppConsts.STATE_LOADED &&
              <div className='horizontal-flex'>
                <div className='expand-horz' />
                <Button
                  text={this.props.i18n.__('Export single file')}
                  onClick={this.handleExportNew.bind(this, false)}
                />
                <Button
                  style={{ marginRight: '32px' }}
                  text={this.props.i18n.__('Export multiple files')}
                  onClick={this.handleExportNew.bind(this, true)}
                />
              </div>
          }
        </div>
      </div>
    );
  }

  handleExport () {
    try {
      const obj = {
        versionInfo: version,
        generalOptions: this.tabRef[4].current.serializeState(),
        appearanceOptions: this.tabRef[1].current.serializeState(),
        groupRenamingOptions: this.tabRef[2].current.serializeState(),
        ioOptions: this.tabRef[3].current.serializeState()
      };

      ipcRenderer.send('json:save', {
        data: obj,
        dialog: true
      });
    } catch (error) {
      console.error(error);
    }
  }

  handleExportNew (multi) {
    const config = this.tabRef[3].current.getExportConfiguration();
    config.multi = multi;

    ipcRenderer.send('preview:create', {
      renderData: config,
      softConfig: {
        versionInfo: version,
        generalOptions: this.tabRef[4].current.serializeState(),
        appearanceOptions: this.tabRef[1].current.serializeState(),
        groupRenamingOptions: this.tabRef[2].current.serializeState(),
        ioOptions: this.tabRef[3].current.serializeState()
      },
      xmlFilePath: this.state.filePath
    });
  }

  listenForImportData () {
    ipcRenderer.once('json:loaded', (ev, data) => {
      if (data.ok) {
        try {
          const dataObj = JSON.parse(data.data);
          this.tabRef[4].current.deserializeState(dataObj.generalOptions);
          this.tabRef[1].current.deserializeState(dataObj.appearanceOptions);
          this.tabRef[2].current.deserializeState(dataObj.groupRenamingOptions);
          this.tabRef[3].current.deserializeState(dataObj.ioOptions);
        } catch (error) {
          console.error(error);
        }
      }
    });
  }

  handleImport () {
    ipcRenderer.removeAllListeners('json:loaded');
    ipcRenderer.send('json:load', {
      dialog: true
    });

    this.listenForImportData();
  }

  handleShowPreview (data) {
    ipcRenderer.send('preview:create', {
      renderData: data,
      softConfig: {
        versionInfo: version,
        generalOptions: this.tabRef[4].current.serializeState(),
        appearanceOptions: this.tabRef[1].current.serializeState(),
        groupRenamingOptions: this.tabRef[2].current.serializeState(),
        ioOptions: this.tabRef[3].current.serializeState()
      },
      xmlFilePath: this.state.filePath
    });
  }
}

const TimetableAppConsts = {
  STATE_NO_FILE: 0,
  STATE_LOAD_PENDING: 1,
  STATE_LOADED: 2,
  STATE_LOAD_ERROR: 3
};

TimetableApp.propTypes = {
  i18n: PropTypes.object
};
