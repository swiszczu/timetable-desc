/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * add-rule.js - main component of Add rule dialog
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Label } from './components/label.jsx';
import { Button } from './components/button.jsx';

import { GroupRow } from './components/group-row.jsx';

import memo from 'memoize-one';

const { ipcRenderer } = window.require('electron');

export class AddRuleDialog extends React.PureComponent {
  constructor (props) {
    super(props);

    this.state = {
      selected: this.getDefaultArray(props.dialogData.groupArr.length, false),
      filter: this.getDefaultArray(props.dialogData.groupArr.length, true),
      filterText: '',
      nameText: ''
    };

    this.groupList = memo(this.getGroupList);

    if (props.dialogData.currentState) {
      const state = props.dialogData.currentState;

      this.state.nameText = state.newName;
      for (let i = 0; i < props.dialogData.groupArr.length; i++) {
        let selected = false;

        state.groupNames.forEach(groupName => {
          if (groupName === props.dialogData.groupArr[i]) {
            selected = true;
          }
        });

        this.state.selected[i] = selected;
      }
    }
  }

  getDefaultArray (length, val) {
    const ret = new Array(length);

    for (let i = 0; i < ret.length; i++) {
      ret[i] = val;
    }

    return ret;
  }

  handleClickCancel () {
    window.close();
  }

  handleClickApply () {
    const groupNames = [];
    this.state.selected.forEach((el, i) => {
      if (el) {
        groupNames.push(this.props.dialogData.groupArr[i]);
      }
    });

    if (this.props.dialogData.currentState) {
      ipcRenderer.send('rule:added', {
        subjectId: this.props.dialogData.subjectId,
        name: this.state.nameText,
        groupNames: groupNames,
        id: this.props.dialogData.currentState.id
      });
    } else {
      ipcRenderer.send('rule:added', {
        subjectId: this.props.dialogData.subjectId,
        name: this.state.nameText,
        groupNames: groupNames
      });
    }

    window.close();
  }

  handleToggleRow (index) {
    this.setState(state => {
      const newSelection = [...state.selected];
      newSelection[index] = !newSelection[index];

      return {
        selected: newSelection
      };
    });
  }

  getGroupList (selected, filter) {
    return (
      <>
        {
          this.props.dialogData.groupArr.map((element, i) => {
            if (filter[i]) {
              return (
                <GroupRow
                  key={i}
                  groupName={element}
                  active={selected[i]}
                  onToggle={this.handleToggleRow.bind(this, i)}
                />
              );
            }
            return null;
          })
        }
      </>
    );
  }

  handleClickSelectAll (newVal) {
    this.setState(state => {
      const newSelection = [...state.selected];
      for (let i = 0; i < newSelection.length; i++) {
        if (state.filter[i]) {
          newSelection[i] = newVal;
        }
      }

      return {
        selected: newSelection
      };
    });
  }

  handleClickInvert () {
    this.setState(state => {
      const newSelection = [...state.selected];
      for (let i = 0; i < newSelection.length; i++) {
        if (state.filter[i]) {
          newSelection[i] = !newSelection[i];
        }
      }

      return {
        selected: newSelection
      };
    });
  }

  handleChangeName (event) {
    this.setState({
      nameText: event.target.value
    });
  }

  handleChangeFilter (event) {
    // Refresh filter
    const query = event.target.value;

    const filt = this.getDefaultArray(this.props.dialogData.groupArr.length,
      query.length === 0);

    if (query.length > 0) {
      for (let i = 0; i < filt.length; i++) {
        if (this.props.dialogData.groupArr[i].toLowerCase()
          .includes(query.toLowerCase())) {
          filt[i] = true;
        }
      }
    }

    this.setState({
      filterText: query,
      filter: filt
    });
  }

  getTitle () {
    if (this.props.dialogData.currentState) {
      return (this.props.i18n.__('Edit rule from ') +
        this.props.dialogData.subjectName);
    }
    return (this.props.i18n.__('Add rule to ') +
      this.props.dialogData.subjectName);
  }

  render () {
    return (
      <div className='vertical-flex nomargin nosel'>
        <h1 className='dialog-title'>{this.getTitle()}</h1>
        <Label text={this.props.i18n.__('New Name') + ':'} />
        <input
          type='text'
          className='text-field expand-horz'
          onChange={this.handleChangeName.bind(this)}
          value={this.state.nameText}
          autoFocus
        />
        <br />

        <Label text={this.props.i18n.__('Groups with this rule applied to') + ':'} />
        <input
          type='text'
          className='text-field expand-horz'
          onChange={this.handleChangeFilter.bind(this)}
          value={this.state.filterText}
          placeholder={this.props.i18n.__('Search...')}
        />
        <br />
        <div style={{ marginBottom: '4px' }}>
          <div style={{ display: 'inline-block', marginRight: '4px' }}>
            <Button
              text={this.props.i18n.__('Select all')}
              onClick={this.handleClickSelectAll.bind(this, true)}
              small
            />
          </div>
          <div style={{ display: 'inline-block', marginRight: '4px' }}>
            <Button
              text={this.props.i18n.__('Deselect all')}
              onClick={this.handleClickSelectAll.bind(this, false)}
              small
            />
          </div>
          <div style={{ display: 'inline-block', marginRight: '4px' }}>
            <Button
              text={this.props.i18n.__('Invert selection')}
              onClick={this.handleClickInvert.bind(this)}
              small
            />
          </div>
        </div>
        <div className='expand-vert scroll'>
          {this.groupList(this.state.selected, this.state.filter)}
        </div>
        <div className='horizontal-flex'>
          <div className='expand-horz' />
          <Button
            text={this.props.i18n.__('Cancel')}
            onClick={this.handleClickCancel.bind(this)}
          />
          <Button
            text={this.props.i18n.__('Apply')}
            onClick={this.handleClickApply.bind(this)}
          />
        </div>
      </div>
    );
  }
}

AddRuleDialog.propTypes = {
  i18n: PropTypes.object,
  dialogData: PropTypes.object
};
