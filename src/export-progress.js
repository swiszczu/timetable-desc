/* Timetable DESC - ASCTT render engine
 * Copyright (C) 2020 Łukasz Świszcz
 *
 * export-progress.js - main component for Export progress panel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Button } from './components/button.jsx';
import { ProgressBar } from './components/progressbar.jsx';

const { ipcRenderer } = window.require('electron');

export class ExportProgressWindow extends React.PureComponent {
  constructor () {
    super();

    this.state = {
      currentPage: 0,
      totalPages: 1
    };

    this.updateBound = this.handleUpdate.bind(this);
  }

  componentDidMount () {
    ipcRenderer.on('export:progress:update', this.updateBound);
  }

  componentWillUnmount () {
    ipcRenderer.removeListener('export:progress:update', this.updateBound);
  }

  handleClickCancel () {
    ipcRenderer.send('export:progress:cancel');
  }

  handleUpdate (ev, data) {
    this.setState({
      currentPage: data.current,
      totalPages: data.total
    });
  }

  render () {
    return (
      <div className='progress-window'>
        <img className='export-decor' src='img/file-export.svg' />
        <div className='progress-label'>{this.props.i18n.__('Exporting timetable...')}</div>
        <ProgressBar value={this.state.currentPage / this.state.totalPages * 100} />
        <div className='progress-label'>{
          this.props.i18n.__mf('Page %d of %d',
            this.state.currentPage,
            this.state.totalPages
          )
        }
        </div>
        <div className='horizontal-flex'>
          <div className='expand-horz' />
          <Button
            text={this.props.i18n.__('Cancel')}
            onClick={this.handleClickCancel.bind(this)}
          />
          <div className='expand-horz' />
        </div>
      </div>
    );
  }
}

ExportProgressWindow.propTypes = {
  i18n: PropTypes.object
};
